<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\UserMeasurements;
use common\models\CenterUser;
use common\models\MemberStrength;
use yii\db\Query;

/* sdasd */

//error_reporting(0);
class CronjobController extends RestController {

    public function actions() {
        $actions_c = [
            'measurementNotification' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionMeasurementNotification']
            ],'pendingNotification' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionPendingNotification']
            ],
        ];

        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }
    
    public function actionPendingNotification()
    {
        
        $trainer_body  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                     *|CONTENT|*<br/><br/>
                
                Thanks <br/>
                Team TraQ
EOP;
        $user_body  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                     *|CONTENT|* <br/><br/>
                
                Thanks <br/>
                Team TraQ
EOP;
        
        
        
        
        $sql = "SELECT n.member_id, u.email user_email,u.first_name user_firstname,CONCAT(u.first_name,' ',u.last_name) user_name,t.first_name trainer_firstname,t.email trainer_email, n.trainer_id,n.assessment,n.due_date,DATEDIFF(STR_TO_DATE(`due_date`,'%d-%m-%Y'),CURDATE()) as date_diff FROM `sb__notification` n,sb__center_user u,sb__center_user t where n.member_id = u.id and n.trainer_id = t.id and DATEDIFF(STR_TO_DATE(`due_date`,'%d-%m-%Y'),CURDATE()) IN(1,3,7) order by date_diff";
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($sql);
        $data = $query_bld->queryAll();
        
        foreach($data as $obj)
        {
            if(!isset($member[$obj['member_id']]['content']))
            {
                $member[$obj['member_id']]['name']  = $obj['user_firstname'];
                $member[$obj['member_id']]['email'] = $obj['user_email'];
                $member[$obj['member_id']]['content'] = "Your due date for assesments <br/><br/><table><th><b>Assement Name</b></th><th><b>Due Date</b></th>";
            }
            
            if($obj['assessment']=="measurement")
            {
                if(!isset($trainer[$obj['trainer_id']]['measurement_content']))
                {
                    $trainer[$obj['trainer_id']]['measurement_content']  = "The Measurements of following members on due date <br><table><tr><th><b>Member Name</b></th><th><b>Due Date</b></th>";
                }
                $trainer[$obj['trainer_id']]['name']  = $obj['trainer_firstname'];
                $trainer[$obj['trainer_id']]['email'] = $obj['trainer_email'];
                $trainer[$obj['trainer_id']]['measurement_content'] .= "<tr><td>".$obj['user_name']."</td><td>".$obj['due_date']."</td><tr/>";
                
                
                $member[$obj['member_id']]['content'] .= "<tr><td>Measurement</td><td>".$obj['due_date']."</td></tr>";
                
                
                        
            }
            else if($obj['assessment']=="fitness_component")
            {
               if(!isset($trainer[$obj['trainer_id']]['component_content']))
               {
                   $trainer[$obj['trainer_id']]['component_content']  = "The Fitness Component of following members on due date <br><table><tr><th><b>Member Name</b></th><th><b>Due Date</b></th>";
               }
               $trainer[$obj['trainer_id']]['name']  = $obj['trainer_firstname'];
               $trainer[$obj['trainer_id']]['email'] = $obj['trainer_email'];
               $trainer[$obj['trainer_id']]['component_content'] .= "<tr><td>".$obj['user_name']."</td><td>".$obj['due_date']."</td></tr>";
           
               $member[$obj['member_id']]['content'] .= "<tr><td>Fitness Component</td><td>".$obj['due_date']."</td></tr>";
            }
            
           
        }
        
        $user_email = array();
        foreach($member as $key=>$val)
        {
            $user_email[] = array($val['name'],$val['email'],array("CONTENT"=>$val['content']));
        }
        
        $trainer_mail = array();
        foreach($trainer as $key=>$val)
        {
            $content = "";
            if(isset($val['measurement_content']))
            {
                $content .= $val['measurement_content']."</table><br/><br/>";
                if(isset($val['component_content']))
                {
                    $content  .= $val['component_content']."</table><br/><br/>";
                }
            }
            else{
                if(isset($val['component_content']))
                {
                    $content  .= $val['component_content']."</table><br/><br/>";
                }
            }
            
            $trainer_mail[]  = array($val['name'],$val['email'],array("CONTENT"=>$content));            
            
        }
        

          $Notification_trainer  =  $this->sendviaMandrill($trainer_mail,'Pending Task Notification', $trainer_body, 'contact@gympik.com', 'Team Traq');
          $Notification_member  =  $this->sendviaMandrill($user_email,'Upcoming Task Notification', $user_body, 'contact@gympik.com', 'Team Traq');
          var_dump($Notification_trainer);
          var_dump($Notification_member);
        
    }

    public function actionMeasurementNotification() {
     /*  $emaillist = [
            [
                'manas pakal',
                'msg4manas@gmail.com',
                [
                    'USR_LST'=>'l1 l2 l4',
                    'TYPE'=>'user1'
                ]
            ],
            [
                'lokesh',
                'lokesh.jain@gympik.com',
                [
                    'USR_LST'=>'lists',
                    'TYPE'=>'user99'
                ]
            ]
        ];
        $content =<<<EOP
                Dear, *|NAME|*
                <br />
                Ur List *|USR_LST|*
                <br />*|TYPE|*
                test text
EOP; 
       $ret = $this->sendviaMandrill($emaillist, 'test eamil *|TYPE|*', $content, 'contact@gympik.com', 'Team Traq','mrg test');
        var_dump($ret);exit;
        */
     
        $body_7days_4user  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                      This email is reminder to you that you have to fill your measurement after 7 days. <br/><br/>
                
                Thanks <br/>
                Team TraQ
EOP;
        
        $body_3days_4user  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                      This email is reminder to you that you have to fill your measurement after 3 days. <br/><br/>
                
                Thanks <br/>
                Team TraQ
EOP;
        
        $body_1days_4user  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                      This email is reminder to you that you have to fill your measurement tomorrow. <br/><br/>
                
                Thanks <br/>
                Team TraQ
EOP;
        $body_7days_4trainer  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                      This email is reminder to you that you have to fill measurement after 7 days for members who are asociated with you. Here is the list of members<br/><br/>
                    *|CONTENT|* <br/><br/>
                Thanks <br/>
                Team TraQ
EOP;
        $body_3days_4trainer  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                      This email is reminder to you that you have to fill measurement after 3 days for members who are asociated with you. Here is the list of members<br/><br/>
                    *|CONTENT|*<br/><br/>
                Thanks <br/>
                Team TraQ
EOP;
        $body_1days_4trainer  =<<<EOP
                   Dear *|NAME|*, <br/><br/>
                        
                      This email is reminder to you that you have to fill measurement tomorrow for members who are asociated with you. Here is the list of members<br/><br/>
                    *|CONTENT|*<br/><br/>
                Thanks <br/>
                Team TraQ
EOP;
   
        $user_7days = array();
        $user_3days = array();
        $user_1days = array();
        $trainer_7days = array();
        $trainer_3days = array();       
        $trainer_1days = array();

        $sql = "SELECT user_id,DATEDIFF(STR_TO_DATE(`due_date`,'%d-%m-%Y'),CURDATE()) as date_diff FROM `sb__user_measurements` where DATEDIFF(STR_TO_DATE(`due_date`,'%d-%m-%Y'),CURDATE()) IN(1,3,7)";
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($sql);
        $data = $query_bld->queryAll();

        foreach ($data as $obj) {
           
            if ($obj['date_diff'] == 7) {

                $user_model = CenterUser::find()->where('id = :id', ['id' => $obj['user_id']])->one();
                          
                $user_7days[] = array($user_model->first_name,$user_model->email);
                if (isset($user_model->personal_trainer_id) && !is_null($user_model->personal_trainer_id)) {
                    $trainer_7days[$user_model->personal_trainer_id][] = $user_model->first_name . " " . $user_model->last_name;
                } else if (isset($user_model->general_trainer_id) && !is_null($user_model->general_trainer_id)) {
                    $trainer_7days[$user_model->general_trainer_id][] = $user_model->first_name . " " . $user_model->last_name;
                }
            } else if ($obj['date_diff'] == 3) {

                $user_model = CenterUser::find()->where('id = :id', ['id' => $obj['user_id']])->one();
                $user_3days[] = array($user_model->first_name,$user_model->email);
                if (isset($user_model->personal_trainer_id) && !is_null($user_model->personal_trainer_id)) {
                    $trainer_3days[$user_model->personal_trainer_id][] = $user_model->first_name . " " . $user_model->last_name;
                } else if (isset($user_model->general_trainer_id) && !is_null($user_model->general_trainer_id)) {
                    $trainer_3days[$user_model->general_trainer_id][] = $user_model->first_name . " " . $user_model->last_name;
                }
            } else if ($obj['date_diff'] == 1) {

                $user_model = CenterUser::find()->where('id = :id', ['id' => $obj['user_id']])->one();
                $user_1days[] = array($user_model->first_name,$user_model->email);
                if (isset($user_model->personal_trainer_id) && !is_null($user_model->personal_trainer_id)) {
                    $trainer_1days[$user_model->personal_trainer_id][] = $user_model->first_name . " " . $user_model->last_name;
                } else if (isset($user_model->general_trainer_id) && !is_null($user_model->general_trainer_id)) {
                    $trainer_1days[$user_model->general_trainer_id][] = $user_model->first_name . " " . $user_model->last_name;
                }
            }
        }
        foreach($trainer_7days as $key=>$val)
        {
            $user_model = CenterUser::find()->where('id = :id', ['id' =>$key])->one();
            
            $user_content = "<ul>";
            foreach($trainer_7days[$key] as $name)
            {
               $user_content .= "<li>$name</li>";
                
            }
            $user_content .= "<ul>";
            $Trainer_7days[]  = array($user_model->first_name.' '.$user_model->last_name,$user_model->email,array("CONTENT"=>$user_content));
        }
        
        foreach($trainer_3days as $key=>$val)
        {
            $user_model = CenterUser::find()->where('id = :id', ['id' =>$key])->one();
            
            $user_content = "<ul>";
            foreach($trainer_3days[$key] as $name)
            {
               $user_content .= "<li>$name</li>";
                
            }
            $user_content .= "<ul>";
            $Trainer_3days[]  = array($user_model->first_name.' '.$user_model->last_name,$user_model->email,array("CONTENT"=>$user_content));
        }
        
        foreach($trainer_1days as $key=>$val)
        {
            $user_model = CenterUser::find()->where('id = :id', ['id' =>$key])->one();
            
            $user_content = "<ul>";
            foreach($trainer_1days[$key] as $name)
            {
               $user_content .= "<li>$name</li>";
                
            }
            $user_content .= "<ul>";
            $Trainer_1days[]  = array($user_model->first_name.' '.$user_model->last_name,$user_model->email,array("CONTENT"=>$user_content));
        }
        
     /*   
        $Notification_user_7days  =  $this->sendviaMandrill($user_7days,'7 Days remaining to fill Measurement', $body_7days_4user, 'contact@gympik.com', 'Team Traq');
        $Notification_user_3days  =  $this->sendviaMandrill($user_3days,'3 Days remaining to fill Measurement', $body_3days_4user, 'contact@gympik.com', 'Team Traq');
        $Notification_user_1days  =  $this->sendviaMandrill($user_1days,'Fill your measurement Tomorrow', $body_1days_4user, 'contact@gympik.com', 'Team Traq');
       */
        
        $Notification_trainer_7days  =  $this->sendviaMandrill($Trainer_7days,'7 Days remaining to fill Measurement', $body_7days_4trainer, 'contact@gympik.com', 'Team Traq');
        $Notification_trainer_3days  =  $this->sendviaMandrill($Trainer_3days,'3 Days remaining to fill Measurement', $body_3days_4trainer, 'contact@gympik.com', 'Team Traq');
        $Notification_trainer_1days  =  $this->sendviaMandrill($Trainer_1days,'Fill your measurement Tomorrow', $body_1days_4trainer, 'contact@gympik.com', 'Team Traq');
        
    }
    public function actionIndex(){
        echo "<pre>";
        print_r($_REQUEST);
        exit;
    }

    // to send email to list
    public function actionSendEmailToList(){
        $now = date("Y-m-d H:i:s");
        $compose_list = \app\models\EmailerCompose::find()
                ->where("sent_on <= '$now' AND completed = 0")
                ->all();
        echo "<pre>";
        print_r($compose_list);
        exit;
    }
}
