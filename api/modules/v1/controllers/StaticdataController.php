<?php

namespace api\modules\v1\controllers;

use api\components\RestController;

class StaticdataController extends RestController
{
    public function actions() {
        $actions_c = [
            'getCity' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetCity']
            ],        
            'getLocality' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetLocality']
            ]        
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }
    public function actionGetCity(){
        /*$data = \common\models\City::find()->select(['id','city'])->all();
        $result = \yii\helpers\ArrayHelper::map($data, 'id', 'city');*/
        $result = array(428=>"Bangalore");
        return array("status"=>1,"message"=>"city listed successfully","result"=>$result); 
    }
    public function actionGetLocality(){
        $city_id = isset($_REQUEST['city_id']) ? $_REQUEST['city_id'] : "";
        $loca_str = isset($_REQUEST['term']) ? $_REQUEST['term'] : "";
        $loca_str = trim($loca_str);
        if($city_id == "")
            return array("status"=>0,"message"=>"city not given"); 
        if($loca_str != '')
            $data = \common\models\Locality::find()->where("city = :city AND locality LIKE '$loca_str%'",[":city"=>$city_id])->select(['id','locality'])->all();
        else
            $data = \common\models\Locality::find()->where("city = :city",[":city"=>$city_id])->select(['id','locality'])->all();
        $result = \yii\helpers\ArrayHelper::map($data, 'id', 'locality');
        return array("status"=>1,"message"=>"Locality listed successfully","result"=>$result); 
    }
}