<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\CenterUser;
use common\models\Role;
use common\models\Attendance;
use yii\db\Query;
use common\models\SocialIntegration;

/* sdasd */

//error_reporting(0);
//class UserController extends Controller
class SocialController extends RestController {

    public function behaviors() {
        $no_auth_actions = ["getTweets"];
        $behaviors = parent::behaviors();
//        return $behaviors;
//        if (in_array($this->action->id, $no_auth_actions))
//            return $behaviors;
        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except' => $no_auth_actions
        ];
        return $behaviors;
    }

    public function actions() {
        $actions_c = [
            'getTweets' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetTweets']
            ],
            'savePostData' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionSavePostData']
            ],
            'saveCredentials' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionSaveCredentials']
            ],
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
    }

    public function actionIndex() {
        
    }
    public function actionSavePostData(){
        $data = [];
        $data['added_by'] = $data['updated_by'] = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $data['company_id'] = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : 0;
        $data['added_on'] = $data['updated_on'] = date("Y-m-d H:i:s");
        $data['social_type'] = isset($_REQUEST['social_type']) ? $_REQUEST['social_type'] : 0;
        $data['post_id'] = isset($_REQUEST['post_id']) ? $_REQUEST['post_id'] : '0';
        $data['post_data'] = isset($_REQUEST['post_data']) ? serialize($_REQUEST['post_data']) : "";
        $model = new \common\models\SocialPostData();
        $model->attributes = $data;
        if($model->save())
            return ["status"=>1, "message"=>"Saved Successfully"];
        return ["status"=>0, "message"=>"Error : ".$this->arrayToString($model->getErrors())];
    }

    public function actionGetTweets() {
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;

        require_once \Yii::getAlias('@app') . '/../common/libraries/social/twitteroauth/twitteroauth.php';
        // it will come dynamically from db
        // manas.pakal
        $twitter_access_token = '936556423-9tFcQhOsXC1d8QfAvNyubn9nDyRCmMstjm2RUTm8';
        $twitter_access_token_secret = 'MYHj4YlzU7WYeR0NPhYFoQhEg3EF0nb7LmbYvxWVLu9qo';

        // app deails
        $twitter_consumer_key = 'DnZ59sUCRv2SMRPtLo63FVdF2';
        $twitter_consumer_secret = 'xygUFNPzToTJKunbCmngk7QDbRrQdUW1ZTy9j0umvNg1jwkq4W';
        $connection = new \TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret, $twitter_access_token, $twitter_access_token_secret);
        // max_id=615423825178537983&q=gympik&include_entities=1&result_type=recent
        if (isset($_REQUEST['hash_tag']) && trim($_REQUEST['hash_tag']) != "") {
            $query = [
                "q" => isset($_REQUEST['hash_tag']) ? $_REQUEST['hash_tag'] : 'gympik', //'gympik', // #gympik-filter:retweets (with retweet 0) // +RT #gympik+filter:retweets (with retweeted atleast once) 
                "result_type" => 'recent', // recent  // popular (optional)
//                "max_id"=> '615423825178537983',
                'count' => 50
            ];
            if (isset($_REQUEST['max_id']))
                $query['max_id'] = $_REQUEST['max_id'];
            $results = $connection->get('search/tweets', $query);
        }else{
            $query = [
                "count" => 50
            ];
            if (isset($_REQUEST['max_id']))
                $query['max_id'] = $_REQUEST['max_id'];
            if (isset($_REQUEST['exclude_replies']))
                $query['exclude_replies'] = TRUE;
            $results = $connection->get('statuses/home_timeline', $query);
            
        }
        $max_id = 0;
        try {
            $max_id = min(array_map('next', (array)$results));
        } catch (Exception $exc) {
//            echo $exc->getTraceAsString();
        }
        $results['max_id'] = $max_id;
        if (isset($results->errors))
            $return = ["status" => 0, "message" => "Something Went Wrong", "result" => $results];
        else
            $return = ["status" => 1, "message" => "fetched successfully", "result" => $results];
        return $return;
    }
    public function actionSaveCredentials(){
        $company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : 0;
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $exist = SocialIntegration::find()
                ->where(["company_id"=>$company_id])
                ->one();
        $model = new SocialIntegration();
        $model = isset($exist->id) ? $exist : $model;
        if(isset($_REQUEST['Setup']['tw_user_id']) && trim($_REQUEST['Setup']['tw_user_id']) != '')
            $model->scenario = "twitter";
        else
            $model->scenario = "facebook";
        $model->attributes = isset($_REQUEST['Setup']) ? $_REQUEST['Setup'] : [];
        $model->company_id = $company_id;
        $model->updated_on = date("Y-m-d H:i:s");
        $model->updated_by = $user_id;
        if($model->isNewRecord){
            $model->added_by = $model->updated_by;
            $model->created_on = $model->updated_on;
        }
        if($model->save()){
            return ["status" => 1, "message" => $model->scenario." credential Saved Successfully"];
        }  else {
            return ["status" => 0, "message" => "Error : ".$this->arrayToString($model->getErrors())];
        }
    }
}
