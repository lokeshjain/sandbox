<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\DietChart;
use yii\db\Query;

/* sdasd */

//error_reporting(0);
class DietController extends RestController {

    public function behaviors() {
        $no_auth_actions = array("getDietChart");
        $behaviors = parent::behaviors();
//        return $behaviors;

        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except' => $no_auth_actions
        ];
        return $behaviors;
    }

    public function actions() {
        $actions_c = [
            'addEditDietChart' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddEditDietChart']
            ],
            'getDietChart' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetDietChart']
            ],
            'getUsersDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUsersDetails']
            ],
            'getStatistics' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetStatistics']
            ],
            'report' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionReport']
            ],
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }

    public function actionAddEditDietChart() {
        $status;
        $message;

        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());

        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
        $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : '';
        $pre_breakfast = isset($_REQUEST['pre_breakfast']) ? $_REQUEST['pre_breakfast'] : '';
        $pre_breakfast_time = isset($_REQUEST['pre_breakfast_time']) ? $_REQUEST['pre_breakfast_time'] : '';
        $breakfast = isset($_REQUEST['breakfast']) ? $_REQUEST['breakfast'] : '';
        $breakfast_time = isset($_REQUEST['breakfast_time']) ? $_REQUEST['breakfast_time'] : '';
        $later_morning_meal = isset($_REQUEST['later_morning_meal']) ? $_REQUEST['later_morning_meal'] : '';
        $later_morning_meal_time = isset($_REQUEST['later_morning_meal_time']) ? $_REQUEST['later_morning_meal_time'] : '';
        $lunch = isset($_REQUEST['lunch']) ? $_REQUEST['lunch'] : '';
        $lunch_time = isset($_REQUEST['lunch_time']) ? $_REQUEST['lunch_time'] : '';
        $late_afternoon = isset($_REQUEST['late_afternoon']) ? $_REQUEST['late_afternoon'] : '';
        $late_afternoon_time = isset($_REQUEST['late_afternoon_time']) ? $_REQUEST['late_afternoon_time'] : '';
        $evening_tea = isset($_REQUEST['evening_tea']) ? $_REQUEST['evening_tea'] : '';
        $evening_tea_time = isset($_REQUEST['evening_tea_time']) ? $_REQUEST['evening_tea_time'] : '';
        $dinner = isset($_REQUEST['dinner']) ? $_REQUEST['dinner'] : '';
        $dinner_time = isset($_REQUEST['dinner_time']) ? $_REQUEST['dinner_time'] : '';
        $post_dinner = isset($_REQUEST['post_dinner']) ? $_REQUEST['post_dinner'] : '';
        $post_dinner_time = isset($_REQUEST['post_dinner_time']) ? $_REQUEST['post_dinner_time'] : '';
        $feedback = isset($_REQUEST['feedback']) ? $_REQUEST['feedback'] : '';
//        $action                         =    isset($_REQUEST['action'])?$_REQUEST['action']:'';
//        $diet_id                        =    isset($_REQUEST['diet_id'])?$_REQUEST['diet_id']:'';

        $logged_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';

        if ($user_id != 0 && !empty($date)) {
            $exist = DietChart::find()->where("user_id = :user_id AND date = :date", [":user_id" => $user_id, ":date" => $date])->one();
            if (isset($exist->id)) {
                $model = $exist;
            } else {
                $model = new DietChart();
            }
            if (isset($model)) {
                $model->user_id = $user_id;
                $model->date = $date;
                $model->pre_breakfast = $pre_breakfast;
                $model->pre_breakfast_time = $pre_breakfast_time;
                $model->breakfast = $breakfast;
                $model->breakfast_time = $breakfast_time;
                $model->later_morning_meal = $later_morning_meal;
                $model->later_morning_meal_time = $later_morning_meal_time;
                $model->lunch = $lunch;
                $model->lunch_time = $lunch_time;
                $model->late_afternoon = $late_afternoon;
                $model->late_afternoon_time = $late_afternoon_time;
                $model->evening_tea = $evening_tea;
                $model->evening_tea_time = $evening_tea_time;
                $model->dinner = $dinner;
                $model->dinner_time = $dinner_time;
                $model->post_dinner = $post_dinner;
                $model->post_dinner_time = $post_dinner_time;
                $model->feedback = $feedback;
                if ($model->isNewRecord) {
                    $model->added_on = $current_timestamp;
                    $model->added_by = $logged_id;
                }

                $model->updated_on = $current_timestamp;
                $model->updated_by = $logged_id;
                $model->status = 1;

                if ($model->save()) {
                    $status = 1;
                    $message = ($model->isNewRecord) ? "Diet plan added sucessfully!" : "Diet plan Updated sucessfully!";
                } else {
                    $status = 0;
                    $message = "Error : " . $this->arrayToString($model->getErrors());
                }
            } else {

                $status = 2;
                $message = "Please provide accurate diet id!";
            }
        } else {
            $status = 0;
            $message = "Please provide all required fields!";
        }


        $result = array("status" => $status, "message" => $message);
        return $result;
    }

    public function actionGetDietChart() {
        $user_id = isset($_REQUEST['user_id']) ? trim($_REQUEST['user_id']) : "";
        $date = isset($_REQUEST['date']) ? trim($_REQUEST['date']) : '';
        $for = isset($_REQUEST['for']) ? trim($_REQUEST['for']) : 'all';

        if ($user_id == '' || $date == '')
            return array("status" => 0, "message" => "user id and date is mandatory");
//        $date = date("Y-m-d",  strtotime($date));
        if ($for == 'trainer' || $for == 'all') {
            $select = ["id", "pre_breakfast", "pre_breakfast_time", "breakfast", "breakfast_time", "later_morning_meal", "later_morning_meal_time", "lunch", "lunch_time", "late_afternoon", "late_afternoon_time", "evening_tea", "evening_tea_time", "dinner", "dinner_time", "post_dinner", "post_dinner_time", "feedback"];
            $exist = DietChart::find()
                            ->select($select)
                            ->where("user_id = :user_id AND date = :date", [":user_id" => $user_id, ":date" => $date])->one();
            $exist_data = array();
            if (isset($exist->id)) {
                if ($for == 'all') {
                    $exist_data['id'] = $exist->id;
                    $exist_data['pre_breakfast']['time'] = $exist->pre_breakfast_time;
                    $exist_data['pre_breakfast']['data'] = $exist->pre_breakfast;
                    $exist_data['breakfast']['time'] = $exist->breakfast_time;
                    $exist_data['breakfast']['data'] = $exist->breakfast;
                    $exist_data['later_morning_meal']['time'] = $exist->later_morning_meal_time;
                    $exist_data['later_morning_meal']['data'] = $exist->later_morning_meal;
                    $exist_data['lunch']['time'] = $exist->lunch_time;
                    $exist_data['lunch']['data'] = $exist->lunch;
                    $exist_data['late_afternoon']['time'] = $exist->late_afternoon_time;
                    $exist_data['late_afternoon']['data'] = $exist->late_afternoon;
                    $exist_data['evening_tea']['time'] = $exist->evening_tea_time;
                    $exist_data['evening_tea']['data'] = $exist->evening_tea;
                    $exist_data['dinner']['time'] = $exist->dinner_time;
                    $exist_data['dinner']['data'] = $exist->dinner;
                    $exist_data['post_dinner']['time'] = $exist->post_dinner_time;
                    $exist_data['post_dinner']['data'] = $exist->post_dinner;
                    $exist_data['feedback'] = $exist->feedback;

                    $select_intake = ['date', 'time', 'meal_type', 'food_intake', 'as_per_trainer'];
                    $intake_data = \common\models\UserFoodIntake::find()
                            ->where('diet_id = :diet_id', [':diet_id' => $exist->id])
                            ->select($select_intake)
                            ->all();

                    foreach ($intake_data as $key => $rec) {
                        $exist_data[$rec->meal_type]['intake_time'] = $rec->time;
                        $exist_data[$rec->meal_type]['intake_data'] = $rec->food_intake;
                        $exist_data[$rec->meal_type]['as_per_trainer'] = $rec->as_per_trainer;
                    }
                }
                return array("status" => 1, "message" => "Diet chart fetched successfully", "result" => $exist_data);
            } else {
                $select_intake = ['date', 'time', 'meal_type', 'food_intake', 'as_per_trainer'];
                $intake_data = \common\models\UserFoodIntake::find()
                        ->where("user_id = :user_id AND date = :date", [":user_id" => $user_id, ":date" => $date])
                        ->select($select_intake)
                        ->all();
                $exist_data = array();
                foreach ($intake_data as $key => $rec) {
                    $exist_data[$rec->meal_type]['intake_time'] = $rec->time;
                    $exist_data[$rec->meal_type]['intake_data'] = $rec->food_intake;
                    $exist_data[$rec->meal_type]['as_per_trainer'] = $rec->as_per_trainer;
                }
                return array("status" => 1, "message" => "Food Intake fetched successfully", "result" => $exist_data);
            }
        } elseif ($for == 'user') {
            $intake_data = \common\models\UserFoodIntake::find()
                    ->where("user_id = :user_id AND date = :date", [":user_id" => $user_id, ":date" => $date])
                    ->select(['date', 'time', 'meal_type', 'food_intake'])
                    ->all();
            return array("status" => 1, "message" => "Food Intake fetched successfully", "result" => $intake_data);
        } else
            return array("status" => 0, "message" => "Diet chart Not Found ");
    }

    /* public function actionGetUsersDetails(){
      $from_date = isset($_REQUEST["from_date"]) ? $_REQUEST["from_date"] : "";
      $to_date = isset($_REQUEST["to_date"]) ? $_REQUEST["to_date"] : "";
      $trainer_id = isset($_REQUEST["trainer_id"]) ? $_REQUEST["trainer_id"] : "";
      if($from_date == "" || $to_date == "" || $trainer_id == ""){
      return array("status"=>0,"message"=>"Date range & logged_id required");
      }
      $dates = array($from_date);
      while(1){
      $date = date('d-m-Y', strtotime(end($dates).' +1 day'));
      $dates[] = $date;
      if($date == $to_date)
      break;
      }
      $users = \common\models\CenterUser::find()
      ->where("nutritionist_id = :nutritionist_id AND in_que = :in_que",[":nutritionist_id"=>$trainer_id,"in_que"=>1])->select(['id','first_name','last_name'])
      ->all();
      $result = array();
      foreach ($users as $user){
      $res = array();
      $res['id'] = $user->id;
      $res['name'] = $user->first_name." ".$user->last_name;
      $last_intake = \common\models\UserFoodIntake::find()
      ->where("user_id=:user_id",[':user_id'=>$user->id])
      ->orderBy("date DESC")
      ->select(['date'])->one();
      $res['last_update'] = isset($last_intake->date) ? $last_intake->date : "";
      $diet_charts = DietChart::find()
      ->select(["date"])
      ->where("user_id = :user_id AND date >= :from_date AND date <= :to_date",['from_date'=>$from_date,"to_date"=>$to_date,"user_id"=>$user->id])
      ->all();
      $dates_f = \yii\helpers\ArrayHelper::map($diet_charts, 'date', 'date');
      $td_arr = array();
      foreach ($dates as $date){
      $td_arr[$date] = in_array($date, $dates_f) ? 1 : 0;
      }
      $res['work_out'] = $td_arr;
      $result[] = $res;
      }
      return array("status"=>1,"message"=>"user data fetched successfulyy","result"=>$result);
      } */

    public function actionGetUsersDetails() {
        $company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : date("d-m-Y", strtotime('monday this week'));
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date("d-m-Y", strtotime('sunday this week'));
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $dates = array();
        $current = strtotime($from_date);
        $last = strtotime($to_date);

        while ($current <= $last) {

            $dates[] = date("d-m-Y", $current);
            $current = strtotime("+1 day", $current);
        }

        $user_tbl = \common\models\CenterUser::tableName();
        $diet_tbl = DietChart::tableName();
        $intake_tbl = \common\models\UserFoodIntake::tableName();
        $user_sql = "SELECT u.id,CONCAT(u.first_name,' ',u.last_name) name,d.id diet_id,d.date diet_date,f.id intake_id "
                . "FROM $user_tbl u LEFT JOIN $diet_tbl d ON u.id = d.user_id AND STR_TO_DATE(d.date,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(d.date,'%d-%m-%Y') <= '$to_date'"
                . "LEFT JOIN $intake_tbl f ON f.diet_id = d.id "
                . " WHERE u.center_id = $company_id AND u.diet_in_queue";
        if (in_array(\Yii::$app->user->identity->role, array(3, 4, 5)))
            $user_sql.= " AND (u.general_trainer_id = '$user_id' OR u.personal_trainer_id = '$user_id' OR u.nutritionist_id = '$user_id')";
        else
            $user_sql.= " AND u.nutritionist_id";
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($user_sql);
        $data = $query_bld->queryAll();
        $result = [];
        foreach ($data as $row) {
            $status = -1;
            if (!is_null($row['diet_id']))
                $status = 0;
            elseif (!is_null($row['intake_id']))
                $status = 1;
            $result[$row['id']]['id'] = $row['id'];
            $result[$row['id']]['name'] = $row['name'];
            if (!isset($result[$row['id']]['diet_date'])) {
                foreach ($dates as $dt) {
                    $result[$row['id']]['diet_date'][$dt] = -1;
                }
            }
            if (!is_null($row['diet_date']))
                $result[$row['id']]['diet_date'][$row['diet_date']] = $status;
        }
        return ['status' => 1, "message" => "Diest list of users Success", "result" => $result];
    }

    public function actionGetStatistics() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $user_id = ($user_id == '' && isset(\Yii::$app->user->identity->id)) ? \Yii::$app->user->identity->id : $user_id;
        if ($user_id == '')
            return ["status" => 0, "message" => "user id not found-unauth Access"];
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : $first_day_this_month;
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : $last_day_this_month;
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        
        $chart_prepared = DietChart::find()
                ->where("user_id = :user_id AND STR_TO_DATE(`date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`date`,'%d-%m-%Y') <= '$to_date'",[":user_id"=>$user_id])
                ->select(["chart_prepared"=>"count(1)"])
                ->groupBy(["user_id"])->asArray()->one();
        $as_per_trainer = \common\models\UserFoodIntake::find()
                ->where("user_id = :user_id AND diet_id AND as_per_trainer AND STR_TO_DATE(`date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`date`,'%d-%m-%Y') <= '$to_date'",[":user_id"=>$user_id])
                ->select(["as_per_diet"=>"count(1)"])
                ->groupBy(["user_id"])->asArray()->one();
        $not_as_per_trainer = \common\models\UserFoodIntake::find()
                ->where("user_id = :user_id AND diet_id AND !as_per_trainer AND STR_TO_DATE(`date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`date`,'%d-%m-%Y') <= '$to_date'",[":user_id"=>$user_id])
                ->select(["not_as_per_diet"=>"count(1)"])
                ->groupBy(["user_id"])->asArray()->one();
//        $no_action["no_action"] = $chart_prepared['chart_prepared'] - ($as_per_trainer['as_per_diet'] + $not_as_per_trainer['not_as_per_diet']);
//        $no_action["no_action"] = max($no_action["no_action"],0);
        $result = [];
        $result['chart_prepared'] = isset($chart_prepared['chart_prepared']) ? $chart_prepared['chart_prepared'] : 0;
        $result['as_per_diet'] = isset($as_per_trainer['as_per_diet']) ? $as_per_trainer['as_per_diet'] : 0;
        $result['not_as_per_diet'] = isset($as_per_trainer['not_as_per_diet']) ? $as_per_trainer['not_as_per_diet'] : 0;
        $result['no_action'] = $chart_prepared['chart_prepared'] - ($as_per_trainer['as_per_diet'] + $not_as_per_trainer['not_as_per_diet']);
        $result["no_action"] = max($result["no_action"],0);
        
        return ["status"=>1,"message"=>"Diet Statistics fetched successfully",
            "result"=>  $result
            ];
    }
    public function actionReport(){
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        if($user_id == '')
            $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';
        if($user_id == '')
            return ['status'=>0,'message'=>"user_id required"];
        $first_day_this_month = date('01-m-Y'); // hard-coded '01' for first day
        $last_day_this_month  = date('t-m-Y');
        $today = date('Y-m-d');
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : $first_day_this_month;
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : $last_day_this_month;
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $dietTbl = DietChart::tableName();
        $data = DietChart::find()
                ->select(["$dietTbl.id","$dietTbl.date","$dietTbl.pre_breakfast","$dietTbl.breakfast","$dietTbl.later_morning_meal","$dietTbl.lunch","$dietTbl.late_afternoon","$dietTbl.evening_tea","$dietTbl.dinner","$dietTbl.post_dinner"])
                ->where("$dietTbl.user_id = $user_id AND STR_TO_DATE($dietTbl.date,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE($dietTbl.date,'%d-%m-%Y') <= '$to_date'")
                ->asArray()
//                ->joinWith('intake')
                ->all();
        $result = [];
        foreach ($data as $row){
            $in_arr = $row;
            $inatake = \common\models\UserFoodIntake::find()
                    ->select(["meal_type","food_intake","as_per_trainer"])
                    ->where("diet_id = {$row['id']}")
//                    ->asArray()
                    ->all();
            $marked = (strtotime($row['date']) < time()) ? "Skipped" : "Pending";
            $as_trainer = 0;
            if(is_array($inatake) && count($inatake)){
                $marked = "Done";
                foreach ($inatake as $irow){
                    if($irow['as_per_trainer'] == 1){
                        $as_trainer = 1;
                        continue;
                    }
                    $foods = @unserialize($irow->food_intake);
                     $take_food = "";
                    if($foods && is_array($foods)){
                       
                        if(isset($foods['qty']) && count($foods['qty'])){
                            for($i=0;$i<count($foods['qty']);$i++){
                                if(isset($foods['qty'][$i]))
                                    $take_food .= " ".$foods['qty'][$i];
                                if(isset($foods['unit'][$i]))
                                    $take_food .= " ".$foods['unit'][$i];
                                if(isset($foods['item'][$i]))
                                    $take_food .= " ".$foods['item'][$i];
                                $take_food.=" ,";
                            }
                        }
                        $in_arr[$irow->meal_type] = rtrim($take_food,',');
                    }
                }
            }
//            $in_arr['as_trainer'] = 1;
            $in_arr['marked'] = $marked;
            $result[] = $in_arr;
        }
        return ['status'=>1,'message'=>"Succesfully diet report done","result"=>$result];
    }
}
