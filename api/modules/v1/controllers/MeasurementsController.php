<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\UserMeasurements;
use common\models\CenterUser;
use common\models\MemberFat;
use common\models\MemberStrength;
use common\models\MemberFlexibility;
use common\models\MemberEndurance;
use common\models\Notification;
use common\models\UserDetails;
use yii\db\Query;


/*sdasd*/
//error_reporting(0);
class MeasurementsController extends RestController
{

    public function behaviors() {

        $no_auth_actions = ['getMeasurementProgress'];
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except'=> $no_auth_actions
        ];
        return $behaviors;
    }
    
    public function actions() {
        $actions_c = [
            'addEditMeasurement' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddEditMeasurement']
            ],
            'getUserMeasurements' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUserMeasurements']
            ],
             'getMeasurementDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetMeasurementDetails']
            ],
             'getMeasurementProgress' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetMeasurementProgress']
            ],
            'getUsersDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUsersDetails']
            ],
            'addFitnessComponents' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddFitnessComponents']
            ],
            'getFitnessComponents' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetFitnessComponents']
            ],
             'addFatPercentage' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddFatPercentage']
            ],
            'getMeasurementList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetMeasurementList']
            ],
            'getUserList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUserList']
            ],
            'getComponentList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetComponentList']
            ],    
             'getComponentProgress' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetComponentProgress']
            ], 
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }
    
    public function actionGetUserMeasurements()
    {
       $details_result    = array();
       $status;
       $message;
       $user_id           =  isset($_REQUEST['user_id'])?$_REQUEST['user_id']:''; 
       
       if(!empty($user_id))
       {
           $measurement_model  = UserMeasurements::find('id,due_date,added_on,added_by')
                               ->where('user_id = :user_id', ['user_id'=>$user_id])
                               ->orderBy('added_on desc')
                               ->all();
           
           if(!empty($measurement_model))
           {
                foreach($measurement_model as $val)
                {
                    $deatil['measurement_id']    =    $val['id'];
                    $deatil['due_date']          =    $val['due_date'];
                    $deatil['added_on']          =    $val['added_on'];

                    $user_model                  =   CenterUser::find('first_name,last_name')
                                                     ->where('id = :id', ['id'=>$user_id])
                                                     ->one();

                    $deatil['added_by']          =   $user_model['first_name'].' '.$user_model['last_name']; 

                    array_push($details_result, $deatil);

                }
                
                $status   = 1;
                $message  = "Measurement fetched sucessfully!";
           }
           else{
                 $status  = 0;
                 $message = "There are no measurements stored ";
               
           }
       }
       else{
           
             $status   = 0;
             $message  = "Please provide user id";
           
       }
       
       $result   =  array("status"=>$status,"message"=>$message,"result"=>$details_result);
       return $result;
       
    }
    
    
    public function actionGetMeasurementDetails()
    {
       $details_result    = array();
       $status;
       $message;
       $user_id     =  isset($_REQUEST['user_id'])?$_REQUEST['user_id']:''; 
       
       if(!empty($user_id))
       {
        $user_infos = CenterUser::find()
                ->select(["first_name","last_name"])
               ->where("id = :id",[":id"=>$user_id])->one();
        $user_infos_details = UserDetails::find()
                ->select(["avtar"])
               ->where("user_id = :id",[":id"=>$user_id])->one();
        $details_result['user_name']            = isset($user_infos->first_name) ? $user_infos->first_name : "";
//                $details_result['user_name']            .= isset($user_infos->last_name) ? " ".$user_infos->last_name : "";
        $details_result['user_avtar']           = isset($user_infos_details->avtar) ? $user_infos_details->avtar : "";
       $measurement_model  = UserMeasurements::find()
               ->select(["notes","goal","weight","height","neck","shoulder","chest_in","chest_exp","u_abd","l_abd","waist","hips","u_thighs","l_thighs","calves",
                   "right_u_arm","right_l_arm","left_u_arm","left_l_arm","wrist","rhr","due_date","user_id","id","updated_on"])
               ->where('user_id = :user_id', ['user_id'=>$user_id])->orderBy("id DESC")->one();
       if(!empty($measurement_model))
       {
                // current user's info
//                echo "<pre>";
//                var_dump($measurement_model->userdetails);
//                exit;
//                $details_result['user_name']            = isset($measurement_model->userdata->first_name) ? $measurement_model->userdata->first_name : "";
//                $details_result['user_name']            .= isset($measurement_model->userdata->last_name) ? " ".$measurement_model->userdata->last_name : "";
//                $details_result['user_avtar']           = isset($measurement_model->userdetails->avtar) ? $measurement_model->userdetails->avtar : "";
                $details_result['notes']                =  $measurement_model->notes;
                $details_result['goal']                 =  $measurement_model->goal;
                $details_result['weight']               =  $measurement_model->weight;
                $details_result['height']               =  $measurement_model->height;
                $details_result['neck']                 =  $measurement_model->neck;
                $details_result['shoulder']             =  $measurement_model->shoulder;
                $details_result['chest_in']             =  $measurement_model->chest_in;
                $details_result['chest_exp']            =  $measurement_model->chest_exp;
                $details_result['u_abd']                =  $measurement_model->u_abd;
                $details_result['l_abd']                =  $measurement_model->l_abd;
                $details_result['waist']                =  $measurement_model->waist;
                $details_result['hips']                 =  $measurement_model->hips;
                $details_result['u_thighs']             =  $measurement_model->u_thighs;
                $details_result['l_thighs']             =  $measurement_model->l_thighs;
                $details_result['calves']               =  $measurement_model->calves;
                $details_result['right_u_arm']          =  $measurement_model->right_u_arm;
                $details_result['right_l_arm']          =  $measurement_model->right_l_arm;
                $details_result['left_u_arm']           =  $measurement_model->left_u_arm;
                $details_result['left_l_arm']           =  $measurement_model->left_l_arm;
                $details_result['wrist']                =  $measurement_model->wrist;
                $details_result['rhr']                  =  $measurement_model->rhr;
                $details_result['due_date']             =  date("d M Y",  strtotime($measurement_model->due_date));
                $details_result['user_id']              =  $measurement_model->user_id;
                $details_result['measurement_id']       =  $measurement_model->id;
                $details_result['last_updated']         =  date("d M 'Y",  strtotime($measurement_model->updated_on));
                
                $status  = 1;
                $message = "Measurement details fetched successfully!"; 
       }
       else{
           $status  = 0;
           $message = "There is no measurements found for tmhis user!";
       }
       }
       else{
           $status  =  0;
           $message =  "Please provide user id!"; 
       }
       $result = array("status"=>$status,"message"=>$message,"result"=>$details_result);
       return $result;
    }
    
    
    public function actionGetMeasurementList()
    {
       $details_result    = array();
       $final_result      = array();
       $status;
       $message;
       $user_id     =  isset($_REQUEST['user_id'])?$_REQUEST['user_id']:''; 
       $from_date   =  isset($_REQUEST['from_date'])?$_REQUEST['from_date']:'';
       $to_date     =  isset($_REQUEST['to_date'])?$_REQUEST['to_date']:'';
       
       
       
       if(!empty($user_id))
       {
            if(!empty($from_date) && !empty($to_date)) 
            {
                $from_date = date('Y-m-d',  strtotime($from_date));
                $to_date = date('Y-m-d',  strtotime($to_date));
                
                $model  = UserMeasurements::find()       
               ->where('user_id = :user_id AND STR_TO_DATE(`added_on`,"%Y-%m-%d") >= :from_date AND STR_TO_DATE(`added_on`,"%Y-%m-%d") <= :to_date', [':user_id'=>$user_id,':from_date'=>$from_date,':to_date'=>$to_date])
               ->orderBy('added_on')->all();

            }else{
               $model  = UserMeasurements::find()->where('user_id = :user_id', ['user_id'=>$user_id])->orderBy('added_on')->all();
            }
       if(!empty($model))
       {
            foreach($model as $measurement_model)
            {
                $details_result['weight'][]               =  $measurement_model->weight;
                $details_result['height'][]               =  $measurement_model->height;
                $details_result['neck'][]                 =  $measurement_model->neck;
                $details_result['shoulder'][]             =  $measurement_model->shoulder;
                $details_result['chest_in'][]             =  $measurement_model->chest_in;
                $details_result['chest_exp'][]            =  $measurement_model->chest_exp;
                $details_result['u_abd'][]                =  $measurement_model->u_abd;
                $details_result['l_abd'][]                =  $measurement_model->l_abd;
                $details_result['waist'][]                =  $measurement_model->waist;
                $details_result['hips'][]                 =  $measurement_model->hips;
                $details_result['u_thighs'][]             =  $measurement_model->u_thighs;
                $details_result['l_thighs'][]             =  $measurement_model->l_thighs;
                $details_result['calves'][]               =  $measurement_model->calves;
                $details_result['right_u_arm'][]          =  $measurement_model->right_u_arm;
                $details_result['right_l_arm'][]          =  $measurement_model->right_l_arm;
                $details_result['left_u_arm'][]           =  $measurement_model->left_u_arm;
                $details_result['left_l_arm'][]           =  $measurement_model->left_l_arm;
                $details_result['wrist'][]                =  $measurement_model->wrist;
                $details_result['rhr'][]                  =  $measurement_model->rhr;
                $details_result['due_date'][]             =  $measurement_model->due_date;
                $details_result['user_id'][]              =  $measurement_model->user_id;
                $details_result['measurement_id'][]       =  $measurement_model->id;
                $details_result['added_on'][]             =  date('d-m-Y',  strtotime($measurement_model->added_on));
                
                //array_push($final_result,$details_result);
            
            }
            
            $status  = 1;
            $message = "Measurement List fetched successfully!"; 
       }
       else{
           $status  = 0;
           $message = "There is no measurements found for this user!";
       }
       }
       else{
           $status  =  0;
           $message =  "Please provide user id!"; 
       }
       $result = array("status"=>$status,"message"=>$message,"result"=>$details_result);
       return $result; 
    }
    
   
    public function actionAddEditMeasurement()
    {
        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());
        
       
        $status;
        $message;
        
        $measurement['weight']             =  !empty($_REQUEST['weight'])?$_REQUEST['weight']:0;
        $measurement['height']             =  !empty($_REQUEST['height'])?$_REQUEST['height']:0;
        $measurement['neck']               =  !empty($_REQUEST['neck'])?$_REQUEST['neck']:0;
        $measurement['shoulder']           =  !empty($_REQUEST['shoulder'])?$_REQUEST['shoulder']:0;
        $measurement['chest_in']           =  !empty($_REQUEST['chest_in'])?$_REQUEST['chest_in']:0;
        $measurement['chest_exp']          =  !empty($_REQUEST['chest_exp'])?$_REQUEST['chest_exp']:0;
        $measurement['u_abd']              =  !empty($_REQUEST['u_abd'])?$_REQUEST['u_abd']:0;
        $measurement['l_abd']              =  !empty($_REQUEST['l_abd'])?$_REQUEST['l_abd']:0;
        $measurement['waist']              =  !empty($_REQUEST['waist'])?$_REQUEST['waist']:0;
        $measurement['hips']               =  !empty($_REQUEST['hips'])?$_REQUEST['hips']:0;
        $measurement['u_thighs']           =  !empty($_REQUEST['u_thighs'])?$_REQUEST['u_thighs']:0;
        $measurement['l_thighs']           =  !empty($_REQUEST['l_thighs'])?$_REQUEST['l_thighs']:0;
        $measurement['calves']             =  !empty($_REQUEST['calves'])?$_REQUEST['calves']:0;
        $measurement['right_u_arm']        =  !empty($_REQUEST['right_u_arm'])?$_REQUEST['right_u_arm']:0;
        $measurement['right_l_arm']        =  !empty($_REQUEST['right_l_arm'])?$_REQUEST['right_l_arm']:0;
        $measurement['left_u_arm']         =  !empty($_REQUEST['left_u_arm'])?$_REQUEST['left_u_arm']:0;
        $measurement['left_l_arm']         =  !empty($_REQUEST['left_l_arm'])?$_REQUEST['left_l_arm']:0;
        $measurement['wrist']              =  !empty($_REQUEST['wrist'])?$_REQUEST['wrist']:0;
        $measurement['rhr']                =  !empty($_REQUEST['rhr'])?$_REQUEST['rhr']:0;
        $measurement['notes']              =  isset($_REQUEST['notes'])?$_REQUEST['notes']:'';
        $measurement['user_id']            =  isset($_REQUEST['user_id'])?$_REQUEST['user_id']:'';
        $measurement['logged_id']          =  isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:'';
        $measurement['due_date']           =  isset($_REQUEST['due_date'])?$_REQUEST['due_date']:'';
        $measurement['status']             =  1;
        $measurement['action']             =  isset($_REQUEST['action'])?$_REQUEST['action']:'';
        $measurement['measurement_id']     =  isset($_REQUEST['measurement_id'])?$_REQUEST['measurement_id']:'';
        $measurement['goal']               =  isset($_REQUEST['goal'])?$_REQUEST['goal']:'';
        
        // calulate due date
        $edit=NULL;
        $diff=NULL;
        $last_mesurements  = UserMeasurements::find()->where('user_id = :user_id', ['user_id'=>$measurement['user_id']])->orderBy('added_on desc')->one();
        $measurement['action'] = 'add';
       
        if(isset($last_mesurements->due_date) && $measurement['due_date'] != ''){
            $measurement['due_date'] = date('d-m-Y',strtotime($measurement['due_date']));
            $from =    new \DateTime($measurement['due_date']);
            $to   =    new \DateTime(date('d-m-Y'));
            $diff =    $from->diff($to)->format("%r%a");
            if($diff<0)
            {
                $measurement['action'] = "edit";
            }
            else{
                $measurement['action'] = "add";
            }
        }
        $add_measurements = $this->addEditMeasurements($measurement);
        $status           = $add_measurements['status'];
        $message          = $add_measurements['message'];
        $result  = array("status"=>$status,"message"=>$message);
        return $result;
    }
    
    public function addEditMeasurements($measurement=NULL)
    {
        if(!is_null($measurement))
        {
            date_default_timezone_set('Asia/Kolkata');
            $current_timestamp = date('Y-m-d h:i:s', time());
                
            /* all type of user can do$user_model   = CenterUser::find()->where('id = :id', ['id'=>$measurement['user_id']])->one();
            // check if user has assigned any personal trainer
            
            if((isset($user_model->personal_trainer_id) && $user_model->personal_trainer_id!=0)?$measurement['logged_id']==$user_model->personal_trainer_id:TRUE)
            { */
                if($measurement['action']=="add")
                {
                   $model   = new UserMeasurements();
                   $notification_model = new Notification();
                }
                else if($measurement['action']=="edit")
                {
                    $model  = UserMeasurements::find()->where('user_id = :user_id', ['user_id'=>$measurement['user_id']])->orderBy('added_on desc')->one();//UserMeasurements::findOne($measurement['measurement_id']);
                    $notification_model = Notification::find()->where('member_id = :member_id', ['member_id'=>$measurement['user_id']])->orderBy('added_on desc')->one();
                }

                               
            if(isset($model))
            {    
                // assign all parameter 
                if($model->isNewRecord){
                    $last_due = $model
                            ->find()
                            ->where(
                                    "user_id = :user_id AND due_date != :due_date",
                                    [":user_id"=>$measurement['user_id'],":due_date"=>$measurement['due_date']])
                            ->max("STR_TO_DATE(`due_date`,'%d-%m-%Y')");
                    $model->prev_due_date = !is_null($last_due) ? date("d-m-Y",  strtotime($last_due)) : date("d-m-Y");
                }
                
                $userModel  = CenterUser::find()->where('id = :id', ['id'=>$measurement['user_id']])->one();
                if(isset($userModel->general_trainer_id) && !is_null($userModel->general_trainer_id) && $userModel->general_trainer_id!=0)
                {
                    $trainer_id  = $userModel->general_trainer_id;
                }
                else if(isset($userModel->personal_trainer_id) && !is_null($userModel->personal_trainer_id) && $userModel->personal_trainer_id!=0)
                {
                    $trainer_id = $userModel->personal_trainer_id;
                }
                else 
                {
                    $trainer_id = 0;
                }
                
                
                
                $model->weight              = $measurement['weight'];
                $model->height              = $measurement['height'];
                $model->neck                = $measurement['neck'];
                $model->shoulder            = $measurement['shoulder'];
                $model->chest_in            = $measurement['chest_in'];
                $model->chest_exp           = $measurement['chest_exp'];
                $model->u_abd               = $measurement['u_abd'];
                $model->l_abd               = $measurement['l_abd'];
                $model->waist               = $measurement['waist'];
                $model->hips                = $measurement['hips'];
                $model->u_thighs            = $measurement['u_thighs'];
                $model->l_thighs            = $measurement['l_thighs'];
                $model->calves              = $measurement['calves'];
                $model->right_u_arm         = $measurement['right_u_arm'];
                $model->right_l_arm         = $measurement['right_l_arm'];
                $model->left_u_arm          = $measurement['left_u_arm'];
                $model->left_l_arm          = $measurement['left_l_arm'];
                $model->wrist               = $measurement['wrist'];
                $model->rhr                 = $measurement['rhr'];
                $model->due_date            = $measurement['due_date'];
                $model->user_id             = $measurement['user_id'];
                $model->goal                = $measurement['goal'];
                $notification_model->member_id = $measurement['user_id'];
                $notification_model->trainer_id = $trainer_id;
                $notification_model->assessment = 'measurement';
                $notification_model->due_date = $measurement['due_date'];
                
                if($measurement['action']=="add")
                {
                    $model->added_on            = $current_timestamp;
                    $model->added_by            = $measurement['logged_id'];
                    $model->updated_on          = $current_timestamp;
                    $model->updated_by          = $measurement['logged_id'];
                    $notification_model->added_on            = $current_timestamp;
                    $notification_model->added_by            = $measurement['logged_id'];
                    $notification_model->updated_on          = $current_timestamp;
                    $notification_model->updated_by          = $measurement['logged_id'];
                }
                else if($measurement['action']=="edit")
                {
                    $notification_model->updated_on          = $current_timestamp;
                    $notification_model->updated_by          = $measurement['logged_id'];
                }
                $model->notes               = $measurement['notes'];
                $model->status              = $measurement['status'];
                $model->company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
                $transaction = Yii::$app->db->beginTransaction();
                
                if($model->save())
                {
                    if($notification_model->save())
                    {
                        $status   = 1;
                        $message  = ($measurement['action']=="add")?"User measurements saved sucessfully!":"User measurements updated sucessfully!";
                        $transaction->commit();
                    }
                    else{
                        $status = -2;
                        $message = "Error : ".$this->arrayToString($model->getErrors());
                        $transaction->rollBack();
                    }
                    
                    
                }
                else{

                    $status  = 0;
                    $message = "Error : ".$this->arrayToString($model->getErrors());
                    $transaction->rollBack();

                }
            }
            else{
                
                $status  = 0;
                $message = "Please provide valid measurement id!";
            }
         /* all type of user can do }
          else{
                $status  = 0;
                $message = "Only personal trainer can add measurements!";
          }*/
        }
        else{
            $status     = 5;
            $message    = "Please provide all required fields";
        }
        
        $result = array("status"=>$status,"message"=>$message);
        return($result);
    }
    public function actionGetMeasurementProgress(){
        if(isset(Yii::$app->user->identity) && Yii::$app->user->identity->isUser)
            $user_id = Yii::$app->user->identity->id;
        else
            $user_id = isset ($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : '00-00-0000';
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date('d-m-Y');
        $from_date = date('Y-m-d',  strtotime($from_date));
        $to_date = date('Y-m-d',  strtotime($to_date));
        $fields = isset ($_REQUEST['fields']) ? $_REQUEST['fields'] : "";
        if($fields == ""){
            return array('status'=>0,'message'=>'Fields is mandatory');
        }
        $model = new UserMeasurements();
        $field_list = explode(",", $fields);
        $exist_flds = array();
        $non_exist_flds = "";
        foreach ($field_list as $field){
            $field = trim($field);
            if($model->hasAttribute($field)){
                $exist_flds[] = $field;
            }else{
                $non_exist_flds .= "'$field' ,";
            }
        }
        $non_exist_flds = trim($non_exist_flds,",");
        if(!count($exist_flds)){
            return array('status'=>0,'message'=>'All fields are invalid or not given',"fields"=>$non_exist_flds);
        }
        $exist_flds[] = 'due_date';
        $data = $model->find()
                ->where('user_id = :user_id AND STR_TO_DATE(`due_date`,"%d-%m-%Y") >= :from_date AND STR_TO_DATE(`due_date`,"%d-%m-%Y") <= :to_date', [':user_id'=>$user_id,':from_date'=>$from_date,':to_date'=>$to_date])
                ->select($exist_flds)
                ->all();
        $result = array();
        foreach ($data as $row){
            foreach ($exist_flds as $fld){
                $result[$fld][] = ($fld == "due_date") ? date('jS M',  strtotime($row->$fld)) : $row->$fld;
            }
        }
        $return = array();
        $return["status"] = 1;
        $return["messsage"] = "user's progress fetched successfully";
        $return["result"] = $result;
        if($non_exist_flds != "")
            $return["wrong_fileds"] = $non_exist_flds;
        return $return;
        
    }
    public function actionGetUsersDetails(){
        $trainer_id = isset($_REQUEST["trainer_id"]) ? $_REQUEST["trainer_id"] : (isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : "");
        if($trainer_id == ""){
            return array("status"=>0,"message"=>"trainer_id is required");
        }
        $monday = date("Y-m-d",strtotime('monday this week'));
        $sunday = date("Y-m-d",strtotime('sunday this week'));
        $user_tbl = CenterUser::tableName();
        $msr_tbl = UserMeasurements::tableName();
        $fld = 'personal_trainer_id';
        if(isset(Yii::$app->user->identity) && Yii::$app->user->identity->isGeneralTrainer)
            $fld = 'general_trainer_id';
        $user_sql = "SELECT u.id,CONCAT(u.first_name,' ',u.last_name) name,IFNULL(MAX(STR_TO_DATE(due_date,'%d-%m-%Y')),'') AS due_date,IFNULL(MAX(STR_TO_DATE(prev_due_date,'%d-%m-%Y')),'') AS prev_due_date, DATE(m.updated_on) AS updated_on
                        FROM $user_tbl u
                        LEFT JOIN $msr_tbl m ON m.user_id = u.id
                        WHERE u.$fld = $trainer_id
                        GROUP BY u.id";
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($user_sql);
        $data = $query_bld->queryAll();
        $res = array();
        foreach ($data as $rec){
            $status = 0;
            if($rec['due_date'] != '' && $rec['prev_due_date'] != ''){
                $prv_due = strtotime($rec['prev_due_date']);
                $due = strtotime($rec['due_date']);
                $updated = strtotime($rec['due_date']);
                if($updated >= $prv_due && $updated <= $due)
                    $status = 1;
            }
            $res[] = ['id'=>$rec['id'],'name'=>$rec['name'],'status'=>$status,'due_date'=>$rec['due_date'],'last_update'=>$rec['prev_due_date']];
        }
        return array("status"=>1,"message"=>"Fetched success fully","result"=>$res);
    }
    
    public function actionAddFitnessComponents()
    {
        
        $status;
        $message;
        
        $component   =  isset($_REQUEST['component'])?$_REQUEST['component']:'';
        $company_id  =  isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $member_id   =  isset($_REQUEST['member_id'])?$_REQUEST['member_id']:'';
        $due_date    =  isset($_REQUEST['due_date'])?$_REQUEST['due_date']:'';
        $logged_id   =  isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:'';
        
        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());
        
        $last_strength  = MemberStrength::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
        $action            = 'add';
       
            
        $from =    new \DateTime($due_date);
        $to   =    new \DateTime(date('d-m-Y'));
        if(isset($last_strength->due_date) && $due_date!= ''){
        $diff =    $from->diff($to)->format("%r%a");
            if($diff<0)
            {
                $action = "edit";
            }
            else{
                $action = "add";
            }
                         
        }
        
        $transaction = Yii::$app->db->beginTransaction();
         
        if(!empty($logged_id))
        {
            if(!empty($component) && !empty($company_id) && !empty($member_id))
            {
                if($component =='strength')
                {
                    $bench_press1   = (isset($_REQUEST['bench_press1']) && !empty($_REQUEST['bench_press1']))?$_REQUEST['bench_press1']:0;
                    $bench_press2   = (isset($_REQUEST['bench_press2']) && !empty($_REQUEST['bench_press2']))?$_REQUEST['bench_press2']:0;
                    $bench_press3   = (isset($_REQUEST['bench_press3']) && !empty($_REQUEST['bench_press3']))?$_REQUEST['bench_press3']:0;
                    $legpress1       = (isset($_REQUEST['legpress1']) && !empty($_REQUEST['legpress1']))?$_REQUEST['legpress1']:0;
                    $legpress2       = (isset($_REQUEST['legpress2']) && !empty($_REQUEST['legpress2']))?$_REQUEST['legpress2']:0;
                    $legpress3       = (isset($_REQUEST['legpress3'])&& !empty($_REQUEST['legpress3']))?$_REQUEST['legpress3']:0;
                    
                    if($action=="add")
                    {
                     $model          =  new MemberStrength();
                     $notification_model = new Notification();
                    }
                    else if($action=="edit")
                    {
                        $model       =  MemberStrength::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                        $notification_model = Notification::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                        
                    }
                    
                    $userModel  = CenterUser::find()->where('id = :id', ['id'=>$member_id])->one();
                    if(isset($userModel->general_trainer_id) && !is_null($userModel->general_trainer_id) && $userModel->general_trainer_id!=0)
                    {
                        $trainer_id  = $userModel->general_trainer_id;
                    }
                    else if(isset($userModel->personal_trainer_id) && !is_null($userModel->personal_trainer_id) && $userModel->personal_trainer_id!=0)
                    {
                        $trainer_id = $userModel->personal_trainer_id;
                    }
                    else 
                    {
                        $trainer_id = 0;
                    }
                    $model->bench_press1  = $bench_press1;
                    $model->bench_press2  = $bench_press2;
                    $model->bench_press3  = $bench_press3;
                    $model->legpress1     = $legpress1;
                    $model->legpress2     = $legpress2;
                    $model->legpress3     = $legpress3;
                    $model->due_date      = $due_date;
                    $model->member_id     = $member_id;
                    $model->company_id    = $company_id;
                    $notification_model->member_id = $member_id;
                    $notification_model->trainer_id =$trainer_id;
                    $notification_model->due_date = $due_date;
                    $notification_model->assessment = "fitness_component";
                    
                    
                    if($action=="add")
                    {
                         $model->added_on      = $current_timestamp;
                         $model->added_by      = $logged_id;
                         $model->updated_on      = $current_timestamp;
                         $model->updated_by      = $logged_id; 
                         $notification_model->added_on  = $current_timestamp;
                         $notification_model->added_by  = $logged_id;
                         $notification_model->updated_on = $current_timestamp;
                         $notification_model->updated_by = $logged_id; 
                    }
                    else if($action=="edit")
                    {
                       $model->updated_on      = $current_timestamp;
                       $model->updated_by      = $logged_id;
                       $notification_model->updated_on = $current_timestamp;
                       $notification_model->updated_by = $logged_id;
                    }
                    
                }
                else if($component =='endurance')
                {
                    $last_endurance  = MemberEndurance::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                    if(!isset($last_endurance))
                    {
                        $action            = 'add';
                    }
                    
                    $knee_sit_repetition  = (isset($_REQUEST['knee_sit_repetition']) && !empty($_REQUEST['knee_sit_repetition']))?$_REQUEST['knee_sit_repetition']:0;
                    $knee_sit_time        = (isset($_REQUEST['knee_sit_time']) && !empty($_REQUEST['knee_sit_time']))?$_REQUEST['knee_sit_time']:0;
                    $push_up_repetition   = (isset($_REQUEST['push_up_repetition']) && !empty($_REQUEST['push_up_repetition']))?$_REQUEST['push_up_repetition']:0;
                    $push_up_time         = (isset($_REQUEST['push_up_time']) && !empty($_REQUEST['push_up_time']))?$_REQUEST['push_up_time']:0;
                    $core                 = (isset($_REQUEST['core']) && !empty($_REQUEST['core']))?$_REQUEST['core']:0;
                    $step_test1           = isset($_REQUEST['step_test1'])?$_REQUEST['step_test1']:'';
                    $step_test2           = isset($_REQUEST['step_test2'])?$_REQUEST['step_test2']:'';
                    $step_test3           = isset($_REQUEST['step_test3'])?$_REQUEST['step_test3']:'';
                    $step_test4           = isset($_REQUEST['step_test4'])?$_REQUEST['step_test4']:'';

                    if($action=="add")
                    {
                     $model          =  new MemberEndurance();
                    }
                    else if($action=="edit")
                    {
                        $model       =  MemberEndurance::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                    }
                    
                    
                    $model->knee_sit_repetition  = $knee_sit_repetition;
                    $model->knee_sit_time        = $knee_sit_time;
                    $model->push_up_repetition   = $push_up_repetition;
                    $model->push_up_time         = $push_up_time;
                    $model->core                 = $core;
                    $model->step_test1           = $step_test1;
                    $model->step_test2           = $step_test2;
                    $model->step_test3           = $step_test3;
                    $model->step_test4           = $step_test4;
                    $model->member_id     = $member_id;
                    $model->company_id    = $company_id;
                    if($action=="add")
                    {
                        $model->added_on  = $current_timestamp;
                        $model->added_by  = $logged_id;
                        $model->updated_on = $current_timestamp;
                        $model->updated_by = $logged_id; 
                    }
                    else if($action=="edit")
                    {
                       $model->updated_on      = $current_timestamp;
                       $model->updated_by      = $logged_id; 
                    }

                }
                else if($component =='flexibility')
                {
                    $last_flexibility  = MemberFlexibility::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                    if(!isset($last_flexibility))
                    {
                        $action            = 'add';
                    }
                    
                    $sit_reach         =  (isset($_REQUEST['sit_reach']) && !empty($_REQUEST['sit_reach']))?$_REQUEST['sit_reach']:0;
                    $shoulder_left     =  (isset($_REQUEST['shoulder_left']) && !empty($_REQUEST['shoulder_left']))?$_REQUEST['shoulder_left']:0;
                    $shoulder_right    =  (isset($_REQUEST['shoulder_right']) && !empty($_REQUEST['shoulder_right']))?$_REQUEST['shoulder_right']:0;
                    $left_leg_standing =  (isset($_REQUEST['left_leg_standing']) && !empty($_REQUEST['left_leg_standing']))?$_REQUEST['left_leg_standing']:0;
                    $right_leg_standing=  (isset($_REQUEST['right_leg_standing']) && !empty($_REQUEST['right_leg_standing']))?$_REQUEST['right_leg_standing']:0;

                    if($action=="add")
                    {
                     $model          =  new MemberFlexibility();
                    }
                    else if($action=="edit")
                    {
                        $model       =  MemberFlexibility::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                    }
                    
                    $model->sit_reach     = $sit_reach;
                    $model->shoulder_left = $shoulder_left;
                    $model->shoulder_right = $shoulder_right;
                    $model->left_leg_standing = $left_leg_standing;
                    $model->right_leg_standing = $right_leg_standing;
                    $model->member_id     = $member_id;
                    $model->company_id    = $company_id;
                    if($action=="add")
                    {
                         $model->added_on      = $current_timestamp;
                         $model->added_by      = $logged_id;
                         $model->updated_on      = $current_timestamp;
                       $model->updated_by      = $logged_id; 
                    }
                    else if($action=="edit")
                    {
                       $model->updated_on      = $current_timestamp;
                       $model->updated_by      = $logged_id; 
                    }

                }

                if($model->save())
                {
                    $status = 1;
                    if($component=="strength")
                    {
                        if($notification_model->save())
                        {
                            $transaction->commit();
                            $message = 'Strength saved sucessfully!';
                        }
                        else{
                            $transaction->rollback();
                            $status = -2;
                            $message =  "Error : ".$this->arrayToString($notification_model->getErrors());
                            
                        }
                    }else{
                    
                    $transaction->commit();
                    switch($component)
                    {

                        case 'endurance':  $message = 'Endurance saved sucessfully!';
                                           break;

                        case 'flexibility' : $message = 'Flexibility Saved sucessfully!';
                                             break;

                    }
                    }
                }
                else{
                    $transaction->rollback();
                    $status = -1;
                    $message = "Error : ".$this->arrayToString($model->getErrors());
                }
            }
            else{
                $status  = -2;
                $message = "Please provide member id, company id and fitness component";
            }
        }else{
            
            $status  = -3;
            $message = "Your session has expired. Please login again!";
        }
        
        $result  = array('status'=>$status,'message'=>$message);
        return($result);
    }
    
    public function actionAddFatPercentage()
    {
        $status=1;
        $message;
        $fat_percentage = NULL;
        $company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";

        $calculation_type =  isset($_REQUEST['calculation_type'])?$_REQUEST['calculation_type']:'';
        $chest            =  (isset($_REQUEST['chest']) && !empty($_REQUEST['chest']))?$_REQUEST['chest']:0;
        $abd              =  (isset($_REQUEST['abd']) && !empty($_REQUEST['abd']))?$_REQUEST['abd']:0;
        $thigh            =  (isset($_REQUEST['thigh']) && !empty($_REQUEST['thigh']))?$_REQUEST['thigh']:0;
        $triceps          =  (isset($_REQUEST['triceps']) && !empty($_REQUEST['triceps']))?$_REQUEST['triceps']:0;
        $subscapular      =  (isset($_REQUEST['subscapular']) && !empty($_REQUEST['subscapular']))?$_REQUEST['subscapular']:0;
        $suprailliac      =  (isset($_REQUEST['suprailliac']) && !empty($_REQUEST['suprailliac']))?$_REQUEST['suprailliac']:0;
        $mid_axillary     =  (isset($_REQUEST['mid_axillary']) && !empty($_REQUEST['mid_axillary']))?$_REQUEST['mid_axillary']:0;
        $bicep            =  (isset($_REQUEST['bicep']) && !empty($_REQUEST['bicep']))?$_REQUEST['bicep']:0;
        $lower_back       =  (isset($_REQUEST['lower_back']) && !empty($_REQUEST['lower_back']))?$_REQUEST['lower_back']:0;
        $calf             =  (isset($_REQUEST['calf']) && !empty($_REQUEST['calf']))?$_REQUEST['calf']:0;
       // $company_id  =  isset($_REQUEST['company_id'])?$_REQUEST['company_id']:'';
        $member_id   =  isset($_REQUEST['member_id'])?$_REQUEST['member_id']:'';
        $due_date    =  isset($_REQUEST['due_date'])?$_REQUEST['due_date']:'';
        $logged_id   =  isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:'';
        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());
        
        
        $last_strength  = MemberStrength::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
        $action            = 'add';
       
        if(isset($last_strength->due_date) && $due_date!= ''){
            
            $from =    new \DateTime($due_date);
            $to   =    new \DateTime(date('d-m-Y'));
            $diff =    $from->diff($to)->format("%r%a");
            if($diff<0)
            {
                $action = "edit";
            }
            else{
                $action = "add";
            }
                         
        }
        
        if(!empty($logged_id))   
        {
            $detail_model   = UserDetails::find()->where('user_id = :user_id', ['user_id'=>$member_id])->one();
            if(isset($detail_model))
            {
                $gender   = $detail_model->gender;
                $dob      = $detail_model->dob;
                $from     = new \DateTime($dob);
                $to       = new \DateTime('today');
                $age      = $from->diff($to)->y;
            }
            else{
                $gender  = '';
                $age     = '';
            }



            if($calculation_type=='jacksonpollock7')
            {   
                if($chest!=0 && $abd!=0 && $thigh!=0 && $triceps!=0 && $subscapular!=0 && $suprailliac!=0 && $mid_axillary)
                {
                    $sum_skinfold    = $chest+$abd+$thigh+$triceps+$subscapular+$suprailliac+$mid_axillary;
                    $fat_percentage  = ($gender==1)?(495/(1.112-(0.00043499*$sum_skinfold)+(0.00000055*$sum_skinfold*$sum_skinfold)-(0.00028826*$age))-450):(495/(1.097-(0.00046971*$sum_skinfold)+(0.00000056*$sum_skinfold*$sum_skinfold)-(0.00012828*$age))-450);
                }
                else{
                    $status  = 0;
                    $message = "Please enter all 7 fields!";
                }
            }
            else if($calculation_type=='jacksonpollock3')
            {
                if($chest!=0 && $abd!=0 && $thigh!=0)
                {
                    $sum_skinfold    = $chest+$abd+$thigh;
                    $fat_percentage  = ($gender==1)?(495/(1.10938-(0.0008267*$sum_skinfold)+(0.0000016*$sum_skinfold*$sum_skinfold)-(0.0002574*$age))-450):(495/(1.089733-(0.0009245*$sum_skinfold)+(0.0000025*$sum_skinfold*$sum_skinfold)-(0.0000979*$age))-450);
                }
                else{
                    $status  = 0;
                    $message = "Please enter all 3 fields!";            
                }
            }
            else if($calculation_type=='jacksonpollock4')
            {
                if($abd!=0 && $triceps!=0 && $thigh!=0 && $suprailliac!=0)
                {
                    $sum_skinfold    = $abd+$triceps+$thigh+$suprailliac;
                    $fat_percentage  = ($gender==1)?((0.29288 * $sum_skinfold) - (0.0005 * $sum_skinfold * $sum_skinfold) + (0.15845 * $age) - 5.76377):((0.29669 * $sum_skinfold) - (0.00043 * $sum_skinfold * $sum_skinfold) + (0.02963 * $age) + 1.4072);
                }  
                else{
                    $status = 0;
                    $message = "Please enter all 4 fields!";
                }
            }
            else if($calculation_type=='parrillocaliper')
            {
                    // get the weight and height from measurement or physical details table to claculate calorie
                    $mesurements  = UserMeasurements::find()->where('user_id = :user_id', ['user_id'=>$member_id])->orderBy('added_on desc')->one();

                    // if user did'nt fill measurements details
                    if(is_null($mesurements))
                    {
                            $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id'=>$user_id])->one();
                            if(!is_null($mesurements))
                            {
                                $weight  = $mesurements->current_weight;
                                $height  = $mesurements->height;
                            }
                            else{
                            $weight  = NULL;
                            $height  = NULL;
                        }
                    }
                    else{
                        if(!empty($mesurements->weight) && !empty($mesurements->height) && $mesurements->weight!=0 && $mesurements->height!=0)
                        {
                            $weight  = $mesurements->weight;
                            $height  = $mesurements->height;
                        }
                        else{
                        $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id'=>$user_id])->one();
                        if(!is_null($mesurements))
                        {
                            $weight  = $mesurements->weight;
                            $height  = $mesurements->height;
                        }
                        else{
                            $weight  = NULL;
                            $height  = NULL;
                        }
                    }                      
                }

                if(!is_null($weight) && $age!=0 && !empty($age))
                {

                    if($chest!=0 && $abd!=0 && $thigh!=0 && $bicep!=0 && $triceps!=0 && $subscapular!=0 && $suprailliac!=0 && $lower_back!=0 && $calf!=0)
                    {
                        $sum_skinfold    = $chest+$abd+$thigh+$bicep+$triceps+$subscapular+$suprailliac+$lower_back+$calf;
                        $lbs_weight      = $weight *  2.20462262185;
                        $fat_percentage  = ($sum_skinfold * $age)/$lbs_weight;
                    }else{
                        $status = 0;
                        $message = "Please enter all fields!";
                    }
                }else{
                    $status = 0;
                    $message = "Please update weight and age. It seems incorrect!";
                }
            }
            else if($calculation_type=='durninwomersley')
            {
                if($bicep!=0 && $triceps!=0 && $subscapular!=0 && $suprailliac!=0)
                {
                    $dencity = 0;
                    $sum_skinfold   = $bicep+$triceps+$subscapular+$suprailliac;
                    switch($age)
                    {
                        case $age<17       : $dencity = ($gender==1)?(1.1533 - (0.0643 * (log($sum_skinfold)/log(10)))) : (1.1369 - (0.0598 * (log($sum_skinfold)/log(10))));
                                             break;

                        case ($age>=17 && $age<20) : $dencity = ($gender==1)?(1.1620 - (0.0630 * (log($sum_skinfold)/log(10)))) : (1.1549 - (0.0678 * (log($sum_skinfold)/log(10))));
                                                    break;

                        case ($age>=20 && $age<30) : $dencity = ($gender==1)?(1.1631 - (0.0632 * (log($sum_skinfold)/log(10)))):(1.1599 - (0.0717 * (log($sum_skinfold)/log(10))));
                                                     break;

                        case ($age>=30 && $age<40) : $dencity = ($gender==1)?(1.1422 - (0.0544 * (log($sum_skinfold)/log(10)))):(1.1423 - (0.0632 * (log($sum_skinfold)/log(10))));
                                                     break;

                        case ($age>=40 && $age<50) : $dencity = ($gender==1)?(1.1620 - (0.0700 * (log($sum_skinfold)/log(10)))):(1.1333 - (0.0612 * (log($sum_skinfold)/log(10))));
                                                     break;

                        case ($age>=50)   : $dencity = ($gender==1)?(1.1715 - (0.0779 * (log($sum_skinfold)/log(10)))):(1.1339 - (0.0645 * (log($sum_skinfold)/log(10))));
                                            break;
                    }

                    $fat_percentage = (495/$dencity)- 450;
                }
                else{
                    $status = 0;
                    $message = "Please enter all fields!";
                }
            }else{
                $status = 0;
                $message = "Please give proper calculation type!";

            }

            if($status!=0)
            {
                $last_endurance  = MemberFat::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                if(!isset($last_endurance))
                {
                    $action            = 'add';
                }
                
                $fat_percentage  = round($fat_percentage, 2);
                if($action=="add")
                {
                     $model   = new MemberFat();
                }else{
                     $model = MemberFat::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on desc')->one();
                }
                $model->calculation_type = $calculation_type;
                $model->chest = $chest;
                $model->abd   = $abd;
                $model->thigh = $thigh;
                $model->triceps = $triceps;
                $model->subscapular = $subscapular;
                $model->suprailliac = $suprailliac;
                $model->mid_axillary = $mid_axillary;
                $model->fat_percentage = $fat_percentage;
                if($action=="add")
                {
                    $model->added_on      = $current_timestamp;
                    $model->added_by      = $logged_id;
                    $model->updated_on      = $current_timestamp;
                    $model->updated_by      = $logged_id; 
                }
                else if($action=="edit")
                    {
                       $model->updated_on      = $current_timestamp;
                       $model->updated_by      = $logged_id; 
                }
                $model->member_id  = $member_id;
                $model->company_id =$company_id;

                if($model->save())
                {
                    $status  = 1;
                    $message = "Fat percentage calculated successfully!";
                }
                else{
                    $status = -1;
                    $message ="Problem in saving model";
                }

            }
            else{
                $status =-2;   
            }
        }else{
            $status = -3;
            $message ="Your session has expired. Please login again.";     
        }
       
        $result = array("status"=>$status,"message"=>$message,"fat_percentage"=>$fat_percentage);
        return $result;
    }
    public function actionGetFitnessComponents(){
        $member_id   =  isset($_REQUEST['member_id'])?$_REQUEST['member_id']:'';
        if(trim($member_id) == '')
            return ['status'=>0,"message"=>"member_id is required"];
        $strength = MemberStrength::find()
                ->select(["id",'due_date','updated_on',"bench_press1","bench_press2","bench_press3","legpress1","legpress2","legpress3"])
                ->where("member_id = :member_id",[":member_id" => $member_id])
                ->orderBy("id DESC")
                ->one();
        $endurance = MemberEndurance::find()
                ->select(["id","updated_on","knee_sit_repetition","knee_sit_time","push_up_repetition","push_up_time","core","step_test1","step_test2","step_test3","step_test4"])
                ->where("member_id = :member_id",[":member_id" => $member_id])
                ->orderBy("id DESC")
                ->one();
        $flexibility = MemberFlexibility::find()
                ->select(["id","updated_on","sit_reach","shoulder_left","shoulder_right","left_leg_standing","right_leg_standing"])
                ->where("member_id = :member_id",[":member_id" => $member_id])
                ->orderBy("id DESC")
                ->one();
        $fatpercentage = MemberFat::find()
                ->select(["id","updated_on","calculation_type","chest","abd","thigh","triceps","subscapular","suprailliac","mid_axillary","bicep","lower_back","calf","fat_percentage"])
                ->where("member_id = :member_id",[":member_id" => $member_id])
                ->orderBy("id DESC")
                ->one();
        $return = ["status"=>1, "message" => "Strength fetched successfully", "result" => [
            "strength"    => $strength,
            "endurance"   => $endurance,
            "flexibility" => $flexibility,
            "fat_percentage" => $fatpercentage,
        ]];
        if(isset($strength->due_date))
            $return["result"]["due_date"] = $strength->due_date;
        if(isset($strength->updated_on))
            $return["result"]["last_updated"] = date('d-m-Y',  strtotime($strength->updated_on));
        return $return;
    }
    public function actionGetUserList(){
        $company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        $role = isset(\Yii::$app->user->identity->role) ? \Yii::$app->user->identity->role : "";
        if($user_id == '' || $company_id == "" || $role == "")
            return ["status"=>0,'message'=>"UnAthorised Access"];
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : date("d-m-Y", strtotime('monday this week'));
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date("d-m-Y", strtotime('sunday this week'));
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $userTbl = CenterUser::tableName();
        $strengthTbl = MemberStrength::tableName();
        $msrTbl = UserMeasurements::tableName();
        /*$user_sql = "SELECT u.id,CONCAT(u.first_name,' ',u.last_name) name,DATE_FORMAT(STR_TO_DATE(s.due_date,'%d-%m-%Y'),'%d %M %Y') fc_due_date,DATE_FORMAT(s.updated_on,'%d %M %Y') fc_last_updated,STR_TO_DATE(m.due_date,'%d-%m-%Y') m_due_date,DATE_FORMAT(m.updated_on,'%d %M %Y') m_last_updated 
FROM $userTbl u 
LEFT JOIN $strengthTbl s ON u.id = s.member_id 
LEFT JOIN $msrTbl m ON m.user_id = u.id WHERE u.center_id = '$company_id'";
        if (in_array($role, array(3, 4, 5)))
            $user_sql.= " AND (u.general_trainer_id = '$user_id' OR u.personal_trainer_id = '$user_id')";
        $user_sql.="
GROUP BY u.id
ORDER BY s.id DESC,m.id DESC"; // to day*/
        $extCond = " AND in_que";
        if (in_array($role, array(3, 4, 5)))
            $extCond.= " AND (u.general_trainer_id = '$user_id' OR u.personal_trainer_id = '$user_id')";
        $user_sql = "SELECT {{SELECT}} FROM
(SELECT u.id,CONCAT(u.first_name,' ',u.last_name) name FROM $userTbl u WHERE u.center_id = '$company_id' $extCond) USR
LEFT JOIN 
(SELECT * FROM 
( SELECT s.member_id,s.due_date fc_due_date, DATE_FORMAT(s.updated_on,'%d-%m-%Y') fc_last_updated 
FROM $strengthTbl s WHERE s.company_id = '$company_id' ORDER BY s.id desc ) AS STR 
GROUP BY STR.member_id 
)STRT ON USR.id = STRT.member_id
LEFT JOIN
(SELECT * FROM 
( SELECT m.user_id,m.due_date m_due_date,DATE_FORMAT(m.updated_on,'%d-%m-%Y') m_last_updated 
FROM $msrTbl m WHERE m.company_id = '$company_id' ORDER BY m.id DESC ) AS MSR
GROUP BY MSR.user_id ) MGRT ON USR.id = MGRT.user_id";
//        echo $user_sql;exit;
        // dataprovider pagination
        $conn = Yii::$app->db;
        $totalCount = $conn->createCommand(str_replace("{{SELECT}}", "COUNT(*)", $user_sql))->queryScalar();
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => str_replace("{{SELECT}}", "*", $user_sql),
            'totalCount' => $totalCount,
        ]);
        if ($this->pagination) {
            $dataProvider->pagination = ['pageSize' => $this->pageSize];
        } else {
            $dataProvider->pagination = FALSE;
        }
        $this->status = 1;
        $this->message = "Userlist measurement success";
        return $dataProvider;
        $conn = Yii::$app->db;  
        $query_bld = $conn->createCommand($user_sql);
        $data = $query_bld->queryAll();
        return ['status'=>1,"message"=>"Userlist measurement success","result"=>$data];
    }
    
    
    public function actionGetComponentList()
    {
      //  $type  =  isset($_REQUEST['component'])?$_REQUEST['component']:'';
        $member_id = isset($_REQUEST['member_id'])?$_REQUEST['member_id']:'';
        $from_date = isset($_REQUEST['from_date'])?$_REQUEST['from_date']:'';
        $to_date = isset($_REQUEST['to_date'])?$_REQUEST['to_date']:'';
        $result = array();
        
         if(!empty($from_date) && !empty($to_date)) 
         {
            $from_date = date('Y-m-d',  strtotime($from_date));
            $to_date = date('Y-m-d',  strtotime($to_date));
         }
        
        if(!empty($member_id))
        {
           
                if(!empty($from_date) && !empty($to_date))
                {
                    $model  = MemberStrength::find()       
                   ->where('member_id = :member_id AND STR_TO_DATE(`added_on`,"%Y-%m-%d") >= :from_date AND STR_TO_DATE(`added_on`,"%Y-%m-%d") <= :to_date', [':member_id'=>$member_id,':from_date'=>$from_date,':to_date'=>$to_date])
                   ->orderBy('added_on')->all();
                }
               else{
                    $model  = MemberStrength::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on')->all();
                }
                $strength_count = 0;
                if(isset($model) && !empty($model))
                {
                    foreach($model as $obj)
                    {
                        $result['bench_press1'][]   = $obj->bench_press1;
                        $result['bench_press2'][]   = $obj->bench_press2;
                        $result['bench_press3'][]   = $obj->bench_press3;
                        $result['legpress1'][]   = $obj->legpress1;
                        $result['legpress2'][]   = $obj->legpress2;
                        $result['legpress3'][]   = $obj->legpress3;
                        $result['due_date'][]   = $obj->due_date;
                        $result['added_on'][]   = $obj->added_on;
                     //   $result['updated_on'][]   = $obj->updated_on;
                        
                        $strength_count++;
                      
                    }
                    
                    $flexibility_count=0;
                    $flexibility_model  = MemberFlexibility::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on')->all();
                    if(isset($flexibility_model) && !empty($flexibility_model))
                    {
                       foreach($flexibility_model as $obj)
                       {
                        $result['sit_reach'][]   = $obj->sit_reach;
                        $result['shoulder_left'][]   = $obj->shoulder_left;
                        $result['shoulder_right'][]   = $obj->shoulder_right;
                        $result['left_leg_standing'][]   = $obj->left_leg_standing;
                        $result['right_leg_standing'][]   = $obj->right_leg_standing;
                        $flexibility_count++;
                      
                        }
                    }
                    
                    if($strength_count!=$flexibility_count)
                    {
                        $count = $strength_count - $flexibility_count;
                        for($i=0;$i<$count;$i++)
                        {
                            $result['sit_reach'][]   = 0;
                            $result['shoulder_left'][]   = 0;
                            $result['shoulder_right'][]   = 0;
                            $result['left_leg_standing'][]   = 0;
                            $result['right_leg_standing'][]   = 0;
                        }
                    }
                    $endurance_count = 0;
                    $model_endurance  = MemberEndurance::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on')->all();
                
                    if(isset($model_endurance) && !empty($model_endurance))
                    {
                        foreach($model_endurance as $obj)
                        {
                            $result['knee_sit_repetition'][]   = $obj->knee_sit_repetition;
                            $result['knee_sit_time'][]   = $obj->knee_sit_time;
                            $result['push_up_repetition'][]   = $obj->push_up_repetition;
                            $result['push_up_time'][]   = $obj->push_up_time;
                            $result['core'][]   = $obj->core;
                            $result['step_test1'][]   = $obj->step_test1;
                            $result['step_test2'][]   = $obj->step_test2;
                            $result['step_test3'][]   = $obj->step_test3;
                            $result['step_test4'][]   = $obj->step_test4;
                            $endurance_count++;
                        }
                    }
                    
                    if($strength_count!=$endurance_count)
                    {
                        $count = $strength_count - $endurance_count;
                        for($i=0;$i<$count;$i++)
                        {
                            $result['knee_sit_repetition'][]   = 0;
                            $result['knee_sit_time'][]   = 0;
                            $result['push_up_repetition'][]   = 0;
                            $result['push_up_time'][]   = 0;
                            $result['core'][]   = 0;
                            $result['step_test1'][]   = '';
                            $result['step_test2'][]   = '';
                            $result['step_test3'][]   = '';
                            $result['step_test4'][]   = '';
                        }
                    }
                    
                    $fat_count = 0;
                    $model_fat  = MemberFat::find()->where('member_id = :member_id', ['member_id'=>$member_id])->orderBy('added_on')->all();
                    if(isset($model_fat) && !empty($model_fat))
                    {
                        foreach($model_fat as $obj)
                        {
                            $result['calculation_type'][]   = $obj->calculation_type;
                            $result['fat_percentage'][]   = $obj->fat_percentage;
                            $fat_count++;
                        }
                    }
                    
                    if($strength_count!=$fat_count)
                    {
                        $count = $strength_count - $fat_count;
                        for($i=0;$i<$count;$i++)
                        {
                            $result['calculation_type'][]   = '';
                            $result['fat_percentage'][]   = 0;
                        }
                    }
                    
                    $status = 1;
                    $message = "Components liste sucessfully!";
                    
                }
                else{
                    $status =-2;
                    $message = "No data Available.";
                    
                }
        }
        else{
            $status = -1;
            $message = "Please provide member id";
        }
        
        $final_result = array("status"=>$status,"message"=>$message,"result"=>$result);
        return $final_result;
        
    }
    
    public function actionGetComponentProgress(){
        if(isset(Yii::$app->user->identity) && Yii::$app->user->identity->isUser)
            $user_id = Yii::$app->user->identity->id;
        else
            $user_id = isset ($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : '00-00-0000';
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date('d-m-Y');
        $from_date = date('Y-m-d',  strtotime($from_date));
        $to_date = date('Y-m-d',  strtotime($to_date));
        $fields = isset ($_REQUEST['fields']) ? $_REQUEST['fields'] : "";
        if($fields == "" && empty($fields)){
            return array('status'=>0,'message'=>'Fields are mandatory');
        }
        $model_strength = new MemberStrength();
        $model_endurance = new MemberEndurance();
        $model_flexibility = new MemberFlexibility();
        $model_fat = new MemberFat();
        $field_list = explode(",", $fields);
        $exist_flds = array();
        $exist_flds['strength'] = array();
        $exist_flds['endurance'] = array();
        $exist_flds['flexibility'] = array();
        $exist_flds['fat'] = array();
        $non_exist_flds = "";
        foreach ($field_list as $field){
            $field = trim($field);
            if($model_strength->hasAttribute($field)){
                
                $exist_flds['strength'][] = $field;
            }
            else if($model_endurance->hasAttribute($field))
            {
                
                $exist_flds['endurance'][] = $field;
            }
            else if($model_flexibility->hasAttribute($field))
            {
                
                 $exist_flds['flexibility'][] = $field;
            }
            else if($model_fat->hasAttribute($field))
            {
                 
                 $exist_flds['fat'][] = $field;
            }
            else{
                $non_exist_flds .= "'$field' ,";
            }
        }
       
        $non_exist_flds = trim($non_exist_flds,",");
        if(!count($exist_flds)){
            return array('status'=>0,'message'=>'All fields are invalid or not given',"fields"=>$non_exist_flds);
        }
        
        $result = array();
        if(count($exist_flds['strength']))
        {
            $exist_flds['strength'][] = 'due_date';
            $data = $model_strength->find()
                ->where('member_id = :member_id AND STR_TO_DATE(`added_on`,"%Y-%m-%d") >= :from_date AND STR_TO_DATE(`updated_on`,"%Y-%m-%d") <= :to_date', [':member_id'=>$user_id,':from_date'=>$from_date,':to_date'=>$to_date])
                ->select($exist_flds['strength'])
                ->all();
            foreach ($data as $row){
            foreach ($exist_flds['strength'] as $fld){
                $result[$fld][] = ($fld == "due_date") ? date('jS M',  strtotime($row->$fld)) : $row->$fld;
            }
        }
        }
        
        if(count($exist_flds['endurance']))
        {
            
            $data = $model_endurance->find()
                ->where('member_id = :member_id AND STR_TO_DATE(`added_on`,"%Y-%m-%d") >= :from_date AND STR_TO_DATE(`updated_on`,"%Y-%m-%d") <= :to_date', [':member_id'=>$user_id,':from_date'=>$from_date,':to_date'=>$to_date])
                ->select($exist_flds['endurance'])
                ->all();
            foreach ($data as $row){
            foreach ($exist_flds['endurance'] as $fld){
                $result[$fld][] = $row->$fld;
            }
        }
        }
        
        if(count($exist_flds['flexibility']))
        {
            
            $data = $model_flexibility->find()
                ->where('member_id = :member_id AND STR_TO_DATE(`added_on`,"%Y-%m-%d") >= :from_date AND STR_TO_DATE(`updated_on`,"%Y-%m-%d") <= :to_date', [':member_id'=>$user_id,':from_date'=>$from_date,':to_date'=>$to_date])
                ->select($exist_flds['flexibility'])
                ->all();
            foreach ($data as $row){
            foreach ($exist_flds['flexibility'] as $fld){
                $result[$fld][] = $row->$fld;
            }
        }
        }
        
        if(count($exist_flds['fat']))
        {
            
            $data = $model_fat->find()
                ->where('member_id = :member_id AND STR_TO_DATE(`added_on`,"%Y-%m-%d") >= :from_date AND STR_TO_DATE(`updated_on`,"%Y-%m-%d") <= :to_date', [':member_id'=>$user_id,':from_date'=>$from_date,':to_date'=>$to_date])
                ->select($exist_flds['fat'])
                ->all();
            
            foreach ($data as $row){
            foreach ($exist_flds['fat'] as $fld){
                $result[$fld][] = $row->$fld;
            }
        }
        }

        $return = array();
        $return["status"] = 1;
        $return["messsage"] = "user's progress fetched successfully";
        $return["result"] = $result;
        if($non_exist_flds != "")
            $return["wrong_fileds"] = $non_exist_flds;
        return $return;
        
    }
}