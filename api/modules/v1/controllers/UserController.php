<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\CenterUser;
use common\models\Role;
use common\models\Attendance;
use yii\db\Query;
use common\models\UserPhysicalDetails;
use common\models\UserDetails;
use common\models\City;
use common\models\Locality;
use common\models\AssignedTimeSlots;
use common\models\TrainerTimeSlots;
/* sdasd */

//error_reporting(0);
//class UserController extends Controller
class UserController extends RestController {

    public function behaviors() {
        $no_auth_actions = array("userLogin", "forgotPassword");
        $behaviors = parent::behaviors();
//        return $behaviors;
//        if (in_array($this->action->id, $no_auth_actions))
//            return $behaviors;
        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except' => $no_auth_actions
        ];
        return $behaviors;
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['getUsersStatisticsForTrainer', 'addEditUser', 'getRoles', 'getTrainer', 'resetPassword', 'deleteUser', 'userListing', 'userDetails', 'addEditUserDetails', 'medicalDetails', 'medicalQuestions', 'personalDetails', 'acceptUser'],
                    'allow' => TRUE,
                    'matchCallback' => function() {
                return !Yii::$app->user->identity->isUser;
            }
                ],
                [
                    'actions' => $no_auth_actions,
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actions() {
        $actions_c = [
            'addEditUser' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddEditUser']
            ],
            'getRoles' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetRoles']
            ],
            'getTrainer' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetTrainer']
            ],
            'userLogin' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionUserLogin']
            ],
            'resetPassword' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionResetPassword']
            ],
            'editUserStatus' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionEditUserStatus']
            ],
            'userListing' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionUserListing']
            ],
            'userDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionUserDetails']
            ],
            'addEditUserDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddEditUserDetails']
            ],
            'medicalDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionMedicalDetails']
            ],
            'medicalQuestions' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionMedicalQuestions']
            ],
            'personalDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionPersonalDetails']
            ],
            'acceptUser' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAcceptUser']
            ],
            'forgotPassword' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionForgotPassword']
            ],
            'getUsersStatisticsForTrainer' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUsersStatisticsForTrainer']
            ],
            'addAttendance' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddAttendance']
            ],
            'getAttendance' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetAttendance']
            ],
            'getAttendanceStatistics' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetAttendanceStatistics']
            ],
            'getUsersAttendance' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUsersAttendance']
            ],
            'getUserData' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUserData']
            ],
            'saveUserData' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionSaveUserData']
            ],
            'getPresentDays' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetPresentDays']
            ],
            'getTrainerTimeSlots' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetTrainerTimeSlots']
            ],
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }

    public function actionIndex() {
        
    }

    public function actionAddEditUser() {
        $status;
        $message;
        $result = array();

        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d H:i:s', time());

        // get all the parameter
        $firstname = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '';
        $lastname = isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : '';
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $mobile = isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : '';
        $role = isset($_REQUEST['role']) ? $_REQUEST['role'] : '';
        $center_id = Yii::$app->user->identity->center_id;
        $general_trainer_id = isset($_REQUEST['general_trainer_id']) ? $_REQUEST['general_trainer_id'] : NULL;
        $personal_trainer_id = isset($_REQUEST['personal_trainer_id']) ? $_REQUEST['personal_trainer_id'] : NULL;
        $nutritionist_id = isset($_REQUEST['nutritionist_id']) ? $_REQUEST['nutritionist_id'] : NULL;
        $logged_in_id = Yii::$app->user->identity->id;
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : NULL;
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : NULL;
        $sub_role = isset($_REQUEST['sub_role']) && trim($_REQUEST['sub_role']) != '' ? $_REQUEST['sub_role'] : 0;
        
        // medical details
        $current_weight = isset($_REQUEST['current_weight']) ? $_REQUEST['current_weight'] : '';
        $goal_weight = isset($_REQUEST['goal_weight']) ? $_REQUEST['goal_weight'] : '';
        $height = isset($_REQUEST['height']) ? $_REQUEST['height'] : '';
        
        // personal details
        $dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';
        $gender = isset($_REQUEST['gender']) ? $_REQUEST['gender'] : '';
        $city = 0;//isset($_REQUEST['city']) ? $_REQUEST['city'] : '';
        $locality = isset($_REQUEST['locality']) ? $_REQUEST['locality'] : '';
        $pincode = isset($_REQUEST['pincode']) ? $_REQUEST['pincode'] : 0;
        $description = isset($_REQUEST['description']) ? $_REQUEST['description'] : '';
        $address = isset($_REQUEST['address']) ? $_REQUEST['address'] : '';
        
        // trainer's availability time slots
        $trainer_availability_from = (isset($_REQUEST['trainer_time_slot_from']) && $_REQUEST['trainer_time_slot_from'] != '') ? $_REQUEST['trainer_time_slot_from'] : "";
        $trainer_availability_to = (isset($_REQUEST['trainer_time_slot_to']) && $_REQUEST['trainer_time_slot_to'] != '') ? $_REQUEST['trainer_time_slot_to'] : "";
        
        // trainer time slots for member
        $mem_class_start = (isset($_REQUEST['time_from']) && $_REQUEST['time_from'] != "") ? $_REQUEST['time_from'] : "";
        $mem_class_end = (isset($_REQUEST['time_to']) && $_REQUEST['time_to'] != "") ? $_REQUEST['time_to'] : "";
        $state = 0;
        if($locality != ''){
            $locality_models = Locality::find()->where(["id"=>$locality])->one();
            $city = isset($locality_models->city) ? $locality_models->city : $city;
            $state = isset($locality_models->city0->state) ? $locality_models->city0->state : 0;
        }
        $user_status = 1;
        $general_trainer_id = is_numeric($general_trainer_id) ? $general_trainer_id : NULL;
        $ret_user_id = 0;
        // generate random password
        // $password = substr(md5(rand()), 0, 7);
        $password = sha1('123456');

        if ($action == 'add') {
            // check if email or mobile no is already registered
            $userExist = CenterUser::find()->where('email = :email or mobile = :mobile', ['email' => $email, 'mobile' => $mobile])->one();
            // echo $firstname.'--'.$email.'--'.$mobile.'--'.$role.'--'.$password.'--'.$center_id;exit;
            if (is_null($userExist)) {
                // check if any of required fields empty
                if (!empty($firstname) && !empty($email) && !empty($mobile) && !empty($role)) {
                    $model = new CenterUser();   // create model of center user table
                    // assign all parameter 
                    $model->first_name = $firstname;
                    $model->last_name = $lastname;
                    $model->email = $email;
                    $model->mobile = $mobile;
                    $model->role = $role;
                    $model->password = $password;
                    $model->center_id = $center_id;
                    $model->general_trainer_id = $general_trainer_id;
                    $model->personal_trainer_id = $personal_trainer_id;
                    $model->nutritionist_id = $nutritionist_id;
                    $model->added_on = $current_timestamp;
                    $model->updated_on = $current_timestamp;
                    $model->added_by = $logged_in_id;
                    $model->updated_by = $logged_in_id;
                    $model->status = $user_status;
                    $model->sub_role = $sub_role;
                    if ($role == 6) {
                        $model->in_que = (isset(\Yii::$app->user->identity) && (\Yii::$app->user->identity->isGeneralTrainer || \Yii::$app->user->identity->isPersonalTrainer ) ) ? 1 : 0;
                        $model->diet_in_queue = (isset(\Yii::$app->user->identity) && (\Yii::$app->user->identity->isNutritionist) ) ? 1 : 0;
                    } else {
                        $model->diet_in_queue = $model->in_que = 1;
                    }
                    $transaction = Yii::$app->db->beginTransaction();   // begin the transaction
                    switch ($role) {
                        case 2 : $user_type = "Manager";
                            break;
                        case 3: $user_type = "General Trainer";
                            break;
                        case 4: $user_type = "Personal Trainer";
                            break;
                        case 5: $user_type = "Nutritionist";
                            break;
                        case 6 : $user_type = "Member";
                            break;
                        case 7: $user_type = "Center Admin";
                            break;
                    }
                    if($role == 6){
                        $physical_model = new UserPhysicalDetails();
                        $physical_model->scenario = 'notmhis';
                        $physical_model->current_weight = $current_weight;
                        $physical_model->goal_weight = $goal_weight;
                        $physical_model->height = $height;
                        $physical_model->added_by = $physical_model->updated_by = $model->added_by;
                        $physical_model->added_on = $physical_model->updated_on = $model->added_on;

                        // personal data
                        $personal_model = new UserDetails();
                        $personal_model->user_id = 0;
                        $personal_model->added_by = $personal_model->updated_by = $model->updated_by;
                        $personal_model->added_on = $personal_model->updated_on = $model->updated_on;
                        $personal_model->dob = $dob;
                        $personal_model->gender = $gender;
                        $personal_model->city = $city;
                        $personal_model->locality = $locality;
                        $personal_model->state = $state;
                        $personal_model->pincode = $pincode;
                        $personal_model->description = $description;
                        $personal_model->address = $address;
                    }   
                    
                    try {
                        //check model is saved or not
                        $is_user_valid = $model->validate();
                        $is_phy_valid = ($role == 6) ? $physical_model->validate() : true;
                        $is_per_valid = ($role == 6) ? $personal_model->validate() : true;
                        if ($is_user_valid && $is_phy_valid && $is_per_valid && $model->save()) {
                            if($role == 6){
                                $physical_model->user_id = $personal_model->user_id = $model->id;
                                $physical_model->save(FALSE);
                                $personal_model->save(FALSE);
                            }
                            $ret_user_id = $model->id;
                            $association_model = new \common\models\CenterAssociation();
                            $association_model->user_id = $ret_user_id;
                            $association_model->center_id = $center_id;
                            $association_model->association_start = date("Y-m-d H:i:s");
                            $association_model->save(FALSE);
                            // save trainer time availability
                            if($role != 6 && $trainer_availability_from != '' && is_array($trainer_availability_from) && count($trainer_availability_from)){
                                $batch_insert_data = [];
                                foreach ($trainer_availability_from as $inx => $val){
                                    $batch_insert_data[] = [
                                        "trainer_id"=>$model->id,
                                        "time_from"=>$val,
                                        "time_to"=>isset($trainer_availability_to[$inx]) ? $trainer_availability_to[$inx] : '0000',
                                        "day"=> 0,
                                        "added_by"=> $model->added_by,
                                        "updated_by"=> $model->updated_by,
                                        "added_on"=> $model->added_on,
                                        "updated_on"=> $model->updated_on,
                                    ];
                                }
                                if(count($batch_insert_data)){
                                    $row_inserted = \Yii::$app->db->createCommand()
                                            ->batchInsert(
                                                TrainerTimeSlots::tableName(), 
                                                ["trainer_id","time_from","time_to","day","added_by","updated_by","added_on","updated_on"],
                                                $batch_insert_data
                                            )->execute();
                                }
                            } 
                            
                            
                            // save member assigned time slot
                            if($role == 6 && !is_null($model->personal_trainer_id) && $model->personal_trainer_id && $mem_class_start != '' && $mem_class_end != ''){
                                $assigned_time_model = new AssignedTimeSlots();
                                $assigned_time_model->added_by = $assigned_time_model->updated_by = $model->updated_by;
                                $assigned_time_model->added_on = $assigned_time_model->updated_on = $model->updated_on;
                                $assigned_time_model->day = 0; // all days (Mon - sun)
                                $assigned_time_model->time_from = $mem_class_start;
                                $assigned_time_model->time_to = $mem_class_end;
                                $assigned_time_model->member_id = $ret_user_id;
                                $assigned_time_model->trainer_id = $model->personal_trainer_id;
                                $assigned_time_model->save(FALSE);
                            }
                            
                            $transaction->commit();   // commit transaction and data sucessfully saved

                            $subject = "Welcome to TraQ";
                            $body = '<table border="0" width="100%" height="100%" style="background:url(http://gympik.com/img/email/bg.jpg)"><tr><td><table border="0" style="margin:50px 50px 0 50px;background: #fff;width: calc(100% - 100px)"><tr style="background-color:#F4F1C2 "><td><table><tr><!--<td><a href="http://gympik.com"><img src="http://gympik.com/img/email/logo.png" style="width: 150px;"/></a></td>--></tr></table></td></tr><tr><td style="padding:20px;"><table><tr><td style="line-height: 1.5">Congratulations and welcome to TraQ!<br /><br/>You have been added as a ' . $user_type . ' . Your credentials are</td></tr><tr><td style="padding-top: 20px;"><table style="border: 2px solid #ff6600;border-radius: 5px;color: #ff6600;margin: 0 auto;padding: 5px;"><tr><td><b>Username :</b></td><td>' . $email . '</td></tr><tr><td><b>Password &nbsp;:</b></td><td>123456</td></tr></table></td></tr><tr><td style="padding: 20px 0; font-size: 13px;">Thanks and Regards,<br />Team TraQ<br /></td></tr></table></td></tr></table></td></tr></table>';


                            //  $body    = "You have been added as a $user_type . Your credentials are<br/>Username  : $email <br/>Password : 123456";
                            $sendMail = $this->sendviaMandrill($email, $subject, $body, "contact@gympik.com", 'Team Sandbox');
                            $status = 1;
                            $message = "user sucessfully saved";
                        } else {

                            $status = 2;
                            $message = "Error : "
                                    .$this->arrayToString($model->getErrors());
                            if($role == 6){
                              $message .=$this->arrayToString($physical_model->getErrors())
                                .$this->arrayToString($personal_model->getErrors());
                            }
                        }
                    } catch (Exception $e) {

                        $transaction->rollBack();
                        $status = 3;
                        $message = "there is some exception";
                    }
                } else {
                    $status = 4;
                    $message = "Please fill all required fields";
                }
            } else {
                $status = 5;
                $message = "email or mobile no already exits";
            }
        } else if ($action == 'edit') {
            $emailExist = count(CenterUser::find()->where('email = :email', ['email' => $email])->one());
            $mobileExist = count(CenterUser::find()->where('mobile = :mobile', ['mobile' => $mobile])->one());

            $user_model = CenterUser::findOne($user_id);

            if (!is_null($user_model)) {
                if (!empty($firstname) && !empty($email) && !empty($mobile) && !empty($role)) {
                    if (($mobileExist == 0 or $user_model->mobile == $mobile) && ($emailExist == 0 or $user_model->email == $email)) {
                        $user_model->first_name = $firstname;
                        $user_model->last_name = $lastname;
                        $user_model->email = $email;
                        $user_model->mobile = $mobile;
                        $user_model->role = $role;
                        $user_model->general_trainer_id = $general_trainer_id;
                        $user_model->personal_trainer_id = $personal_trainer_id;
                        $user_model->nutritionist_id = $nutritionist_id;
                        $user_model->updated_on = $current_timestamp;
                        $user_model->updated_by = $logged_in_id;
                        $user_model->sub_role = $sub_role;
                        // physical data
                        if($role == 6){
                            $physical_model = UserPhysicalDetails::find()->where(["user_id"=>$user_model->id]) 
                                            ->one();
                            if(!isset($physical_model->id)){
                                $physical_model = new UserPhysicalDetails();
                                $physical_model->user_id = $user_model->id;
                                $physical_model->added_by = $user_model->updated_by;
                                $physical_model->added_on = $user_model->updated_on;

                            }
                            $physical_model->scenario = 'notmhis';
                            $physical_model->current_weight = $current_weight;
                            $physical_model->goal_weight = $goal_weight;
                            $physical_model->height = $height;
                            $physical_model->updated_by = $user_model->updated_by;
                            $physical_model->updated_on = $user_model->updated_on;

                            // personal data
                            $personal_model = UserDetails::find()
                                            ->where(["user_id"=>$user_model->id]) 
                                            ->one();
                            if(!isset($personal_model->id)){
                                $personal_model = new UserDetails();
                                $personal_model->user_id = $user_model->id;
                                $personal_model->added_by = $user_model->updated_by;
                                $personal_model->added_on = $user_model->updated_on;
                            }
                            $personal_model->dob = $dob;
                            $personal_model->gender = $gender;
                            $personal_model->city = $city;
                            $personal_model->locality = $locality;
                            $personal_model->state = $state;
                            $personal_model->pincode = $pincode;
                            $personal_model->description = $description;
                            $personal_model->address = $address;
                            $personal_model->updated_by = $user_model->updated_by;
                            $personal_model->updated_on = $user_model->updated_on;
                        }
                                
                        $is_phy_valid = ($role == 6) ? $physical_model->validate() : TRUE;
                        $is_per_valid = ($role == 6) ? $personal_model->validate() : TRUE;
                        $is_user_valid = $user_model->validate();
                        if (($is_user_valid && $is_per_valid && $is_user_valid && $user_model->save() )) {
                            if($role == 6){
                                    $physical_model->save(false);
                                    $personal_model->save(false);
                            }
                            
                            // save trainer time availability
                            // delete all available slots then we ll add
                            if($role != 6){
                                \Yii::$app->db->createCommand()
                                        ->delete(TrainerTimeSlots::tableName(),"trainer_id = {$user_model->id}")
                                        ->execute();
                            }
                            if($role != 6 && $trainer_availability_from != '' && is_array($trainer_availability_from) && count($trainer_availability_from)){
                                $batch_insert_data = [];
                                foreach ($trainer_availability_from as $inx => $val){
                                    $batch_insert_data[] = [
                                        "trainer_id"=>$user_model->id,
                                        "time_from"=>$val,
                                        "time_to"=>isset($trainer_availability_to[$inx]) ? $trainer_availability_to[$inx] : '0000',
                                        "day"=> 0,
                                        "added_by"=> $user_model->added_by,
                                        "updated_by"=> $user_model->updated_by,
                                        "added_on"=> $user_model->added_on,
                                        "updated_on"=> $user_model->updated_on,
                                    ];
                                }
                                if(count($batch_insert_data)){
                                    $row_inserted = \Yii::$app->db->createCommand()
                                            ->batchInsert(
                                                TrainerTimeSlots::tableName(), 
                                                ["trainer_id","time_from","time_to","day","added_by","updated_by","added_on","updated_on"],
                                                $batch_insert_data
                                            )->execute();
                                }
                            }
                            
                            // save member assigned time slot
                            if($role == 6 && $mem_class_start != '' && $mem_class_end != ''){
                                $assigned_time_model = AssignedTimeSlots::find()
                                        ->where(["member_id"=>$user_model->id])->one();
                                $assigned_time_model = isset($assigned_time_model->id) ? $assigned_time_model : new AssignedTimeSlots();
                                $assigned_time_model->updated_by = $user_model->updated_by;
                                $assigned_time_model->updated_on = $user_model->updated_on;
                                $assigned_time_model->day = 0; // all days (Mon - sun)
                                $assigned_time_model->time_from = $mem_class_start;
                                $assigned_time_model->member_id = $user_model->id;
                                $assigned_time_model->time_to = $mem_class_end;
                                $assigned_time_model->trainer_id = $user_model->personal_trainer_id;
                                $assigned_time_model->save(FALSE);
                            }
                            
                            $status = 1;
                            $message = "User information updated sucessfully!";
                            $ret_user_id = $user_model->id;
                        } else {
                            $status = -1;
                            $message = "Error : " 
                                    . $this->arrayToString($user_model->getErrors());
                            if($role == 6){
                                  $message  .= $this->arrayToString($physical_model->getErrors()) 
                                    . $this->arrayToString($personal_model->getErrors());
                            }
                        }
                    } else if ($mobileExist > 0 && $user_model->mobile != $mobile && $emailExist > 0 && $user_model->email != $email) {
                        $status = -1;
                        $message = "Both mobile and email are registered with someone else";
                    } else if ($mobileExist > 0 && $user_model->mobile != $mobile) {
                        $status = -1;
                        $message = "This mobile number is registered with someone else";
                    } else if ($emailExist > 0 && $user_model->email != $email) {
                        $status = -1;
                        $message = "This email is registered with someone else";
                    }
                } else {
                    $status = -1;
                    $message = "Please fill all required fields";
                }
            } else {
                $status = -1;
                $message = "This user doesn't exist. please add this user.";
            }
        } else {
            $status = 0;
            $message = "Please provide valid action";
        }
        $result = array("status" => $status, "message" => $message);
        if ($ret_user_id)
            $result["result"] = ["user_id" => $ret_user_id];
        return $result;
        exit;
    }

    // get user roles
    public function actionGetRoles() {
        $roles_result = array();
        $role = array();
        $status;
        $message;

        try {
            $model = Role::find()->all();
            if (isset($model)) {
                foreach ($model as $val) {
                    $role = array();
                    if ($val->status == 1) {
                        $role['id'] = $val->id;
                        $role['name'] = $val->role;
                    }

                    array_push($roles_result, $role);
                }

                $status = 1;
                $message = "User Roles fetched sucessfully!";
            } else {
                $status = 0;
                $message = "No roles defined!";
            }
        } catch (Exception $ex) {

            $status = 0;
            $message = "Exception Occurs!";
        }

        $result = array("status" => $status, "message" => $message, "result" => $roles_result);
        return $result;
        echo json_encode($result);
    }

    public function actionGetTrainer() {
        $status;
        $message;
        $details_result = array();
        $result = array();
        $role = (isset($_REQUEST['trainer_type'])) ? $_REQUEST['trainer_type'] : '';

        $company_id = Yii::$app->user->identity->center_id;   //(isset($_REQUEST['company_id'])) ? $_REQUEST['company_id'] : ''; 
        if (!empty($role) && !empty($company_id)) {
            $role_model = Role::find()->where('role = :role', ['role' => $role])->one();

            if (!is_null($role_model)) {
                $role_id = $role_model->id;
                $query = new Query();
                $query->select('id,first_name,last_name')
                        ->from('sb__center_user')
                        ->where(['role' => $role_id, 'center_id' => $company_id]);

                $command = $query->createCommand();
                $data = $command->queryAll();

                if (!empty($data)) {
                    foreach ($data as $val) {
                        $user['id'] = $val['id'];
                        $user['first_name'] = $val['first_name'];
                        $user['last_name'] = $val['last_name'];

                        array_push($details_result, $user);
                    }
                    $status = 1;
                    $message = "Trainer List fetched succefully";
                } else {
                    $status = 0;
                    $message = "No trainer found in this center";
                }
            } else {
                $status = 0;
                $message = "This type of trainers not available!";
            }
        } else {
            $status = 0;
            $message = "Please provide trainer type and center!";
        }

        $result = array('status' => $status, 'message' => $message, 'result' => $details_result);
        return $result;
        echo json_encode($result);
    }

    // user login
    public function actionUserLogin() {

        $status;
        $message;
        $result = array();
        $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : '';
        $password = isset($_REQUEST['password']) ? sha1($_REQUEST['password']) : '';

        if (!empty($username) && !empty($password)) {


            $model = CenterUser::find()
                    ->where(['email' => $username])
                    ->orWhere(['mobile' => $username])
                    ->andWhere(['password' => $password, 'status' => 1])
                    ->one();
            if (isset($model->email) && !empty($model->email)) {
                $status = 1;
                $message = "Logged in Successfully";
            } else {

                $status = 5;
                $api_status = 400;
                $message = "wrong username or password";
            }
        } else {
            $status = 5;
            $api_status = 400;
            $message = "please enter all required fields";
        }

        $result = array("status" => $status, "message" => $message);
        if (isset($model->email)) {
            $result['result'] = $model->attributes;
            $result['result']['avtar'] = isset($model->details->avtar) ? $model->details->avtar : "";
            if (isset($model->center)) {
                $company = $model->center;
                $company_data = [];
                $company_data['logo'] = isset($company->avtar) ? $company->avtar : "";
                $company_data['name'] = isset($company->center_name) ? $company->center_name : "";
                $company_data['locality'] = isset($company->locality0->locality) ? $company->locality0->locality : "";
                $company_data['city'] = isset($company->city0->city) ? $company->city0->city : "";
                $company_data['city_id'] = isset($company->city) ? $company->city : "";
                $social_data = [];
                if (in_array($model->role, [2, 7]) && isset($company->socialcredentials)) {
                    $social_data['fb_page_id'] = isset($company->socialcredentials->fb_page_id) ? $company->socialcredentials->fb_page_id : "";
                    $social_data['fb_page_name'] = isset($company->socialcredentials->fb_page_name) ? $company->socialcredentials->fb_page_name : "";
                    $social_data['fb_page_access_token'] = isset($company->socialcredentials->fb_page_access_token) ? $company->socialcredentials->fb_page_access_token : "";
                    $social_data['tw_access_token'] = isset($company->socialcredentials->tw_access_token) ? $company->socialcredentials->tw_access_token : "";
                    $social_data['tw_access_token_secret'] = isset($company->socialcredentials->tw_access_token_secret) ? $company->socialcredentials->tw_access_token_secret : "";
                    $social_data['tw_app_consumer_key'] = isset($company->socialcredentials->tw_app_consumer_key) ? $company->socialcredentials->tw_app_consumer_key : "";
                    $social_data['tw_app_consumer_secret'] = isset($company->socialcredentials->tw_app_consumer_secret) ? $company->socialcredentials->tw_app_consumer_secret : "";
                }
                $company_data['social_creds'] = $social_data;
                $result['result']['company'] = $company_data;
            }
            $result['result']['token'] = md5($model->email);
            $result['result']['role_text'] = isset($model->role0->role) ? $model->role0->role : '';
        }
        return $result;
        $this->_sendResponse($api_status, $result);
        exit;
    }

    // reset password using center admin
    public function actionResetPassword() {
        //  echo "error";exit;
        //   $oldpassword  =  isset($_REQUEST['oldpassword'])?$_REQUEST['oldpassword']:'';
        $newpassword = isset($_REQUEST['newpassword']) ? $_REQUEST['newpassword'] : '';
        $confirmpassword = isset($_REQUEST['confirmpassword']) ? $_REQUEST['confirmpassword'] : '';
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';

        if (!empty($user_id) && !empty($newpassword)) {
            if ($confirmpassword == $newpassword) {
                $transaction = Yii::$app->db->beginTransaction();   // begin the transaction

                try {
                    $model = CenterUser::findOne($user_id);
                    if (!is_null($model)) {
                        $model->password = sha1($newpassword);
                        if ($model->save()) {
                            $transaction->commit();
                            $status = 1;
                            $api_status = 200;
                            $message = "password changed successfully";
                        } else {
                            $status = 2;
                            $api_status = 400;
                            $message = "problem in saving model";
                        }
                    } else {
                        $status = 2;
                        $api_status = 400;
                        $message = "this user does not exist";
                    }
                } catch (Exception $e) {

                    $transaction->rollBack();
                    $status = 3;
                    $api_status = 500;
                    $message = "there is some exception";
                }
            } else {
                $status = 4;
                $api_status = 400;
                $message = "password does not match";
            }
        } else {
            $status = 5;
            $api_status = 400;
            $message = "Please fill all required feilds";
        }

        $result = array("status" => $status, "message" => $message);
        return $result;
        exit;
    }

    public function actionEditUserStatus() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $ustatus = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';
        if ($user_id == "" || $ustatus == "")
            return["status" => 0, "message" => "user_id / status required"];
        if (!empty($user_id)) {
            $transaction = Yii::$app->db->beginTransaction();   // begin the transaction

            try {
                $model = CenterUser::findOne($user_id);
                if (!is_null($model)) {
                    $model->status = $ustatus; //5; // deleted status
                    $model->updated_by = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : $model->updated_by;
                    $model->updated_on = date('Y-m-d H:i:s');
                    if ($model->save(false)) {
                        if($ustatus == 5){
                            $association_model = \common\models\CenterAssociation::find()
                                    ->where(['user_id' => $user_id, "center_id"=>$model->center_id])
                                    ->one();
                            if(!isset($association_model->id)){
                                $association_model = new \common\models\CenterAssociation();
                                $association_model->user_id = $user_id;
                                $association_model->center_id = $model->center_id;
                                $association_model->association_start = $model->added_on;
                            }
                            $association_model->association_end = date('Y-m-d H:i:s');
                            $association_model->save(FALSE);
                        }
                        $transaction->commit();
                        $status = 1;
                        $api_status = 200;
                        if ($ustatus == 5)
                            $message = "user has been deaactivated sucessfully";
                        else
                            $message = "user's status updated sucessfully";
                    } else {
                        $status = -2;
                        $api_status = 400;
                        $message = "problem in saving model";
                    }
                } else {
                    $status = -4;
                    $api_status = 400;
                    $message = "this user does not exist";
                }
            } catch (Exception $e) {

                $transaction->rollBack();
                $status = -3;
                $api_status = 500;
                $message = "there is some exception";
            }
        } else {

            $status = -5;
            $api_status = 400;
            $message = "Please select a user/status to delete";
        }

        $result = array("status" => $status, "message" => $message);
        return $result;
        exit;
    }

    /*
     * listing of users
     * company_id is required
     */

    public function actionUserListing() {

        $logged_user = Yii::$app->user;
        $identity = $logged_user->identity;
        $company_id = $identity->center_id;

        if ($company_id == 0) {
            throw new \yii\web\BadRequestHttpException("Invalid Request for Userlist");
            exit;
        }
        $trainer_id = isset($_REQUEST['trainer_id']) ? $_REQUEST['trainer_id'] : 0;
        $diet_id = isset($_REQUEST['nutritionist_id']) ? $_REQUEST['nutritionist_id'] : 0;
        $role = isset($_REQUEST['role']) ? trim($_REQUEST['role'], ',') : 0;
        $in_que = isset($_REQUEST['in_que']) ? $_REQUEST['in_que'] : FALSE;
        $diet_in_queue = isset($_REQUEST['diet_in_queue']) ? $_REQUEST['diet_in_queue'] : FALSE;

        $tblName = CenterUser::tableName();
        $nutrition_alias = "nutritionist";
        $personal_alias = "personal_trainer_alias";
        $general_alias = "general_trainer_alias";
        $added_by = "added_by";
        $roleTbl = Role::tableName();
        $subRoleTbl = \common\models\CenterSubRole::tableName();
        $params = array();
//        $query = CenterUser::find();
        $query = (new Query)->from($tblName);
//        $query->orderBy = "$tblName.updated_on";
        // get realted trainers/dieticians
//        $query->joinWith('nutrition');
        $query->leftJoin("$tblName $added_by", "$tblName.added_by = $added_by.id");
        $query->leftJoin("$tblName $nutrition_alias", "$tblName.nutritionist_id = $nutrition_alias.id");
        $query->leftJoin("$tblName $personal_alias", "$tblName.personal_trainer_id = $personal_alias.id");
        $query->leftJoin("$tblName $general_alias", "$tblName.general_trainer_id = $general_alias.id");
        $query->leftJoin("$roleTbl", "$tblName.role = $roleTbl.id");
        $query->leftJoin("$subRoleTbl", "$tblName.sub_role = $subRoleTbl.id");

        if ($role != 0) {
            $query->where($tblName . '.role IN(' . $role . ')');
        }
        if (Yii::$app->user->identity->role != 1 || Yii::$app->user->identity->role != 7)
            $query->andWhere("$tblName.role NOT IN ('1','7')");
        $query->select(["$tblName.id","$roleTbl.role", "$tblName.first_name", "$tblName.last_name", "$tblName.email", "$tblName.mobile", "$tblName.in_que", "$tblName.status",
            "role" => "IFNULL($subRoleTbl.role_name,$roleTbl.role)",
            "added_by_name" => "CONCAT($added_by.first_name,' ',$added_by.last_name)",
            "dietician_name" => "CONCAT($nutrition_alias.first_name,' ',$nutrition_alias.last_name)",
            "personal_trainer_name" => "CONCAT($personal_alias.first_name,' ',$personal_alias.last_name)",
            "general_trainer_name" => "CONCAT($general_alias.first_name,' ',$general_alias.last_name)",
        ]);
        /* $dataProvider = new \yii\data\ActiveDataProvider([
          'query' => $query
          ]);
          if ($this->pagination) {
          $dataProvider->pagination = ['pageSize' => $this->pageSize];
          } else {
          $dataProvider->pagination = FALSE;
          } */
        if ($company_id)
            $params['center_id'] = $company_id;
        if ($trainer_id) {
            $query->andWhere(['OR', "$tblName.general_trainer_id = '$trainer_id'", "$tblName.personal_trainer_id = '$trainer_id'"]);
        }
        /*
         * role based data restriction
         */
        if ($identity->isGeneralTrainer)
            $query->andFilterWhere([$tblName . '.general_trainer_id' => $logged_user->id]);
        elseif ($identity->isPersonalTrainer)
            $query->andFilterWhere([$tblName . '.personal_trainer_id' => $logged_user->id]);
        elseif ($identity->isNutritionist)
            $query->andFilterWhere([$tblName . '.nutritionist_id' => $logged_user->id]);
        // restrict himself and admin and center admin
        $query->andWhere("$tblName.id != :id", [":id" => isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : 0]);

        $query->andWhere("$tblName.status != 5");
        if ($in_que !== FALSE)
            $params['in_que'] = $in_que;
        if ($diet_in_queue !== FALSE)
            $params['diet_in_queue'] = $diet_in_queue;
        if ($diet_id)
            $params['nutritionist_id'] = $diet_id;
        $query->orderBy = ["$tblName.updated_on"=>SORT_DESC];
        /* if ($role)
          $params['role'] = $role; */
        if (!count($params)) {
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
            ]);
            if ($this->pagination) {
                $dataProvider->pagination = ['pageSize' => $this->pageSize];
            } else {
                $dataProvider->pagination = FALSE;
            }
//            return $query->asArray()->all();//$dataProvider;
        }
        foreach ($params as $param => $value) {
            $query->andFilterWhere([
                $tblName . "." . $param => $value,
            ]);
        }
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
        if ($this->pagination) {
            $dataProvider->pagination = ['pageSize' => $this->pageSize];
        } else {
            $dataProvider->pagination = FALSE;
        }
//        return $query->asArray()->all();
        return $dataProvider;
    }

    /*
     * Details of a user
     * company_id required
     * user_id is requuired
     */

    public function actionUserDetails() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : 0;
        if ($user_id == 0) {
            $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : $user_id;
        }

        if ($user_id == 0) {
            throw new \yii\web\BadRequestHttpException("Invalid Request for Userlist");
            exit;
        }
        $model = CenterUser::findOne($user_id);
        $personal_data = $basic_data = $traing_data = $medical_data = [];
        $result = array();
        if (!is_null($model)){
            $basic_data['first_name'] = $model->first_name;
            $basic_data['last_name'] = $model->last_name;
            $basic_data['email'] = $model->email;
            $basic_data['mobile'] = $model->mobile;
            $basic_data['role'] = $model->role;
            $basic_data['sub_role'] = $model->sub_role;
            $traing_data['personal_trainer_id'] = $model->personal_trainer_id;
            $traing_data['general_trainer_id'] = $model->general_trainer_id;
            $traing_data['nutritionist_id'] = $model->nutritionist_id;
            $traing_data['current_weight'] = isset($model->physicalDetails->current_weight) ? $model->physicalDetails->current_weight : '';
            $traing_data['goal_weight'] = isset($model->physicalDetails->goal_weight) ? $model->physicalDetails->goal_weight : '';
            $traing_data['height'] = isset($model->physicalDetails->height) ? $model->physicalDetails->height : '';
            $traing_data['time_slot'] = ["text"=>"","value"=>""];
            if(isset($model->userTimeslot)){
                $from_val = isset($model->userTimeslot->time_from) ? $model->userTimeslot->time_from : "";
                $to_val = isset($model->userTimeslot->time_to) ? $model->userTimeslot->time_to : "";
                $from_txt = "";
                $to_txt = "";
                if($from_val != ""){
                    $from_val = strlen($from_val) != 4 ? "0$from_val" : $from_val;
                    $from_txt = DATE("g:i A", STRTOTIME($from_val));
                }
                if($to_val != ""){
                    $to_val = strlen($to_val) != 4 ? "0$to_val" : $to_val;
                    $to_txt = DATE("g:i A", STRTOTIME($to_val));
                }
                $traing_data['time_slot'] = ["text"=>"$from_txt - $to_txt","value"=>"$from_val-$to_val"];
            }
            $medical_data = isset($model->physicalDetails->medical_history) ? $model->physicalDetails->medical_history : "";
        }
        $personal_data['dob'] = isset($model->details->dob) ? $model->details->dob : "";
        $personal_data['gender'] = isset($model->details->gender) ? $model->details->gender : "";
        $personal_data['gender'] = isset($model->details->gender) ? $model->details->gender : "";
        $personal_data['city'] = isset($model->details->city) ? $model->details->city : "";
        $personal_data['city_text'] = isset($model->details->city0->city) ? $model->details->city0->city : "";
        $personal_data['locality'] = isset($model->details->locality) ? $model->details->locality : "";
        $personal_data['locality_text'] = isset($model->details->locality0->locality) ? $model->details->locality0->locality : "";
        $personal_data['pincode'] = isset($model->details->pincode) ? $model->details->pincode : "";
        $personal_data['address'] = isset($model->details->address) ? $model->details->address : "";
        $personal_data['about'] = isset($model->details->description) ? $model->details->description : "";
            
        if (is_null($model)) {
            $result['status'] = 0;
            $result['message'] = "user details not Available";
            $result['result'] = array();
        } else {
            $result['status'] = 1;
            $result['message'] = "user details success";
            $result['result'] = ["basic"=>$basic_data,"training"=>$traing_data,"personal"=>$personal_data,"medical"=>$medical_data];
        }
        return $result;
    }

    public function getUserDetails($id) {
        $data = \common\models\UserDetails::find()->where(['user_id' => $id])->one();
        /* if(is_null($data)){
          throw new \yii\web\BadRequestHttpException("user_id not exist..");
          exit;
          } */
        return $data;
    }

    /*
     * check is the company and user is asscociated
     */

    public function checkAsscociate($company_id, $user_id, $logged_id) {
        return TRUE;
        $user = CenterUser::findOne($user_id);
        if (is_null($user)) {
            throw new \yii\web\BadRequestHttpException("user_id not exist!");
            exit;
        }
        if (!($user->center_id == $company_id && ($user_id == $logged_id || $user->general_trainer_id == $logged_id || $user->personal_trainer_id == $logged_id || $user->nutritionist_id == $logged_id ))) {
            throw new \yii\web\BadRequestHttpException("Company_id or trainer/nutrition id association failed");
            exit;
        }
        return TRUE;
    }

    /*
     * add / edit user details
     */

    public function actionAddEditUserDetails() {
        $logged_id = Yii::$app->user->identity->id; //isset($_REQUEST['logged_id']) ? $_REQUEST['logged_id'] : "";
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $company_id = Yii::$app->user->identity->center_id; //isset($_REQUEST['company_id']) ? $_REQUEST['company_id'] : "";

        $this->checkAsscociate($company_id, $user_id, $logged_id);
        $model = $this->getUserDetails($user_id);

        if (is_null($model)) {
            $model = new \common\models\UserDetails();
            $model->added_by = $logged_id;
            $model->added_on = date("Y-m-d H:i:s");
        } else {
            $model->updated_by = $logged_id;
            $model->updated_on = date("Y-m-d H:i:s");
        }
        foreach ($_REQUEST as $column => $value) {
            if ($model->hasAttribute($column))
                $model->setAttribute($column, $value);
        }

        $result = array();
        if (isset($model->locality0->city0)) {
            $model->city = isset($model->locality0->city0->id) ? $model->locality0->city0->id : '';
            $model->state = isset($model->locality0->city0->state) ? $model->locality0->city0->state : '';
        }
        $medical_model = \common\models\UserPhysicalDetails::findOne(['user_id' => $user_id]);
        if (!isset($medical_model->id)) {
            $medical_model = new \common\models\UserPhysicalDetails();
            $medical_model->added_by = $model->added_by;
            $medical_model->added_on = $model->added_on;
            $medical_model->user_id = $user_id;
        } else {
            $medical_model->updated_by = $model->updated_by;
            $medical_model->updated_on = $model->updated_on;
        }
        $medical_model->scenario = 'notmhis';
        $medical_model->current_weight = (isset($_REQUEST['current_weight'])) ? $_REQUEST['current_weight'] : '';
        $medical_model->goal_weight = (isset($_REQUEST['goal_weight'])) ? $_REQUEST['goal_weight'] : '';
        $medical_model->height = (isset($_REQUEST['height'])) ? $_REQUEST['height'] : '';
        if (($model->validate() && $medical_model->validate()) && ($model->save() && $medical_model->save())) {
            $result['status'] = 1;
            $result['message'] = "User Details saved successfully";
        } else {
            $model->validate();
            $medical_model->validate();
            $result['status'] = 0;
            $result['message'] = "User Details saved failed : ";
            foreach ($model->getErrors() as $column => $msg_ar) {
                foreach ($msg_ar as $msgg) {
                    $result['message'] .= "$msgg ";
                }
            }
            foreach ($medical_model->getErrors() as $column => $msg_ar) {
                foreach ($msg_ar as $msgg) {
                    $result['message'] .= "$msgg ";
                }
            }
        }
        return $result;
    }

    /*
     * save or get medical details of user
     * if already exist then save updates otherwise add new record
     */

    public function actionMedicalDetails() {
//        $logged_id = isset($_REQUEST['logged_id']) ? $_REQUEST['logged_id'] : "";
        $logged_id = Yii::$app->user->identity->id;
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $company_id = Yii::$app->user->identity->center_id; //isset($_REQUEST['company_id']) ? $_REQUEST['company_id'] : "";
        $this->checkAsscociate($company_id, $user_id, $logged_id);
        $result = array();
        
        // user basic info
        $first_name = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : "";
        $last_name = isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : "";
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : "";
        $mobile = isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : "";
        
        $user_model = CenterUser::findOne($user_id);
        if(is_null($user_model)){
            throw new \yii\web\BadRequestHttpException("In Valid User Id");
            exit;
        }
        $user_model->first_name = $first_name;
        $user_model->last_name = $last_name;
        $user_model->email = $email;
        $user_model->mobile = $mobile;
        
        
        $data = \common\models\UserPhysicalDetails::find()->where(['user_id' => $user_id])->one();
        
        if (is_null($data)) {
            $data = new \common\models\UserPhysicalDetails();
            $data->added_on = date('Y-m-d H:i:s');
            $data->added_by = $logged_id;
        }
        $user_model->updated_on = $data->updated_on = date('Y-m-d H:i:s');
        $user_model->updated_by = $data->updated_by = $logged_id;
        foreach ($_REQUEST as $column => $value) {
            if ($data->hasAttribute($column))
                $data->setAttribute($column, $value);
        }
//            $_REQUEST['medical_history'] = array(1=>0,2=>1,3=>1,4=>0,5=>0,6=>1);
        if (isset($_REQUEST['medical_history']) && is_array($_REQUEST['medical_history']))
            $data->medical_history = json_encode($_REQUEST['medical_history']);
        $med_valid = $data->validate();
        $per_valid = $user_model->validate();
        if ($med_valid && $per_valid && $data->save() && $user_model->save()) {
            $result['status'] = 1;
            $result['message'] = "user's medical details saved successfully";
        } else {
            $result['status'] = 0;
            $result['message'] = "Error : "
                    .$this->arrayToString($user_model->getErrors())
                    .$this->arrayToString($data->getErrors());
        }
        return $result;
    }

    /*
     * returns all medical questions
     */

    public function actionMedicalQuestions() {
        $data = \common\models\MedicalQuestions::find()->where(['status' => 1])->all();
        $medical_data = array();
        if (isset($_REQUEST['user_id'])) {
            $medical_history = \common\models\UserPhysicalDetails::findOne(['user_id' => $_REQUEST['user_id']]);
            if (isset($medical_history->id))
                $medical_data = json_decode($medical_history->medical_history, TRUE);
        }
        $data_set = array();
        foreach ($data as $k => $value) {
            $data_set[$k] = $value->attributes;
            if (isset($medical_data[$value->id]))
                $data_set[$k]['checked'] = $medical_data[$value->id];
            else
                $data_set[$k]['checked'] = 0;
        }
        return array('status' => 1, 'result' => $data_set);
    }

    /*
     * returns personal details
     */

    public function actionPersonalDetails() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $personal_details = \common\models\UserDetails::findOne(['user_id' => $user_id]);
        $result = array();
        if (isset($personal_details->id)) {
            $result = $personal_details->attributes;
            $result['current_weight'] = isset($personal_details->medicalDetails->current_weight) ? $personal_details->medicalDetails->current_weight : "";
            $result['goal_weight'] = isset($personal_details->medicalDetails->goal_weight) ? $personal_details->medicalDetails->goal_weight : "";
            $result['height'] = isset($personal_details->medicalDetails->height) ? $personal_details->medicalDetails->height : "";
        }
        $response = array();
        if (count($result)) {
            $response['code'] = 1;
            $response['message'] = "User's personal details success";
        } else {
            $response['code'] = 0;
            $response['message'] = "User's personal details not yet added";
        }
        $response['result'] = $result;
        return $response;
    }

    public function actionAcceptUser() {
        if (!isset($_REQUEST['user_id'])) {
            throw new \yii\web\BadRequestHttpException("user_id Required!");
            exit;
        }
        $user = CenterUser::findOne($_REQUEST['user_id']);
        $response = array();
        if (isset($user->id)) {
            if (isset(\Yii::$app->user->identity->isNutritionist) && \Yii::$app->user->identity->isNutritionist)
                $user->diet_in_queue = 1;
            else
                $user->in_que = 1;
            if ($user->save(FALSE)) {
                $response['code'] = 1;
                $response['message'] = "Successfully Accepted by trainer";
            } else {
                $response['code'] = 0;
                $response['message'] = "Failed to save";
            }
        } else {
            $response['code'] = 0;
            $response['message'] = "User Doesn't Exist";
        }
        return $response;
    }

    public function actionForgotPassword() {
        $user_name = isset($_REQUEST['user_name']) ? trim($_REQUEST['user_name']) : "";
        if ($user_name == "")
            return array("code" => 0, "message" => "UserName Can't be blank");
        $user = CenterUser::find()
                ->where("email = :email OR mobile = :mobile", [":email" => $user_name, ":mobile" => $user_name])
                ->one();
        if (!isset($user->id))
            return array("status" => 0, "message" => "Wrong Email or Mobile No");
        $password = "traq_" . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $password = strtoupper($password);
//        $ret = TRUE;//$this->sendEmail($user->email,"Password Changed - Traq","New Password : $password");
        $to = [
            ["{$user->first_name} {$user->last_name}", $user->email]
        ];
        $subject = "Password Changed - Traq";
        $body = "<b>Dear *|NAME|* </b> <br />New Password : $password";
        $tag = "forgot password";
        $ret = $this->sendviaMandrill($to, $subject, $body, $this->adminEmail, $this->adminName, $tag);
        if ($ret) {
            $user->password = sha1($password);
            if ($user->save()) {
                return array("status" => 1, "message" => "Password Sent to your email "/* .$password */);
            } else {
                return array("status" => 0, "message" => "Error : " . $this->arrayToString($user->getErrors()));
            }
        } else {
            return array("status" => 0, "message" => "Unable to send Email. Please try after some time");
        }
    }

    public function actionGetUsersStatisticsForTrainer() {
        $trainer_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $fld = "personal_trainer_id";
        $q_fld = "in_que";
        $cond = "personal_trainer_id = :trainer_id";
        if (isset(\Yii::$app->user->identity)) {
            if (\Yii::$app->user->identity->isGeneralTrainer) {
                $fld = "general_trainer_id";
                $cond = "general_trainer_id = :trainer_id";
            } elseif (\Yii::$app->user->identity->isPersonalTrainer) {
                $fld = "personal_trainer_id";
                $cond = "personal_trainer_id = :trainer_id";
            } elseif (\Yii::$app->user->identity->isNutritionist) {
                $fld = "nutritionist_id";
                $q_fld = "diet_in_queue";
                $cond = "nutritionist_id = :trainer_id";
            }
        }
        $accepted = $in_que = $status = $pending_m = $diet_p = 0;
        $message = "";
        try {
            $accepted = CenterUser::find()
                    ->where("$cond AND $q_fld", [":trainer_id" => $trainer_id])
                    ->count();
            $in_que = CenterUser::find()
                    ->where("$cond AND !$q_fld", [":trainer_id" => $trainer_id])
                    ->count();
            $user_tbl = CenterUser::tableName();
            $mea_tbl = \common\models\UserMeasurements::tableName();
            $todate = date('Y-m-d');

            if (\Yii::$app->user->identity->isNutritionist) {
                
            } else {
                $conn = \Yii::$app->db;
                $total_client_sql = "SELECT GROUP_CONCAT(id) ids FROM $user_tbl  "
                        . "WHERE $fld = $trainer_id ";
                $query_bld = $conn->createCommand($total_client_sql);
                $total_client = $query_bld->queryOne();
                $ids = "";
                if (isset($total_client['ids']) && $total_client['ids'] != '') {
                    $ids = $total_client['ids'];
                    $pending_measurement_sql = "SELECT COUNT(*) as cnt "
                            . "FROM $mea_tbl WHERE user_id IN ( "
                            . $ids
                            . ") "
                            . "GROUP BY user_id HAVING MAX(STR_TO_DATE(`due_date`,'%d-%m-%Y')) <= '$todate'";
                    //            echo $pending_measurement_sql;exit;
                    $pending = $conn->createCommand($pending_measurement_sql);
                    $pending_rs = $pending->queryOne();
                    $total_added_users = "SELECT COUNT(*) as cnt "
                            . "FROM $mea_tbl WHERE user_id IN ( "
                            . $ids
                            . ") ";
                    $total_bld = $conn->createCommand($total_added_users);
                    $total_added_rs = $total_bld->queryOne();
                    $total_clients = explode(",", $ids);
                    $total_clients = is_array($total_clients) ? count($total_clients) : 0;
                    $total_cnt_added = isset($total_added_rs['cnt']) ? $total_added_rs['cnt'] : 0;
                    $balance = $total_clients - $total_cnt_added;
                    $pending_m = isset($pending_rs['cnt']) ? $pending_rs['cnt'] : $pending_m;
                    $pending_m+=$balance;
                }
            }
            $message = "Statitics fetched Succefully";
            $status = 1;
        } catch (Exception $ex) {
            $message = "unable to fetch";
            $status = 0;
        }
        $ret = ["status" => $status, "message" => $message, "result" => ["accepted" => $accepted, "in_que" => $in_que]];
        if (\Yii::$app->user->identity->isNutritionist) {
            $ret["result"]["pending_diet"] = $diet_p;
        } else {
            $ret["result"]["pending_measurements"] = $pending_m;
        }
        return $ret;
    }

    public function actionAddAttendance() {
        $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : "";
        $company_id = isset(Yii::$app->user->identity->center_id) ? Yii::$app->user->identity->center_id : "";
        $month = isset($_REQUEST['month']) ? $_REQUEST['month'] : "";
        $year = isset($_REQUEST['year']) ? $_REQUEST['year'] : "";
        $attendance = isset($_REQUEST['attendance']) ? $_REQUEST['attendance'] : "";
        $model = \common\models\Attendance::find()
                ->where("user_id = :user_id AND company_id = :company_id AND year = :year AND month = :month", ["user_id" => $user_id, "company_id" => $company_id, "year" => $year, "month" => $month])
                ->one();
        if (!isset($model->id))
            $model = new \common\models\Attendance ();
        $model->user_id = $user_id;
        $model->company_id = $company_id;
        $model->month = $month;
        $model->year = $year;
        $model->attendance = str_replace(" ", "", $attendance);
        $is_new = $model->isNewRecord ? TRUE : FALSE;
        if ($month !== "") {
            $mon_str = jdmonthname(gregoriantojd($month, 1, 1), CAL_MONTH_GREGORIAN_LONG);
        }
        if ($model->save()) {
            if ($is_new)
                return ["status" => 1, "message" => "Attendance For '$mon_str - $year' Saved Successfully"];
            else
                return ["status" => 1, "message" => "Attendance For '$mon_str - $year' Updated Successfully"];
        }else {
            return ["status" => 0, "message" => "Error : " . $this->arrayToString($model->getErrors())];
        }
    }

    public function actionGetAttendance() {
        $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : "";
        $company_id = isset(Yii::$app->user->identity->center_id) ? Yii::$app->user->identity->center_id : "";
        $month = isset($_REQUEST['month']) ? trim($_REQUEST['month']) : "";
        $year = isset($_REQUEST['year']) ? trim($_REQUEST['year']) : "";
        if ($month == "" || $year == "")
            return ["status" => 0, "message" => "month and year is required"];
        $mon_str = $month;
        if ($month !== "") {
            $mon_str = jdmonthname(gregoriantojd($month, 1, 1), CAL_MONTH_GREGORIAN_LONG);
        }
        $attendance = \common\models\Attendance::find()
                ->select(["id", "attendance", "month"])
                ->where("user_id = :user_id AND company_id = :company_id AND month = :month AND year = :year", [
                    ":user_id" => $user_id,
                    ":company_id" => $company_id,
                    ":month" => $month,
                    ":year" => $year
                ])
                ->one();
        $dates = [];
        if (isset($attendance->id)) {
            $dmonth = $attendance->month;
            $date_arr = explode(",", $attendance->attendance);
            if (is_array($date_arr)) {
                foreach ($date_arr as $day) {
                    $day = trim($day);
                    if ($day == "")
                        continue;
                    $dates[] = "$day-$dmonth-$year";
                }
            }
        }
        if (count($dates))
            return ["status" => 1, "message" => "attendane of $mon_str - $year Fetched Successfully", "result" => $dates];
        return ["status" => 0, "message" => "attendane of $mon_str - $year not found"];
    }

    public function actionGetUsersAttendance() {
        $company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        if ($company_id == "" || $user_id == "")
            return ["status" => 0, "message" => "Invalid Access"];
        $month = isset($_REQUEST['month']) ? $_REQUEST['month'] : date('m');
        $year = isset($_REQUEST['year']) ? $_REQUEST['year'] : date('Y');
        $user_tbl = CenterUser::tableName();
        $atten_tbl = \common\models\Attendance::tableName();
        $sel_clmn = "u.id,CONCAT(u.first_name,' ',u.last_name) name,IFNULL(a.attendance,'') attendance";
        $user_sql = "SELECT |*SELECT*| FROM $user_tbl u LEFT JOIN $atten_tbl a ON u.id = a.user_id AND a.month = '$month' AND a.year = '$year'"
                . " WHERE u.center_id = '$company_id' AND u.role=6";
        if (in_array(\Yii::$app->user->identity->role, array(3, 4, 5)))
            $user_sql.= " AND (u.general_trainer_id = '$user_id' OR personal_trainer_id = '$user_id' OR nutritionist_id = '$user_id') ";
        $conn = Yii::$app->db;
        $totalCount = $conn->createCommand(str_replace("|*SELECT*|", "COUNT(*)", $user_sql))->queryScalar();
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => str_replace("|*SELECT*|", $sel_clmn, $user_sql),
            'totalCount' => $totalCount
        ]);
        if ($this->pagination) {
            $dataProvider->pagination = ['pageSize' => $this->pageSize];
        } else {
            $dataProvider->pagination = FALSE;
        }
//        echo "<pre>";
//        print_r($dataProvider);
//        exit;
        $this->status = 1;
        $this->message = "Users Attendance fetched successfully";
        return $dataProvider;
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($user_sql);
        $data = $query_bld->queryAll();
        return ["status" => 1, "message" => "Users Attendance fetched successfully", "result" => $data];
    }

    public function actionGetUserData() {
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : $user_id;
        if ($user_id == "")
            return ["status" => 0, "message" => "Unathorise Access"];
        $userdata = [];
        $user = CenterUser::findOne($user_id);
        if (!isset($user->id))
            return ["status" => 0, "message" => "User not exist"];
//        $user = \Yii::$app->user->identity;
        $userdata['first_name'] = $user->first_name;
        $userdata['last_name'] = $user->last_name;
        $userdata['email'] = $user->email;
        $userdata['mobile'] = $user->mobile;
        $userdata['role'] = $user->role;
        $userdata['role_text'] = isset($user->role0->role) ? $user->role0->role : "";
        $userdata['general_trainer_id'] = $user->general_trainer_id;
        $userdata['general_trainer_text'] = (isset($user->generalTrainer->first_name) && isset($user->generalTrainer->last_name)) ? $user->generalTrainer->first_name . " " . $user->generalTrainer->last_name : "";
        $userdata['personal_trainer_id'] = $user->personal_trainer_id;
        $userdata['personal_trainer_text'] = (isset($user->personalTrainer->first_name) && isset($user->generalTrainer->last_name)) ? $user->personalTrainer->first_name . " " . $user->personalTrainer->last_name : "";
        $userdata['nutritionist_id'] = $user->nutritionist_id;
        $userdata['nutritionist_text'] = (isset($user->nutrition->first_name) && isset($user->nutrition->last_name)) ? $user->nutrition->first_name . " " . $user->nutrition->last_name : "";
        if (isset($user->sub_role) && $user->sub_role) {
            $userdata['sub_role'] = isset($user->sub_role) ? $user->sub_role : 0;
            $userdata['sub_role_text'] = isset($user->subRole->role_name) ? $user->subRole->role_name : "";
        }

        $userdetails = $this->getUserDetails($user->id);
        $user_details = [];
        if (isset($userdetails->id)) {
            $user_details['avtar'] = isset($userdetails->avtar) ? $userdetails->avtar : "";
            $user_details['address'] = isset($userdetails->address) ? $userdetails->address : "";
            $user_details['description'] = isset($userdetails->description) ? $userdetails->description : "";
            $user_details['locality'] = isset($userdetails->locality) ? $userdetails->locality : "";
            $user_details['locality_text'] = isset($userdetails->locality0->locality) ? $userdetails->locality0->locality : "";
            $user_details['city'] = isset($userdetails->locality0->city) ? $userdetails->locality0->city : "";
            $user_details['city_text'] = isset($userdetails->locality0->city0->city) ? $userdetails->locality0->city0->city : "";
            $user_details['state'] = isset($userdetails->locality0->city0->state) ? $userdetails->locality0->city0->state : "";
            $user_details['state_text'] = isset($userdetails->locality0->city0->state0->state) ? $userdetails->locality0->city0->state0->state : "";
            $user_details['pincode'] = isset($userdetails->pincode) ? $userdetails->pincode : "";
            $user_details['dob'] = isset($userdetails->dob) ? $userdetails->dob : "";
            $user_details['gender'] = isset($userdetails->gender) ? $userdetails->gender : "";
            $user_details['gender_text'] = (isset($userdetails->gender) && $userdetails->gender == 1) ? 'Male' : ((isset($userdetails->gender) && $userdetails->gender == 0) ? 'Female' : (isset($userdetails->gender) && $userdetails->gender == 2) ? "Others" : "");
        }
        $return = [];
        $return["status"] = 1;
        $return["message"] = "User Data Fetched succcessfully";
        $return["result"]["data"] = $userdata;
        $return["result"]["details"] = $user_details;
        return $return;
    }

    public function actionSaveUserData() {
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        //$user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : $user_id;
        if ($user_id == '')
            return ["status" => 0, "message" => "Unauthorised Access"];
        $user = CenterUser::findOne($user_id);
        if (!isset($user->id))
            return ["status" => 0, "message" => "Unauthorised Access"];
        $user->first_name = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : "";
        $user->last_name = isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : "";
        if (isset($_REQUEST['password']))
            $user->password = sha1($_REQUEST['password']);
        $userDetails = \common\models\UserDetails::find()->where("user_id = :user_id", ["user_id" => $user_id])->one();
        if (!isset($userDetails->id))
            $userDetails = new \common\models\UserDetails ();
        $userDetails->avtar = isset($_REQUEST['avtar']) ? $_REQUEST['avtar'] : "";
        $userDetails->description = isset($_REQUEST['description']) ? $_REQUEST['description'] : "";
        $user_err = $user->validate(["first_name", "last_name"]);
        $userd_err = $userDetails->validate(["avtar", "description"]);
        $errors = "";
        if ($user_err !== TRUE)
            $errors .= $this->arrayToString($user->getErrors());
        if ($userd_err !== TRUE)
            $errors .= $errors = $this->arrayToString($userDetails->getErrors());
        if ($errors != "")
            return ['status' => 0, 'message' => "Error : " . $errors];
        if ($user->save(FALSE) && $userDetails->save(FALSE)) {
            return ['status' => 1, "message" => "Updated Successfully"];
        }
        return ['status' => 0, 'message' => "Error : " . $this->arrayToString($user->getErrors()) . $this->arrayToString($userDetails->getErrors())];
    }

    public function actionGetPresentDays() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $month = (int) isset($_REQUEST['month']) ? $_REQUEST['month'] : date('n', strtotime('0 month'));
        $attendance = array();



        $current_year = (int) isset($_REQUEST['year']) ? $_REQUEST['year'] : date("Y");
        //echo $month.'--'.$current_year;exit;
        $total_days = (int) cal_days_in_month(CAL_GREGORIAN, $month, $current_year);

        if (empty($user_id) || $user_id == '') {

            $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';

            if (empty($user_id) && $user_id == '') {
                $result = array("status" => -1, "message" => "Your session is expired. Please Login!");
                return $result;
            }
        }

        //  $model = Attendance::find()->where("user_id = :user_id and month = :month",["user_id"=>$user_id,"month"=>$month])->one();
        $model = Attendance::find()->where('user_id = :user_id and month = :month and year = :year', ['user_id' => $user_id, 'month' => $month, 'year' => $current_year])->one();
        if (isset($model) && !empty($model)) {

            $present_days = (int) (!empty($model->attendance)) ? count(explode(",", $model->attendance)) : 0;
            $skipped = $total_days - $present_days;
            $attendance['attended'] = $present_days;
            $attendance['skipped'] = $skipped;
            $result = array("status" => 1, "message" => "Attendance get successfully!", "result" => $attendance);
            return $result;
        } else {
            $present_days = 0;
            $skipped = $total_days - $present_days;
            $attendance['attended'] = $present_days;
            $attendance['skipped'] = $skipped;
            $result = array("status" => 3, "message" => "Attendance get successfully!", "result" => $attendance);
            return $result;
        }
    }

    // attendance statistics
    public function actionGetAttendanceStatistics() {
        $day = date('d');
        $mon = date('m');
        $year = date('Y');
        if (isset($_REQUEST['date']) && $_REQUEST['date'] != '') {
            $day = date('d', $_REQUEST['date']);
            $mon = date('m', $_REQUEST['date']);
            $year = date('Y', $_REQUEST['date']);
        }
        $center_id = isset(Yii::$app->user->identity->center_id) ? Yii::$app->user->identity->center_id : 1;
        $user_tbl = CenterUser::tableName();
        $conn = \Yii::$app->db;
        $role_tbl = Role::tableName();
        $atten_tbl = Attendance::tableName();
        $total_users_sql = "SELECT count(1) total,u.role "
                . "FROM $user_tbl u "
                . "LEFT JOIN $role_tbl r ON r.id = u.role "
                . "WHERE center_id = $center_id AND u.status = 1 group by u.role";
        $stream = $conn->createCommand($total_users_sql);
        $total_users = $stream->queryAll();
        $total_users = \yii\helpers\ArrayHelper::map($total_users, 'role', 'total');
        $attendance_sql = "SELECT u.role,COUNT(1) AS present 
                        FROM $atten_tbl a 
                        LEFT JOIN $user_tbl u ON u.id = a.user_id
                        WHERE a.company_id = $center_id AND a.year = $year AND a.month = $mon AND FIND_IN_SET($day,attendance)
                        GROUP BY u.role";
        $present_stream = $conn->createCommand($attendance_sql);
        $present_data = $present_stream->queryAll();
        $present_data = \yii\helpers\ArrayHelper::map($present_data, 'role', 'present');

        $roles = \yii\helpers\ArrayHelper::map(Role::find()->all(), 'id', 'role');
        $attendance = [];
        foreach ($roles as $role => $role_txt) {
            $ptotal_users = isset($total_users[$role]) ? $total_users[$role] : 0;
            $present_users = isset($present_data[$role]) ? $present_data[$role] : 0;
            $attendance[str_replace(" ", "_", $role_txt)] = ["role" => $role, "total" => $ptotal_users, "present" => $present_users];
        }
        return $attendance;
    }
    public function actionGetTrainerTimeSlots(){
        
        $trainer_id = isset($_REQUEST['trainer_id']) ? $_REQUEST['trainer_id'] : "";
        if($trainer_id == ''){
            throw new \yii\web\BadRequestHttpException("Invalid Request for Time Slot");
            exit;
        }
        $timeslots = TrainerTimeSlots::find()
                ->where(["trainer_id"=>$trainer_id])
                ->select(["time_from","time_to"])
                ->all();
        
        $assigned_time_slots = AssignedTimeSlots::find()
                ->where(["trainer_id"=>$trainer_id])
                ->select(["time_from","time_to"])
                ->all();
        $assigned_time_slots = \yii\helpers\ArrayHelper::map($assigned_time_slots, "time_from", "time_to");
        
        $time_division = [];
        foreach ($timeslots as $times){
            $start_time = $times->time_from == '' ? 0 : $times->time_from;
            $end_time = $times->time_to == '' ? 24 : $times->time_to;
            for($intv = $start_time; $intv < $end_time; ){
                $start = $intv;
                $intv+=100;
                $end = $intv;
                $end = ($end <= $end_time) ? $end : $end_time;
                $disable = 0;
                foreach ($assigned_time_slots as $s=>$e){
                    if($start >= $s && $start < $e)
                        $disable = 1;
                }
                $start = strlen($start) != 4 ? "0$start" : $start;
                $start_c = DATE("g:i A", STRTOTIME($start));
                $end = strlen($end) != 4 ? "0$end" : $end;
                $end_c = DATE("g:i A", STRTOTIME($end));
//                $time_division[] = ["start"=>$start,"end"=>$end,"disable"=>$disable];
                $time_division[] = ["text"=>"$start_c - $end_c","value"=>"$start-$end","disable"=>$disable];
                
            }
        }
       
        return ["status"=>1,"message"=>"trainer timeslot fatched successfully","result"=>$time_division];
    }
}
