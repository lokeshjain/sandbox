<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
class FoodController extends RestController {
     public function behaviors() {
        $no_auth_actions = ['deleteMeal'];
        $behaviors = parent::behaviors();
//        return $behaviors;
        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except'=> $no_auth_actions
        ];
        return $behaviors;
    }
     public function actions() {
         
            return [
                'addMeal' => [
                    'class' => 'yii\rest\IndexAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                    'prepareDataProvider' => [$this, 'actionAddMeal']
                ],
                'deleteMeal' => [
                    'class' => 'yii\rest\IndexAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                    'prepareDataProvider' => [$this, 'actionDeleteMeal']
                ],
            ];
     }
     public function actionAddMeal(){
         $user_id = "";
         if(isset(\Yii::$app->user->identity) && \Yii::$app->user->identity->isUser)
             $user_id = \Yii::$app->user->identity->id;
         elseif(isset ($_REQUEST['user_id']))
             $user_id = $_REQUEST['user_id'];
         $food_intake = new \common\models\UserFoodIntake;
         if(isset($_REQUEST['diet_id']) && $_REQUEST['diet_id']){
             $exist = $food_intake
                     ->find()
                     ->where('user_id = :user_id AND diet_id = :diet_id AND meal_type = :meal_type', [':user_id'=>$user_id,':diet_id'=>$_REQUEST['diet_id'],':meal_type'=>isset($_REQUEST['meal_type']) ? $_REQUEST['meal_type'] : ""])
                     ->one();
             if(isset($exist->id))
                $food_intake = $exist; 
         }
         $food_intake->user_id = $user_id;
         $food_intake->diet_id = isset($_REQUEST['diet_id']) && trim($_REQUEST['diet_id']) != "" ? $_REQUEST['diet_id'] : 0;
         $food_intake->date = isset($_REQUEST['date']) ? $_REQUEST['date'] : "";
        $food_intake->time = isset($_REQUEST['time']) ? $_REQUEST['time'] : "";
        $food_intake->meal_type = isset($_REQUEST['meal_type']) ? $_REQUEST['meal_type'] : "";
        $food_intake->food_intake = isset($_REQUEST['food_intake']) ? $_REQUEST['food_intake'] : "";
        $food_intake->as_per_trainer = (isset($_REQUEST['as_per_trainer']) && (int)$_REQUEST['as_per_trainer'] == 1) ? 1 : 0;
        if($food_intake->save()){
             $result['status'] = 1;
             $result['message'] = "Food Intake Saved Successfully";
         }else{
             $result['status'] = 0;
             $result['message'] = "Error : ".$this->arrayToString($food_intake->getErrors());
         }
         return $result;
     }
     public function actionDeleteMeal(){
         $user_id = "";
         $intake_id = isset ($_REQUEST['intake_id']) ? $_REQUEST['intake_id'] : 0;
         if(isset(\Yii::$app->user->identity) && \Yii::$app->user->identity->isUser)
             $user_id = \Yii::$app->user->identity->id;
         elseif(isset ($_REQUEST['user_id']))
             $user_id = $_REQUEST['user_id'];
         $ret = \common\models\UserFoodIntake::deleteAll("id = :id AND user_id = :user_id", array(':id'=>$intake_id,":user_id"=>$user_id));
         if($ret > 0){
             $result['status'] = 1;
             $result['message'] = "$ret No of Meals deleted Successfully";
         }else{
             $result['status'] = 0;
             $result['message'] = "$ret No of Meals deleted Successfully";
         }
         return $result;
     }
}