<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\Worktype;
use common\models\Workouts;
use common\models\WorkoutsMet;
use common\models\UserMeasurements;
use common\models\TrainerWorkout;
use common\models\UserWorkout;
use common\models\UserPhysicalDetails;
use common\models\UserCardioWorkout;
use common\models\UserDetails;
use common\models\UserNonCardioWorkout;
use yii\db\Query;

/* sdasd */

//error_reporting(0);
class WorkoutController extends RestController {

    public function actionGetCity() {
        $data = \common\models\City::find()->select(['id', 'city'])->all();
        $result = \yii\helpers\ArrayHelper::map($data, 'id', 'city');
        return array("status" => 1, "message" => "city listed successfully", "result" => $result);
    }

    public function behaviors() {
        $no_auth_actions = array("getUserWorkout", 'getCity', 'getUsersDetails');
        $behaviors = parent::behaviors();
//       return $behaviors;

        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except' => $no_auth_actions
        ];
        return $behaviors;
    }

    public function actions() {
        $actions_c = [
            'getCity' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetCity']
            ],
            'workoutCategoryList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionWorkoutCategoryList']
            ],
            'workoutList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionWorkoutList']
            ],
            'addTrainerWorkout' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddTrainerWorkout']
            ],
            'addUserWorkout' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddUserWorkout']
            ],
            'getUserWorkout' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUserWorkout']
            ],
            'getUsersDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetUsersDetails']
            ],
            'calorieBurnt' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionCalorieBurnt']
            ],
            'personalTrainerAddWorkout' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionPersonalTrainerAddWorkout']
            ],
            'getPersonalTrainerWorkout' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetPersonalTrainerWorkout']
            ],
            'getWorkoutUserList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetWorkoutUserList']
            ],
            'markPersonalTrainerWorkout' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionMarkPersonalTrainerWorkout']
            ],
            'getStatistics' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetStatistics']
            ],
            'report' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionReport']
            ],
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }

    public function actionAddUserWorkout() {
        $status = 0;
        $message = '';
        $current_timestamp = date('Y-m-d h:i:s', time());

        $trainer_workout_id = isset($_REQUEST['trainer_workout_id']) ? $_REQUEST['trainer_workout_id'] : '';
        $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';
        $trainer_wo_model = TrainerWorkout::findOne($trainer_workout_id);
        if (!isset($trainer_wo_model->id) /* || (isset($trainer_wo_model->user_id) && $trainer_wo_model->user_id != $user_id) */) {
            $result = array("status" => 0, "message" => "Trying to update wrong workout");
            return $result;
        }

        $worktype_id = isset($trainer_wo_model->workoutName->workoutType->id) ? $trainer_wo_model->workoutName->workoutType->id : '';
        $workout_id = isset($trainer_wo_model->workout_id) ? $trainer_wo_model->workout_id : '';
        $duration = isset($_REQUEST['duration']) ? $_REQUEST['duration'] : 0;
        $distance = isset($_REQUEST['distance']) ? $_REQUEST['distance'] : 0;
        $calories = isset($_REQUEST['calories']) ? $_REQUEST['calories'] : 0;
        $weight = isset($_REQUEST['weight']) ? $_REQUEST['weight'] : 0;
        $repetition = isset($_REQUEST['repetition']) ? $_REQUEST['repetition'] : 0;
        $workout_date = isset($trainer_wo_model->workout_date) ? $trainer_wo_model->workout_date : '';
        $notes = isset($_REQUEST['notes']) ? $_REQUEST['notes'] : '';
        $is_complete = isset($_REQUEST['is_complete']) ? $_REQUEST['is_complete'] : 0;
        $logged_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';

        if (!empty($user_id) && !empty($logged_id)) {
            if ($worktype_id == 2) {
                if ($is_complete == 0) {
                    $detail_model = UserDetails::find()->where('user_id = :user_id', ['user_id' => $user_id])->one();
                    if (isset($detail_model)) {
                        $gender = $detail_model->gender;
                        $dob = $detail_model->dob;
                        $from = new \DateTime($dob);
                        $to = new \DateTime('today');
                        $age = $from->diff($to)->y;
                    } else {

                        $gender = '';
                        $age = '';
                    }

                    // get the weight and height from measurement or physical details table to claculate calorie
                    $mesurements = UserMeasurements::find()->where('user_id = :user_id', ['user_id' => $user_id])->orderBy('added_on desc')->one();

                    // if user did'nt fill measurements details
                    if (is_null($mesurements)) {
                        $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id' => $user_id])->one();
                        if (!is_null($mesurements)) {
                            $weight = $mesurements->current_weight;
                            $height = $mesurements->height;
                        } else {
                            $weight = NULL;
                            $height = NULL;
                        }
                    } else {
                        if (!empty($mesurements->weight) && !empty($mesurements->height) && $mesurements->weight != 0 && $mesurements->height != 0) {
                            $weight = $mesurements->weight;
                            $height = $mesurements->height;
                        } else {

                            $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id' => $user_id])->one();
                            if (!is_null($mesurements)) {
                                $weight = $mesurements->weight;
                                $height = $mesurements->height;
                            } else {
                                $weight = NULL;
                                $height = NULL;
                            }
                        }
                    }

                    if (!empty($gender) && !empty($age) && $age != 0) {
                        if (!is_null($weight) && !is_null($height) && $weight != 0 && $height != 0 && !empty($weight) && !empty($height)) {

                            // get MET of workout from workout table
                            $workout_model = Workouts::find()->where('id = :id', ['id' => $workout_id])->one();
                            $met = $workout_model->met;
                            if ($met == 0) {
                                $duration_hour = $duration / 60;
                                $speed = round($distance / $duration_hour);
                                $workout_met_model = WorkoutsMet::find('met')->where('workout_id = :workout_id and min_speed <= :min_speed and max_speed > :max_speed', ['workout_id' => $workout_id, 'min_speed' => $speed, 'max_speed' => $speed])->one();
                                $met = isset($workout_met_model) ? $workout_met_model->met : 0;
                            }

                            if ($gender == 1) {
                                $bmr = 13.75 * $weight + (5 * $height) - (6.76 * $age) + 66;    // calculate bmr for male
                                $calories = ($bmr / 24) * $met * ($duration / 60);     // calculate calorie
                            } else {
                                $bmr = 9.56 * $weight + (1.85 * $height) - (4.68 * $age) + 655;  // calculate bmr for female
                                $calories = ($bmr / 24) * $met * ($duration / 60);     // calculate calorie
                            }
                        } else {
                            $status = 9;
                            $message = "Please fill the weight and height in measurements or physical details";
                        }
                    } else {
                        $status = 9;
                        $message = "Please fill gender and dob!";
                    }
                }


                if ($status != 9 && !empty($workout_id) && !empty($workout_date)) {
                    $add_model = UserWorkout::find()->where('trainer_workout_id = :trainer_workout_id', ['trainer_workout_id' => $trainer_workout_id])->one();
                    if (!isset($add_model) || is_null($add_model)) {
                        $add_model = new UserWorkout();
                    }
                    $add_model->trainer_workout_id = $trainer_workout_id;
                    $add_model->user_id = $user_id;
                    $add_model->workout_id = $workout_id;
                    $add_model->worktype_id = $worktype_id;
                    $add_model->duration = $duration;
                    $add_model->distance = (isset($distance) && !is_null($distance) && $distance != '') ? $distance : 0;
                    $add_model->workout_date = $workout_date;
                    $add_model->calories = round($calories, 2);
                    $add_model->notes = $notes;
                    $add_model->added_on = $current_timestamp;
                    $add_model->added_by = $logged_id;
                    $add_model->updated_on = $current_timestamp;
                    $add_model->updated_by = $logged_id;
                    $add_model->status = 1;
                    if ($add_model->save()) {
                        $status = 1;
                        $message = "Workout has been updated successfully!";
                    } else {

                        $status = 0;
                        $message = "Error : " . $this->arrayToString($add_model->getErrors());
                    }
                } else if ($status != 9) {

                    $status = 2;
                    $message = "Please provide workout id and workout date!";
                }
            } else if (!empty($worktype_id)) {

                if (!empty($workout_id) && !empty($weight) && !empty($repetition)) {

                    $model = UserWorkout::find()->where('trainer_workout_id = :trainer_workout_id', ['trainer_workout_id' => $trainer_workout_id])->one();
                    if (!isset($model) || is_null($model)) {
                        $model = new UserWorkout();
                    }
                    $model->trainer_workout_id = $trainer_workout_id;
                    $model->user_id = $user_id;
                    $model->workout_id = $workout_id;
                    $model->worktype_id = $worktype_id;
                    $model->workout_date = $workout_date;
                    $model->weight = $weight;
                    $model->repetition = $repetition;
                    $model->notes = $notes;
                    $model->added_on = $current_timestamp;
                    $model->added_by = $logged_id;
                    $model->updated_on = $current_timestamp;
                    $model->updated_by = $logged_id;
                    $model->status = 1;
                    $model->distance = (isset($distance) && !is_null($distance) && $distance != '') ? $distance : 0;
                    if ($model->save()) {
                        $status = 1;
                        $message = "Workout has been upadted successfully!";
                    } else {
                        $status = 0;
                        $message = "Problem in saving model " . $this->arrayToString($model->getErrors());
                    }
                } else {
                    $status = 0;
                    $message = "Please provide weigt ,repetition and workout_id!";
                }
            } else {
                $status = 3;
                $message = "Please provide worktype id!";
            }
        } else {
            $status = 5;
            $message = "Your session is expired! Please Login !";
        }


        $result = array("status" => $status, "message" => $message);
        return $result;
    }

    public function actionAddTrainerWorkout() {

        $current_timestamp = date('Y-m-d h:i:s', time());
        $data = isset($_REQUEST['data']) ? $_REQUEST['data'] : '';
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $workout_date = isset($_REQUEST['workout_date']) ? $_REQUEST['workout_date'] : '';
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $logged_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';

        if (!empty($logged_id)) {
            if (isset($data) && !empty($data)) {
                // get age and gender to calculate calories
                if (!empty($user_id)) {
                    if ($type == "cardio") {
                        $detail_model = UserDetails::find()->where('user_id = :user_id', ['user_id' => $user_id])->one();
                        if (isset($detail_model)) {
                            $gender = $detail_model->gender;
                            $dob = $detail_model->dob;
                            $from = new \DateTime($dob);
                            $to = new \DateTime('today');
                            $age = $from->diff($to)->y;
                        } else {

                            $gender = '';
                            $age = '';
                        }

                        // get the weight and height from measurement or physical details table to claculate calorie
                        $mesurements = UserMeasurements::find()->where('user_id = :user_id', ['user_id' => $user_id])->orderBy('added_on desc')->one();

                        // if user did'nt fill measurements details
                        if (is_null($mesurements)) {
                            $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id' => $user_id])->one();
                            if (!is_null($mesurements)) {
                                $weight = $mesurements->current_weight;
                                $height = $mesurements->height;
                            } else {
                                $weight = NULL;
                                $height = NULL;
                            }
                        } else {
                            if (!empty($mesurements->weight) && !empty($mesurements->height) && $mesurements->weight != 0 && $mesurements->height != 0) {
                                $weight = $mesurements->weight;
                                $height = $mesurements->height;
                            } else {

                                $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id' => $user_id])->one();
                                if (!is_null($mesurements)) {
                                    $weight = $mesurements->weight;
                                    $height = $mesurements->height;
                                } else {
                                    $weight = NULL;
                                    $height = NULL;
                                }
                            }
                        }
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    $k = 0;
                    foreach ($data as $workout) {
                        $worktype_id = $workout['worktype_id'];
                        if ($worktype_id == 2) {
                            $workout_id = isset($workout['workout_id']) ? $workout['workout_id'] : '';
                            $duration = isset($workout['duration']) ? $workout['duration'] : '';
                            $notes = isset($workout['notes']) ? $workout['notes'] : '';
                            $distance = isset($workout['distance']) ? $workout['distance'] : 0;
                            if (!empty($gender) && !empty($age) && $age != 0) {
                                if (!is_null($weight) && !is_null($height) && $weight != 0 && $height != 0 && !empty($weight) && !empty($height)) {
                                    if ($k == 0) {
                                        //delete all old recods of this workout date
                                        $old_id = TrainerWorkout::find('id')->where('workout_date = :workout_date and user_id = :user_id and worktype_id = :worktype_id', [':workout_date' => $workout_date, ':user_id' => $user_id, ':worktype_id' => 2])->all();
                                        if (isset($old_id) && !empty($old_id)) {
                                            foreach ($old_id as $delete_id) {
                                                $delete_id->delete();
                                            }
                                        }
                                    }

                                    // get MET of workout from workout table
                                    $workout_model = Workouts::find()->where('id = :id', ['id' => $workout_id])->one();
                                    $met = $workout_model->met;
                                    if ($met == 0) {
                                        $duration_hour = $duration / 60;
                                        $speed = round($distance / $duration_hour);
                                        $workout_met_model = WorkoutsMet::find('met')->where('workout_id = :workout_id and min_speed <= :min_speed and max_speed > :max_speed', ['workout_id' => $workout_id, 'min_speed' => $speed, 'max_speed' => $speed])->one();
                                        $met = isset($workout_met_model) ? $workout_met_model->met : 0;
                                    }

                                    if ($gender == 1) {
                                        $bmr = 13.75 * $weight + (5 * $height) - (6.76 * $age) + 66;    // calculate bmr for male
                                        $calorie = ($bmr / 24) * $met * ($duration / 60);     // calculate calorie
                                    } else {
                                        $bmr = 9.56 * $weight + (1.85 * $height) - (4.68 * $age) + 655;  // calculate bmr for female
                                        $calorie = ($bmr / 24) * $met * ($duration / 60);     // calculate calorie
                                    }

                                    $add_model = new TrainerWorkout();
                                    $add_model->user_id = $user_id;
                                    $add_model->workout_id = $workout_id;
                                    $add_model->worktype_id = $worktype_id;
                                    $add_model->duration = $duration;
                                    $add_model->distance = (isset($distance) && !is_null($distance) && $distance != '') ? $distance : 0;
                                    $add_model->workout_date = $workout_date;
                                    $add_model->calories = round($calorie, 2);
                                    $add_model->notes = $notes;
                                    $add_model->added_on = $current_timestamp;
                                    $add_model->added_by = $logged_id;
                                    $add_model->updated_on = $current_timestamp;
                                    $add_model->updated_by = $logged_id;
                                    $add_model->is_complete = (Yii::$app->user->identity->isUser) ? 1 : 0;
                                    $add_model->status = 1;
                                    if ($add_model->save()) {
                                        $status = 1;
                                        $message = "Workout has added successfully!";
                                    } else {

                                        $status = 0;
                                        $message = "Problem in saving model " . $this->arrayToString($add_model->getErrors());
                                        $transaction->rollBack();
                                        break;
                                    }
                                } else {
                                    $status = 0;
                                    $message = "Please fill the weight and height in measurements or physical details";
                                    $transaction->rollBack();
                                    break;
                                }
                            } else {
                                $status = 0;
                                $message = "Please fill gender and dob!";
                                $transaction->rollBack();
                                break;
                            }
                        } else if (isset($worktype_id) && !empty($worktype_id)) {
                            $weight = isset($workout['weight']) ? $workout['weight'] : '';
                            $workout_id = isset($workout['workout_id']) ? $workout['workout_id'] : '';
                            $repetition = isset($workout['repetition']) ? $workout['repetition'] : '';
                            $notes = isset($workout['notes']) ? $workout['notes'] : '';

                            if (!empty($workout_id) && !empty($weight) && !empty($repetition)) {
                                if ($k == 0) {
                                    //delete all old recods of this workout date
                                    $old_id = TrainerWorkout::find('id')->where('workout_date = :workout_date and user_id = :user_id and worktype_id != :worktype_id', ['workout_date' => $workout_date, 'user_id' => $user_id, 'worktype_id' => 2])->all();
                                    if (isset($old_id) && !empty($old_id)) {
                                        foreach ($old_id as $delete_id) {
                                            $delete_id->delete();
                                        }
                                    }
                                }


                                $model = new TrainerWorkout();
                                $model->user_id = $user_id;
                                $model->workout_id = $workout_id;
                                $model->worktype_id = $worktype_id;
                                $model->workout_date = $workout_date;
                                $model->weight = $weight;
                                $model->repetition = $repetition;
                                $model->notes = $notes;
                                $model->added_on = $current_timestamp;
                                $model->added_by = $logged_id;
                                $model->updated_on = $current_timestamp;
                                $model->updated_by = $logged_id;
                                $model->status = 1;
                                $model->is_complete = (Yii::$app->user->identity->isUser) ? 1 : 0;
                                $model->distance = (isset($distance) && !is_null($distance) && $distance != '') ? $distance : 0;
                                if ($model->save()) {
                                    $status = 1;
                                    $message = "Workout has been added successfully!";
                                } else {
                                    $status = 0;
                                    $message = "Problem in saving model " . $this->arrayToString($model->getErrors());
                                    $transaction->rollBack();
                                    break;
                                }
                            } else {
                                $status = 0;
                                $message = "Please provide weigt ,repetition and workout_id!";
                                $transaction->rollBack();
                                break;
                            }
                        }
                        $k++;
                    }
                } else {
                    $status = 0;
                    $message = "Please provide user id!";
                }
            } else {

                $status = 2;
                $message = "Please provide parameters!";
            }
        } else {
            $status = 5;
            $message = "Your session is expired! Please Login !";
        }

        if ($status == 1) {
            $user_model = CenterUser::find()->where('id = :id', ['id' => $user_id])->one();
            $to = $user_model->email;
            $from = "contact@gympik.com";

            $subject = "workout plan for $workout_date .";
            $body = "Your workout is ready for $workout_date . Please check this out.";
            $sendMail = $this->sendviaMandrill($to, $subject, $body, $from, 'Team Sandbox');
            $transaction->commit();
        }
        $result = array("status" => $status, "message" => $message);
        return $result;
    }

    /*
      public function actionAddWorkOutn()
      {
      $current_timestamp = date('Y-m-d h:i:s', time());
      $data              = isset($_REQUEST['data'])?$_REQUEST['data']:'';
      $user_id           = isset($_REQUEST['user_id'])?$_REQUEST['user_id']:'';
      $workout_date      = isset($_REQUEST['workout_date'])?$_REQUEST['workout_date']:'';
      $type              = isset($_REQUEST['type'])?$_REQUEST['type']:'';
      $logged_id         = isset(Yii::$app->user->identity->id)?Yii::$app->user->identity->id:'';

      if(isset($data) && !empty($data))
      {
      // get age and gender to calculate calories
      if(!empty($user_id))
      {
      if($type=="cardio")
      {
      $detail_model   = UserDetails::find()->where('user_id = :user_id', ['user_id'=>$user_id])->one();
      if(isset($detail_model))
      {
      $gender   = $detail_model->gender;
      $dob      = $detail_model->dob;
      $from     = new \DateTime($dob);
      $to       = new \DateTime('today');
      $age      = $from->diff($to)->y;
      }
      else{

      $gender  = '';
      $age     = '';
      }

      // get the weight and height from measurement or physical details table to claculate calorie
      $mesurements  = UserMeasurements::find()->where('user_id = :user_id', ['user_id'=>$user_id])->orderBy('added_on desc')->one();

      // if user did'nt fill measurements details
      if(is_null($mesurements))
      {
      $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id'=>$user_id])->one();
      if(!is_null($mesurements))
      {
      $weight  = $mesurements->current_weight;
      $height  = $mesurements->height;
      }
      else{
      $weight  = NULL;
      $height  = NULL;
      }
      }
      else{
      if(!empty($mesurements->weight) && !empty($mesurements->height) && $mesurements->weight!=0 && $mesurements->height!=0)
      {
      $weight  = $mesurements->weight;
      $height  = $mesurements->height;
      }
      else{

      $mesurements = UserPhysicalDetails::find()->where('user_id = :user_id', ['user_id'=>$user_id])->one();
      if(!is_null($mesurements))
      {
      $weight  = $mesurements->weight;
      $height  = $mesurements->height;
      }
      else{
      $weight  = NULL;
      $height  = NULL;
      }

      }
      }
      }

      $transaction = Yii::$app->db->beginTransaction();
      $k=0;
      foreach($data as $workout)
      {
      $worktype_id  = $workout['worktype_id'];
      if($worktype_id == 2)
      {
      $workout_id  = $workout['workout_id'];
      $duration    = $workout['duration'];
      $notes       = $workout['notes'];
      if(!empty($gender) && !empty($age) && $age!=0)
      {
      if(!is_null($weight) && !is_null($height) && $weight!=0 && $height!=0 && !empty($weight) && !empty($height))
      {
      if($k==0)
      {
      //delete all old recods of this workout date
      $old_id       =   UserCardioWorkout::find('id')->where('workout_date = :workout_date and user_id = :user_id', ['workout_date'=>$workout_date, 'user_id'=>$user_id])->all();
      if(isset($old_id) && !empty($old_id))
      {
      foreach($old_id as $delete_id)
      {
      $delete_id->delete();
      }
      }
      }

      // get MET of workout from workout table
      $workout_model =  Workouts::find()->where('id = :id', ['id'=>$workout_id])->one();
      $met           =  $workout_model->met;

      if($gender==1)
      {
      $bmr           =  13.75 * $weight + (5*$height) -(6.76 * $age) + 66;    // calculate bmr for male
      $calorie       = ($bmr/24) * $met * ($duration/60);     // calculate calorie
      }
      else
      {
      $bmr           =  9.56 * $weight + (1.85*$height) - (4.68 * $age) + 655;  // calculate bmr for female
      $calorie       = ($bmr/24) * $met * ($duration/60);     // calculate calorie
      }

      $add_model                 =    new UserCardioWorkout();
      $add_model->user_id        =    $user_id;
      $add_model->workout_id     =    $workout_id;
      $add_model->duration       =    $duration;
      $add_model->workout_date   =    $workout_date;
      $add_model->calories       =    round($calorie,2);
      $add_model->notes          =    $notes;
      $add_model->added_on       =    $current_timestamp;
      $add_model->added_by       =    $logged_id;
      $add_model->updated_on     =    $current_timestamp;
      $add_model->updated_by     =    $logged_id;
      $add_model->is_complete    =    ($user_id==$logged_id)?1:0;
      $add_model->status          =    1;
      if($add_model->save())
      {
      $status  = 1;
      $message = "Workout has added successfully!";
      }
      else{

      $status  = 0;
      $message = "Problem in saving model ".$this->arrayToString($add_model->getErrors());
      $transaction->rollBack();
      break;
      }

      }
      else{
      $status  = 0;
      $message = "Please fill the weight and height in measurements or physical details";
      $transaction->rollBack();
      break;

      }

      }
      else{
      $status  = 0;
      $message = "Please fill gender and dob!";
      $transaction->rollBack();
      break;
      }

      }
      else if(isset($worktype_id) && !empty($worktype_id))
      {
      $weight        =     isset($workout['weight'])?$workout['weight']:'';
      $workout_id    =     isset($workout['workout_id'])?$workout['workout_id']:'';
      $repetition    =     isset($workout['repetition'])?$workout['repetition']:'';

      if(!empty($workout_id) && !empty($weight) && !empty($repetition))
      {
      if($k==0)
      {
      //delete all old recods of this workout date
      $old_id       =   UserNonCardioWorkout::find('id')->where('workout_date = :workout_date and user_id = :user_id', ['workout_date'=>$workout_date, 'user_id'=>$user_id])->all();
      if(isset($old_id) && !empty($old_id))
      {
      foreach($old_id as $delete_id)
      {
      $delete_id->delete();
      }
      }
      }


      $model                   =    new UserNonCardioWorkout();
      $model->user_id          =    $user_id;
      $model->workout_id       =    $workout_id;
      $model->workout_date     =    $workout_date;
      $model->weight           =    $weight;
      $model->repetition       =    $repetition;
      $model->added_on         =    $current_timestamp;
      $model->added_by         =    $logged_id;
      $model->updated_on       =    $current_timestamp;
      $model->updated_by       =    $logged_id;
      $model->status           =    1;
      $model->is_complete      =    ($user_id==$logged_id)?1:0;
      if($model->save())
      {
      $status  = 1;
      $message = "Workout has been added successfully!";
      }
      else{
      $status  = 0;
      $message = "Problem in saving model";
      $transaction->rollBack();
      break;
      }

      }else{
      $status  = 0;
      $message = "Please provide weigt ,repetition and workout_id!";
      $transaction->rollBack();
      break;
      }

      }
      $k++;
      }
      }
      else{

      $status  = 0;
      $message = "Please provide user id!";

      }
      }
      else{

      $status    = 2;
      $message   = "Please provide parameters!";

      }

      if($status==1)
      {
      $transaction->commit();
      }
      $result = array("status"=>$status,"message"=>$message);
      return $result;
      }
     */

    public function actionWorkoutCategoryList() {
        $model = Worktype::find()->all();
        $result = array();

        foreach ($model as $val) {
            $worktype['id'] = $val->id;
            $worktype['name'] = $val->name;

            array_push($result, $worktype);
        }

        $status = 1;
        $message = "Workout category list sucessfully fetch!";
        $final_result = array("status" => $status, "message" => $message, "result" => $result);
        return $final_result;
    }

    public function actionWorkoutList() {

        $category = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : '';
        $result = array();
        if (!empty($category)) {
            $model = Workouts::find()->where('worktype_id = :worktype_id', ['worktype_id' => $category])->all();
            foreach ($model as $val) {
                $workout['id'] = $val->id;
                $workout['worktype'] = $val->worktype_id;
                $workout['name'] = $val->name;
                $workout['distance'] = ($val->met == 0) ? 1 : 0;

                array_push($result, $workout);
            }

            $status = 1;
            $message = "Workout list has been fetched sucessfully for specified category";
        } else {

            $model = Workouts::find()->all();
            foreach ($model as $val) {
                $workout['id'] = $val->id;
                $workout['worktype'] = $val->worktype_id;
                $workout['name'] = $val->name;

                array_push($result, $workout);
            }

            $status = 1;
            $message = "Workout list of all category has been fetched sucessfully!";
        }

        $final_result = array("status" => $status, "message" => $message, "result" => $result);
        return $final_result;
    }

    public function actionGetUserWorkout() {

        $status = 1;
        $message = "Workout fetched Successfully";
        $trainer_cardio = array();
        $user_cardio = array();
        $trainer_noncardio = array();
        $user_noncardio = array();


        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $workout_date = isset($_REQUEST['workout_date']) ? $_REQUEST['workout_date'] : '';
        $data_for = isset($_REQUEST['for']) ? $_REQUEST['for'] : 'all';
        $trainer_cardio = array();
        $trainer_non_cardio = array();
        $user_cardio = array();
        $user_non_cardio = array();
        if (!empty($user_id) && !empty($workout_date)) {
            if ($data_for == 'trainer' || $data_for == 'all') {
                $trainer_model_cardio = TrainerWorkout::find()->select(['id', 'workout_id', 'worktype_id', 'duration', 'distance', 'calories', 'weight', 'repetition', 'notes'])->where('user_id = :user_id and workout_date = :workout_date AND worktype_id = :worktype_id', ['user_id' => $user_id, 'workout_date' => $workout_date, 'worktype_id' => 2])->all();
                if (isset($trainer_model_cardio) && !is_null($trainer_model_cardio) && !empty($trainer_model_cardio)) {
                    if ($data_for == 'all') {
                        foreach ($trainer_model_cardio as $trn_car) {
                            $trn_card_data = array();
                            $trn_card_data['id'] = isset($trn_car->id) ? $trn_car->id : "";
                            $trn_card_data['workout_name'] = isset($trn_car->workoutName->name) ? $trn_car->workoutName->name : "";
                            $trn_card_data['workout_type'] = isset($trn_car->workoutName->workoutType->name) ? $trn_car->workoutName->workoutType->name : "";
                            $trn_card_data['duration'] = isset($trn_car->duration) ? $trn_car->duration : "";
                            $trn_card_data['distance'] = isset($trn_car->distance) ? $trn_car->distance : "";
                            $trn_card_data['notes'] = isset($trn_car->notes) ? $trn_car->notes : "";
                            $trn_card_data['user_duration'] = isset($trn_car->userWorkOut->duration) ? $trn_car->userWorkOut->duration : "";
                            $trn_card_data['user_distance'] = isset($trn_car->userWorkOut->distance) ? $trn_car->userWorkOut->distance : "";
                            $trn_card_data['user_notes'] = isset($trn_car->userWorkOut->notes) ? $trn_car->userWorkOut->notes : "";
                            $trainer_cardio[] = $trn_card_data;
                        }
                    } else {
                        $trainer_cardio = $trainer_model_cardio;
                    }
                }

                $trainer_model_noncardio = TrainerWorkout::find()->select(['id', 'workout_id', 'worktype_id', 'duration', 'distance', 'calories', 'weight', 'repetition', 'notes'])->where('user_id = :user_id and workout_date = :workout_date AND worktype_id != :worktype_id', ['user_id' => $user_id, 'workout_date' => $workout_date, 'worktype_id' => 2])->all();
                if (isset($trainer_model_noncardio) && !is_null($trainer_model_noncardio) && !empty($trainer_model_noncardio)) {
                    if ($data_for == 'all') {
                        foreach ($trainer_model_noncardio as $trn_car) {
                            $trn_card_data = array();
                            $trn_card_data['id'] = isset($trn_car->id) ? $trn_car->id : "";
                            $trn_card_data['workout_name'] = isset($trn_car->workoutName->name) ? $trn_car->workoutName->name : "";
                            $trn_card_data['workout_type'] = isset($trn_car->workoutName->workoutType->name) ? $trn_car->workoutName->workoutType->name : "";
                            $trn_card_data['weight'] = isset($trn_car->weight) ? $trn_car->weight : "";
                            $trn_card_data['repetition'] = isset($trn_car->repetition) ? $trn_car->repetition : "";
                            $trn_card_data['notes'] = isset($trn_car->notes) ? $trn_car->notes : "";
                            $trn_card_data['user_weight'] = isset($trn_car->userWorkOut->weight) ? $trn_car->userWorkOut->weight : "";
                            $trn_card_data['user_repetition'] = isset($trn_car->userWorkOut->repetition) ? $trn_car->userWorkOut->repetition : "";
                            $trn_card_data['user_notes'] = isset($trn_car->userWorkOut->notes) ? $trn_car->userWorkOut->notes : "";
                            $trainer_non_cardio[] = $trn_card_data;
                        }
                    } else {
                        $trainer_non_cardio = $trainer_model_noncardio;
                    }
                }
            } elseif ($data_for == 'user') {
                $user_model_cardio = UserWorkout::find()->select(['trainer_workout_id', 'workout_id', 'worktype_id', 'duration', 'distance', 'calories', 'weight', 'repetition', 'notes'])->where('user_id = :user_id and workout_date = :workout_date AND worktype_id = :worktype_id', ['user_id' => $user_id, 'workout_date' => $workout_date, 'worktype_id' => 2])->all();
                if (isset($user_model_cardio) && !is_null($user_model_cardio) && !empty($user_model_cardio)) {
                    $user_cardio = $user_model_cardio;
                }
                $user_model_noncardio = UserWorkout::find()->select(['trainer_workout_id', 'workout_id', 'worktype_id', 'duration', 'distance', 'calories', 'weight', 'repetition', 'notes'])->where('user_id = :user_id and workout_date = :workout_date AND worktype_id != :worktype_id', ['user_id' => $user_id, 'workout_date' => $workout_date, 'worktype_id' => 2])->all();
                if (isset($user_model_noncardio) && !is_null($user_model_noncardio) && !empty($user_model_noncardio)) {
                    $user_non_cardio = $user_model_noncardio;
                }
            }
        } else {

            $status = 0;
            $message = "Pleae provide user id and workout date!";
        }
        $workouts = array();
        if ($data_for == 'trainer' || $data_for == 'all') {
            $workouts['cardio'] = $trainer_cardio;
            $workouts['noncardio'] = $trainer_non_cardio;
        } elseif ($data_for == 'user') {
            $workouts['cardio'] = $user_cardio;
            $workouts['noncardio'] = $user_non_cardio;
        }

        $result = array("status" => $status, "message" => $message, "result" => $workouts);
        return $result;
    }

    public function actionGetUsersDetails() {
//        $trainer_id = 1;
        $from_date = isset($_REQUEST["from_date"]) ? $_REQUEST["from_date"] : "";
        $to_date = isset($_REQUEST["to_date"]) ? $_REQUEST["to_date"] : "";
        $trainer_id = isset($_REQUEST["trainer_id"]) ? $_REQUEST["trainer_id"] : "";
        if ($from_date == "" || $to_date == "" || $trainer_id == "") {
            return array("status" => 0, "message" => "Date range & logged_id required");
        }
        $dates = array($from_date);
        while (1) {
            $date = date('d-m-Y', strtotime(end($dates) . ' +1 day'));
            $dates[] = $date;
            if ($date == $to_date)
                break;
        }
        $users = \common\models\CenterUser::find()
                ->where("personal_trainer_id = :personal_trainer_id AND in_que = :in_que", [":personal_trainer_id" => $trainer_id, "in_que" => 1])->select(['id', 'first_name', 'last_name'])
                ->all();
        $result = array();
        foreach ($users as $user) {
            $res = array();
            $res['id'] = $user->id;
            $res['name'] = $user->first_name . " " . $user->last_name;
            $last_workout = \common\models\UserWorkout::find()
                            ->where("user_id=:user_id", [':user_id' => $user->id])
                            ->orderBy("workout_date DESC")
                            ->select(['workout_date'])->one();
            $res['last_update'] = isset($last_workout->workout_date) ? $last_workout->workout_date : "";
            $trainer_workout = \common\models\TrainerWorkout::find()
                    ->select(["workout_date"])
                    ->where("user_id = :user_id AND workout_date >= :from_date AND workout_date <= :to_date", ['from_date' => $from_date, "to_date" => $to_date, "user_id" => $user->id])
                    ->all();
            $dates_f = \yii\helpers\ArrayHelper::map($trainer_workout, 'workout_date', 'workout_date');
            $tw_arr = array();
            foreach ($dates as $date) {
                $tw_arr[$date] = in_array($date, $dates_f) ? 1 : 0;
            }

            $res['work_out'] = $tw_arr;
            $result[] = $res;
        }
        return array("status" => 1, "message" => "user data fetched successfulyy", "result" => $result);
    }

    public function actionCalorieBurnt() {
        $user_id = '';
        if (isset(\Yii::$app->user->identity) && \Yii::$app->user->identity->isUser)
            $user_id = \Yii::$app->user->identity->id;
        else
            $user_id = isset($_REQUEST['user_id']) ? trim($_REQUEST['user_id']) : "";
        if ($user_id == '')
            return ["status" => 0, "message" => "user_id missing"];
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : '00-00-0000';
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date('d-m-Y');
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $conn = Yii::$app->db;
        $user_wo_tbl = UserWorkout::tableName();
        // calorie burnt what user did
        $users_sql = "SELECT workout_date,sum(calories) AS calories FROM $user_wo_tbl "
                . "WHERE worktype_id = 2 AND user_id = $user_id AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') <= '$to_date'"
                . "GROUP BY workout_date";
        $usr_qb = $conn->createCommand($users_sql);
        $users_data = $usr_qb->queryAll();
        $user_burnt_cal = array();
        if (is_array($users_data)) {
            foreach ($users_data as $row) {
                $user_burnt_cal['dates'][] = date('jS M', strtotime($row['workout_date']));
                $user_burnt_cal['calories'][] = $row['calories'];
            }
        }
        // calories burnt according to trainer's assigned workout
        $trainer_tbl = TrainerWorkout::tableName();
        $trainers_sql = "SELECT workout_date,sum(calories) AS calories FROM $trainer_tbl "
                . "WHERE worktype_id = 2 AND user_id = $user_id AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') <= '$to_date'"
                . "GROUP BY workout_date";
        $trainer_qb = $conn->createCommand($trainers_sql);
        $trainers_data = $trainer_qb->queryAll();
        $trainer_burnt_cal = array();
        if (is_array($trainers_data)) {
            foreach ($trainers_data as $row) {
                $trainer_burnt_cal['dates'][] = date('jS M', strtotime($row['workout_date']));
                $trainer_burnt_cal['calories'][] = $row['calories'];
            }
        }
        return ["status" => 1, "message" => "calories burnt fetched", "result" => ["user" => $user_burnt_cal, "trainer" => $trainer_burnt_cal]];
    }

    public function actionPersonalTrainerAddWorkout() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $data = isset($_REQUEST['data']) ? $_REQUEST['data'] : [];
        if ($user_id == "" || !count($data))
            return ["status" => 0, "message" => "user_id and data are required"];
        $transaction = Yii::$app->db->beginTransaction();
        $succes = TRUE;
        foreach ($data as $row) {
            $workout_date = isset($row['date']) ? $row['date'] : "";
            $exist = \common\models\PersonaltrainerWorkout::find()
                    ->where("user_id = :user_id AND workout_date = :workout_date", [":user_id" => $user_id, ":workout_date" => $workout_date])
                    ->one();
            $model = isset($exist->id) ? $exist : new \common\models\PersonaltrainerWorkout();
            $new_rec = false;
            if ($model->isNewRecord) {
                $model->added_on = date('Y-m-d H:i:s');
                $model->added_by = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
                $new_rec = TRUE;
            }
            $model->updated_on = date('Y-m-d H:i:s');
            $model->updated_by = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
            $model->user_id = $user_id;
            $model->workout_plan_cardio = isset($row['workout_plan_cardio']) ? $row['workout_plan_cardio'] : "";
            $model->workout_plan_noncardio = isset($row['workout_plan_noncardio']) ? $row['workout_plan_noncardio'] : "";
            $model->workout_date = $workout_date;
            $model->user_notes = isset($row['user_notes']) ? $row['user_notes'] : (isset($model->user_notes) ? $model->user_notes : "");
            $model->status = isset($row['status']) ? $row['status'] : (isset($model->status) ? $model->status : 0);
            try {
                if ($model->save()) {
                    
                } else {
                    $succes = FALSE;
                    $transaction->rollBack();
                    break;
                }
            } catch (Exception $exc) {
                $succes = FALSE;
                $transaction->rollBack();
                return ["status" => 0, "message" => "Personal Workout Failed to save"];
            }
        }
        if ($succes) {

            $transaction->commit();
            $user_model = \common\models\CenterUser::find()->where('id = :id', ['id' => $user_id])->one();
            $to = $user_model->email;
            $from = "contact@gympik.com";

            $subject = "workout plan for $workout_date by personal trainer.";
            $body = "Your workout is ready for $workout_date by personal trainer . Please check this out.";
            $sendMail = $this->sendviaMandrill($to, $subject, $body, $from, 'Team Sandbox');
            return ["status" => 1, "message" => "Personal Workout saved successfully"];
        } else {
            return ["status" => 0, "message" => "Personal Workout Failed to save"];
        }
    }

    public function actionGetPersonalTrainerWorkout() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        if (trim($user_id) == '')
            $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : '00-00-0000';
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date('d-m-Y');
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $date = (isset($_REQUEST['date']) && trim($_REQUEST['date']) != '') ? $_REQUEST['date'] : '';
        if ($user_id == "")
            return ["status" => 0, "message" => "user_id is required"];

        if ($date != '') {
            $date = date('Y-m-d', strtotime($date));
            $exist = \common\models\PersonaltrainerWorkout::find()
                ->select(["id", "workout_date", "workout_plan_cardio", "workout_plan_noncardio", "user_notes", "status"])
                ->where("user_id = :user_id AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') = :date", [":user_id" => $user_id, ":date" => $date])
                ->one();
            return ["status" => 1, "message" => "Personal workout fetched successfully", "result" => $exist];
        }
        $exist = \common\models\PersonaltrainerWorkout::find()
                ->select(["id", "workout_date", "workout_plan_cardio", "workout_plan_noncardio", "user_notes", "status"])
                ->where("user_id = :user_id AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') >= :from_date AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') <= :to_date", [":user_id" => $user_id, ":from_date" => $from_date, ":to_date" => $to_date])
                ->all();
        $ret = array();
        foreach ($exist as $rec) {
            $ret[$rec->workout_date] = [
                "id" => $rec->id,
                "workout_plan_cardio" => $rec->workout_plan_cardio,
                "workout_plan_noncardio" => $rec->workout_plan_noncardio,
                "user_notes" => $rec->user_notes,
                "status" => $rec->status
            ];
        }
        return ["status" => 1, "message" => "Personal workout fetched successfully", "result" => $ret];
        return ["status" => 0, "message" => "not found"];
    }

    public function actionMarkPersonalTrainerWorkout() {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        if (trim($id) == "")
            return ["status" => 0, "message" => "id is required"];
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : 0;
        $user_notes = isset($_REQUEST['user_notes']) ? $_REQUEST['user_notes'] : "";
        $model = \common\models\PersonaltrainerWorkout::findOne($id);
        if (isset($model->id)) {
            $model->status = $status;
            $model->user_notes = $user_notes;
            $model->updated_on = date("Y-m-d H:i:s");
            if (isset(\Yii::$app->user->identity->id))
                $model->updated_by = \Yii::$app->user->id;
            if ($model->save()) {
                return ["status" => 1, "message" => "Marked Successfully"];
            } else {
                return ["status" => 0, "message" => "Error : " . $this->arrayToString($model->getErrors())];
            }
        } else {
            return ["status" => 0, "message" => "not found"];
        }
    }

    public function actionGetWorkoutUserList() {
        $company_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $user_id = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : "";
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : date("d-m-Y", strtotime('monday this week'));
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : date("d-m-Y", strtotime('sunday this week'));
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));

        $dates = array();
        $current = strtotime($from_date);
        $last = strtotime($to_date);

        while ($current <= $last) {

            $dates[] = date("d-m-Y", $current);
            $current = strtotime("+1 day", $current);
        }

        $user_tbl = \common\models\CenterUser::tableName();
        $workout_tbl = \common\models\PersonaltrainerWorkout::tableName();
        $user_sql = "SELECT u.id,CONCAT(u.first_name,' ',u.last_name) name,IFNULL(w.workout_date,'') workout_date, w.status FROM $user_tbl u LEFT JOIN $workout_tbl w ON u.id = w.user_id AND STR_TO_DATE(w.workout_date,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(w.workout_date,'%d-%m-%Y') <= '$to_date'"
                . " WHERE u.center_id = '$company_id' AND u.role=6";
        if (in_array(\Yii::$app->user->identity->role, array(3, 4, 5)))
            $user_sql.= " AND (u.general_trainer_id = '$user_id' OR u.personal_trainer_id = '$user_id' OR u.nutritionist_id = '$user_id')";
//        $user_sql.=" GROUP BY u.id ORDER BY STR_TO_DATE(w.workout_date,'%d-%m-%Y') DESC";
//        echo $user_sql;
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($user_sql);
        $data = $query_bld->queryAll();
        $result = [];
        foreach ($data as $row) {
            $result[$row['id']]['id'] = $row['id'];
            $result[$row['id']]['name'] = $row['name'];
            if (!isset($result[$row['id']]['workout_date'])) {
                foreach ($dates as $dt) {
                    $result[$row['id']]['workout_date'][$dt] = -1;
//                    $result[$row['id']]['workout_date'][] = ["date" => $dt, "status" => ''];
                }
            }
            if ($row['workout_date'] != '')
                $result[$row['id']]['workout_date'][$row['workout_date']] = $row["status"];
//                $result[$row['id']]['workout_date'][] = ["date" => $row['workout_date'], "status" => $row["status"]];
        }
        return ["status" => 1, "message" => "success", "result" => $result];
    }

    public function actionGetStatistics() {
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        $user_id = ($user_id == '' && isset(\Yii::$app->user->identity->id)) ? \Yii::$app->user->identity->id : $user_id;
        if ($user_id == '')
            return ["status" => 0, "message" => "user id not found-unauth Access"];
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : $first_day_this_month;
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : $last_day_this_month;
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $wTbl = \common\models\PersonaltrainerWorkout::tableName();
        $sql = "SELECT status,count(1) no FROM $wTbl "
                . "WHERE user_id = $user_id AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') <= '$to_date' GROUP BY status";
        $conn = Yii::$app->db;
        $command = $conn->createCommand($sql);
        $data = $command->queryAll();
        $fdata = ["pending" => 0, "accepted" => 0, "declined" => 0, "total" => 0];
        foreach ($data as $row) {
            $key = "";
            if ($row['status'] == 0) {
                $key = "pending";
            } else if ($row['status'] == 1) {
                $key = "accepted";
            } else if ($row['status'] == 2) {
                $key = "declined";
            }
            $fdata[$key] = $row['no'];
        }
        $fdata["total"] = $fdata["pending"] + $fdata["accepted"] + $fdata["declined"];
        $return = [];
        $return['status'] = 1;
        $return["message"] = "Statistics fetched successfully";
        $return["result"] = $fdata;
        return $return;
    }

    public function actionReport() {

        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        if ($user_id == '')
            $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '';
        if ($user_id == '')
            return ['status' => 0, 'message' => "user_id required"];
        $first_day_this_month = date('01-m-Y'); // hard-coded '01' for first day
        $last_day_this_month = date('t-m-Y');
        $today = date('Y-m-d');
        $from_date = (isset($_REQUEST['from_date']) && trim($_REQUEST['from_date']) != '') ? $_REQUEST['from_date'] : $first_day_this_month;
        $to_date = (isset($_REQUEST['to_date']) && trim($_REQUEST['to_date']) != '') ? $_REQUEST['to_date'] : $last_day_this_month;
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        $data = \common\models\PersonaltrainerWorkout::find()
                ->where("user_id = $user_id AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(`workout_date`,'%d-%m-%Y') <= '$to_date'")
                ->select(["id", "workout_plan_cardio", "workout_plan_noncardio", "user_notes",
                    "workout_date" => "DATE_FORMAT(STR_TO_DATE(`workout_date`,'%d-%m-%Y'),'%d %b %Y')",
                    "status",
                    "status_txt" => "IF(status = 0,IF(STR_TO_DATE(`workout_date`,'%d-%m-%Y') < '$today' , 'Skipped','Pending'),IF(status = 1,'As Per Trainer',IF(status = 2,'As Own','N/A')))"
                ])
                ->asArray()
                ->all();
        return ['status' => 1, "message" => "report generated successfully", "result" => $data];
    }

}