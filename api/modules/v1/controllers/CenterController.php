<?php
namespace api\modules\v1\controllers;
use api\components\RestController;
class CenterController extends RestController{
   public function behaviors() {
        $no_auth_actions = [];
        $behaviors = parent::behaviors();
//        return $behaviors;
        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : \yii\filters\auth\HttpBasicAuth::className(),
            'except'=> $no_auth_actions
        ];
        return $behaviors;
    }
    public function actions() {
        $actions_c = [
            'getCenterDetails' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetCenterDetails']
            ] ,
            'editCenter' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionEditCenter']
            ] ,
            'addEditSubRole' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddEditSubRole']
            ] ,
            'getSubRoleList' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetSubRoleList']
            ] ,


        ];
        return $actions_c;
    }
    public function actionGetCenterDetails(){
        $center_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : '';
        if($center_id == '')
            return ["status"=>0,"message"=>"Unauth request"];
        
        $center_info = \common\models\Center::find()
                ->where("id = :center_id",[":center_id"=>$center_id])
                ->select(["id","center_name","city","locality","state","avtar","description"])
                ->one();
        $center['id'] = $center_info->id;
        $center['center_name'] = $center_info->center_name;
        $center['avtar'] = $center_info->avtar;
        $center['description'] = $center_info->description;
        $center['city']     = isset($center_info->city0->city)?$center_info->city0->city:'';
        $center['locality'] = isset($center_info->locality0->locality)?$center_info->locality0->locality:'';
        $center['state'] = isset($center_info->state0->state)?$center_info->state0->state:'';
        
        
        $center_user = \common\models\CenterUser::find()
                ->select(['no'=>'COUNT(*)','role'])
                ->where("center_id = :center_id",[":center_id"=>$center_id])
                ->groupBy(['role'])
                ->asArray()
                ->all();
         
         $center['nutritionist'] = 0;
         $center['user']  = 0;
         $center['general_trainer'] = 0;
         $center['personal_trainer'] = 0;
        
        if(isset($center_user) && !empty($center_user))
        {
        foreach($center_user as $obj)
        {   
           
            if($obj['role']==5)
            {
                $center['nutritionist'] = $obj['no'];
            }
            else if($obj['role']==6)
            {
                $center['user'] = $obj['no'];
            }
            else if($obj['role']==3)
            {
                 $center['general_trainer'] = $obj['no'];
            }
            else if($obj['role']==4)
            {
                  $center['personal_trainer'] = $obj['no'];
            }
        }
        }
        
        if(isset($center['id'])){
            return ["status"=>1,"message"=>"Center Details","result"=>$center];
        }
        return ["status"=>0,"message"=>"Center Details Not Found"];
    }
    
    public function actionEditCenter()
    {
        $name = isset($_REQUEST['name'])?$_REQUEST['name']:'';
        $description = isset($_REQUEST['description'])?$_REQUEST['description']:'';
        $avtar = isset($_REQUEST['avtar'])?$_REQUEST['avtar']:'';
        $center_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id :'';
        $logged_id   =  isset(\Yii::$app->user->identity->id)?\Yii::$app->user->identity->id:'';
        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());
        $status = -1;
        $message = '';
        
        
        if(!empty($name))
        {
            $model  = \common\models\Center::find()->where('id = :center_id', ['center_id'=>$center_id])->one();
            if(isset($model) && !empty($model) && isset($logged_id) && !empty($logged_id))
            { 
                $model->center_name = $name;
                $model->description = $description;
                $model->avtar = $avtar;
                $model->updated_by = $logged_id;
                $model->updated_on = $current_timestamp;
                if($model->save())
                {
                    $status = 1;
                    $message = "Center details updated sucessfully!";
                }else{
                    
                    $status = -2;
                    $message ="problem in saving model";
                }
            }
            else{
                $status = -2;
                $message = "Your session expired! Please login again";
                
            }
                    
        }
        else{
            $status = -1;
            $message = "Please enter center name!";
        }
        
        $result = array("status"=>$status,"message"=>$message);
        return $result;
            
    }
    public function actionAddEditSubRole(){
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
        if($id == ""){
            $model = new \common\models\CenterSubRole();
            $model->added_by = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
            $model->added_on = date('Y-m-d H:i:s');
        }else{
            $model = \common\models\CenterSubRole::findOne($id);
            if(!isset($model->id)){
                throw new NotFoundHttpException('The requested page does not exist.!!');
                exit;
            }
        }
        $is_new = $model->isNewRecord;
        $model->updated_by = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
        $model->updated_on = date('Y-m-d H:i:s');
        $model->role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : "";
        $model->center_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $model->role_name = isset($_REQUEST['role_name']) ? $_REQUEST['role_name'] : "";
        if($model->save()){
            $msg = $is_new ? "Saved" : "Updated";
            return ["status"=>1,"message"=>"Sub Role ".$msg." SuccessFully"];
        }else{
            return ["status"=>0,"message"=>"Error : ".$this->arrayToString($model->getErrors())];
        }
    }
    public function actionGetSubRoleList(){
        $center_id = isset(\Yii::$app->user->identity->center_id) ? \Yii::$app->user->identity->center_id : "";
        $role_id = isset($_REQUEST['role_id']) && trim($_REQUEST['role_id']) != '' ? $_REQUEST['role_id'] : '';
        if($center_id == ""){
            throw new \yii\web\UnauthorizedHttpException("UnAuth Access");
            exit;
        }
        $bld = \common\models\CenterSubRole::find();
        if($role_id != '')
            $bld->where("center_id = :center_id && role_id = :role_id",[":center_id"=>$center_id,':role_id'=>$role_id]);
        else
            $bld->where("center_id = :center_id",[":center_id"=>$center_id]);
        
        $bld->select(["id","role_id","role_name"]);
        $data = $bld->all();
        if(count($data))
            return ["status"=>1,"message"=>"SubRoles listed successfully","result"=>$data];
        else
            return ["status"=>0,"message"=>"Empty Not Found Anything"];
    }
}