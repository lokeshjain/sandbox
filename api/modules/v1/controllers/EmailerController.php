<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\EmailerGroup;
use common\models\EmailerList;
use common\models\EmailerCompose;

/* sdasd */

//error_reporting(0);
//class UserController extends Controller
class EmailerController extends RestController {

    public function behaviors() {
        $no_auth_actions = []; //["createGroup"];
        $behaviors = parent::behaviors();
//        return $behaviors;
//        if (in_array($this->action->id, $no_auth_actions))
//            return $behaviors;
        $behaviors['authenticator'] = [
            'class' => (isset($_REQUEST['access-token']) && trim($_REQUEST['access-token']) != '') ? \yii\filters\auth\QueryParamAuth::className() : HttpBasicAuth::className(),
            'except' => $no_auth_actions
        ];
        return $behaviors;
    }

    public function actions() {
        $actions_c = [
            'createGroup' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionCreateGroup']
            ],
            'getGroups' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionGetGroups']
            ],
            'sendComposed' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionSendComposed']
            ],
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
    }

    public function actionIndex() {
        
    }

    public function actionCreateGroup() {
        $file_name = isset($_FILES['email_list_file']['name']) ? $_FILES['email_list_file']['name'] : "";
        $tmp_name = isset($_FILES['email_list_file']['tmp_name']) ? $_FILES['email_list_file']['tmp_name'] : "";
        $data = isset($_REQUEST['data']) ? $_REQUEST['data'] : "";
        parse_str($data, $data);

        $center_id = isset(Yii::$app->user->identity->center_id) ? Yii::$app->user->identity->center_id : '0';
        $filename = $center_id . "-" . date("Y-m-d_H-i-s_") . $file_name;
        $file_up_path = Yii::$app->basePath . "/uploaded_file/emailer_xls/$filename";

        $model = new EmailerGroup();
        $model->attributes = $data;
        $model->added_by = $model->updated_by = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : 0;
        $model->added_on = $model->updated_on = date('Y-m-d H:i:s');
        $model->excel_file = $filename;
        $model->center_id = $center_id;
        $data_valid = $model->validate();
        $uploaded = FALSE;
        $message = "";
        if ($data_valid) {
            $uploaded = move_uploaded_file($tmp_name, $file_up_path);
            if ($uploaded) {
                // convert excel to csv
                $csv_convert_cmd = "ssconvert $file_up_path $file_up_path.csv";
                $converted = exec($csv_convert_cmd);
            } else {
                $message = "Unable to upload file";
            }
        }
        if ($data_valid && $uploaded) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $model->save(FALSE);
                $handle = @fopen("$file_up_path.csv", "r");
                if ($handle) {
                    $batch_insert_data = [];
                    $isFirst = TRUE;
                    while (!feof($handle)) {
                        $buffer = fgetcsv($handle, 4096);
                        if ($isFirst) {
                            $isFirst = FALSE;
                            continue;
                        }
//                        if (isset($buffer[1]) && trim($buffer[1]) != '')
                            $batch_insert_data[] = [
                                "name" => @$buffer[0],
                                "email" => @$buffer[1],
                                "group_id" => $model->id
                            ];
                    }
                    fclose($handle);
                    $row_inserted = $valid_count = 0;

                    if (count($batch_insert_data)) {
                        $row_inserted = \Yii::$app->db->createCommand()
                                        ->batchInsert(
                                                '{{%emailer_list_trash}}', ["name", "email", "group_id"], $batch_insert_data
                                        )->execute();

                        // delete duplicate emails this is not needed
                       /* $delete_duplicate = "DELETE n1 FROM {{%emailer_list}} n1,{{%emailer_list}} n2 "
                                . "WHERE n1.group_id = {$model->id} AND n1.id > n2.id AND n1.email = n2.email";
                        // delete duplicate emails FROM each group
                        $delete_duplicate = "DELETE m1 FROM sb__emailer_list m1 "
                                . "LEFT JOIN sb__emailer_list m2 ON (m1.group_id = {$model->id} AND m1.email = m2.email AND m1.id < m2.id) "
                                . "WHERE m2.id IS NOT NULL";
//                        $deleted_rows = Yii::$app->db->createCommand($delete_duplicate)->execute();
                        * 
                        */
                        $insert_unique = "INSERT INTO {{%emailer_list}} (name,email,group_id) SELECT name,email,group_id FROM {{%emailer_list_trash}} WHERE group_id = {$model->id} AND TRIM(email) != '' GROUP BY email";
                        $valid_count = Yii::$app->db->createCommand($insert_unique)->execute();
                        $flush_temp_emails = \yii::$app->db->createCommand()
                            ->delete('{{%emailer_list_trash}}', ["group_id"=>$model->id])
                            ->execute();
                    }
                     $transaction->commit();  
                    return [
                        "status" => 1,
                        "message" => "Group Created Successfully , Total Rows $row_inserted (valid emails $valid_count) ",
                        "result" => [
                            "total_rows" => $row_inserted,
                            "valid_rows" => $valid_count,
//                            "query" => $delete_duplicate
                        ]
                    ];
                }
            } catch (Exception $exc) {
                $transaction->rollBack();
                return [
                        "status" => 0,
                        "message" => "Error ".$exc->getTraceAsString(),
                    ];
            }
        } else {
            return ["status" => 0, "message" => "Error : $message, " . $this->arrayToString($model->getErrors())];
        }
    }
    public function actionGetGroups(){
        $center_id = isset(Yii::$app->user->identity->center_id) ? Yii::$app->user->identity->center_id : '0';
        $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '0';
        $sql = "SELECT grp.id, grp.name, COUNT(1) cnt  FROM {{%emailer_group}} grp LEFT JOIN {{%emailer_list}} lst ON grp.id = lst.group_id "
                . "WHERE grp.center_id = $center_id "
                . "GROUP BY lst.group_id "
                ."ORDER BY grp.id DESC";
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        return ['status'=>1,"message"=>"emailer group fetched successfully", "result"=>$data];
    }
    public function actionSendComposed(){
        $center_id = isset(Yii::$app->user->identity->center_id) ? Yii::$app->user->identity->center_id : '0';
        $user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : '0';
        $post_data = (isset($_POST['Compose'])) ? $_POST['Compose'] : [];
        if(isset($post_data['is_test']) && $post_data['is_test']){
            $to = str_replace(" ", "", $post_data['test_email']);
            $to = explode(",", $to);
            $result_email = array();
            array_walk($to, function (&$value,$key) use (&$result_email) {
                $result_email[] = ["User", $value];
            });
            $to = $result_email;
           
            $subject = $post_data['subject'];
            $body = $post_data['body'];
            $from = $post_data['from_email'];
            $fromname = $post_data['from_name'];
            $tag = "TEST BY center : $center_id & user : $user_id";
            $ret = $this->sendviaMandrill($to, $subject, $body, $from, $fromname, $tag);
            if($ret)
                return ["status"=>1,"message"=>"Test email sent to ({$post_data['test_email']})"];
            else
                return ["status"=>0,"message"=>"Unable to Send ({$post_data['test_email']})"];
            exit;
        }else{
            $model = new EmailerCompose();
            $model->attributes = $post_data;
            $model->sent_on = date('Y-m-d H:i:s',  strtotime($model->sent_on));
            $model->center_id = $center_id;
            $model->added_by = $model->updated_by = $user_id;
            $model->added_on = $model->updated_on = date('Y-m-d H:i:s');
            if($model->save()){
                return ["status"=>1, "message"=>"Email Sending Will start on <b><i>{$post_data['sent_on']}</i></b>"];
            }else{
                return ["status"=>0, "message"=>"Error: ".$this->arrayToString($model->getErrors())];
            }
            exit;
        }
        return ["manas"];
    }
}
