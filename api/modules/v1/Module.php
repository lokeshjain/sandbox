<?php
namespace api\modules\v1;

/**
 * Gympik API V1 Module
 * 
 * @author Manas Pakal <msg4manas@gmail.com>
 * @since 1.0
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();        
    }
}
