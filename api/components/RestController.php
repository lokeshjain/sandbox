<?php
/**
 * Gympik API Rest controller
 * 
 * @author Manas Pakal <msg4manas@gmail.com>
 * @since 1.0
 */
namespace api\components;

use yii\rest\ActiveController;

class RestController extends ActiveController {

    public $adminEmail = 'manas.pakal@gympik.com';
    public $adminName = 'Traq Team';
    public $modelClass = '';
    public $pageSize = 5;
    public $pagination = true;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'result',
    ];
    public $message = "Success";
    public $status = 1;
    public function init() {
        parent::init();
        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->loginUrl = NULL;
    }
    public function afterAction($action, $result)
    {
        if(\Yii::$app->response->format == 'jsonp' && isset($_REQUEST['callback']) && trim($_REQUEST['callback']))
            $result = ['callback'=>$_REQUEST['callback'],'data'=>$result];
        $result = parent::afterAction($action, $result);
        $result = array_merge(["status"=>  $this->status,"message"=>  $this->message],$result);
        return $this->serializeData($result);
    }
    public function beforeAction($action) {
//        error_log("Custom".  print_r($_REQUEST,TRUE));
//        \Yii::warning(\Yii::$app->request->url." = ".print_r($_REQUEST,TRUE), "ReqestData");
        parent::beforeAction($action);
        \Yii::$app->response->format = isset($_REQUEST['callback']) && trim($_REQUEST['callback']) != '' ? \yii\web\Response::FORMAT_JSONP : \yii\web\Response::FORMAT_JSON;
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSONP;
        return true;
    }
  /*  public function afterAction($action, $result) {
        \Yii::warning(\Yii::$app->request->url." = ".print_r($result,TRUE), "ResponseData");
        return $result;
        parent::afterAction($action, $result);
    }*/

    public function arrayToString($arr) {
        $err = "";
        foreach ($arr as $erra) {
            foreach ($erra as $fld => $e)
                $err.="$e,";
        }
        return trim($err, ',');
    }

    public function sendviaMandrill($to, $subject, $body, $from, $fromname = '',$tag = '', $cc = '', $ccname = '', $bcc = '', $bccname = '', $template='', $attach = '') {
        $fromname = ($fromname != '') ? $fromname : "Traq Team";
        require_once \Yii::getAlias('@app') .'/../common/libraries/mandrill/class.phpmailer.php';
        $mail = new \PHPMailer;
        $mail->IsSMTP();                                      // Set mailer to use SMTP
         $mail->Host = 'smtp.mandrillapp.com';
        $mail->Port = '587';
        $mail->SMTPAuth = true;
        $mail->Username = 'afixi.manas09@gmail.com';
        $mail->Password = '1x0pLq1TNH4ph9iMlJuWSg';
        $mail->SMTPSecure = 'tls';
        $mail->From = $from;
        $mail->FromName = $fromname;
        //Here for multiple recipients
        if(is_array($to) && !empty($to)){
	        //Header not to show other recipients in the mail
	        $mail->addCustomHeader('X-MC-PreserveRecipients', "false");
	        //Here added tags to mail content
	        if($tag)
        	    $mail->addCustomHeader('X-MC-Tags', $tag);
				// below line is temporary
				//$mail->addCustomHeader('X-MC-SigningDomain', 'gmail.com');
		foreach ($to as $k => $v) {
                    if (is_array($v)) {
                        if (strlen(trim($v[0]))){
                            $mail->AddAddress($v[1], $v[0]);
                            $mail->addCustomHeader('X-MC-MergeVars', '{"_rcpt": "'.$v[1].'", "NAME": "'.$v[0].'"}');
                            if(isset($v[2]) && is_array($v[2]) && count($v[2])){
                                foreach($v[2] as $tag=>$tag_val){
                                    $mail->addCustomHeader('X-MC-MergeVars', '{"_rcpt": "'.$v[1].'", "'.$tag.'": "'.$tag_val.'"}');
                                }
                            }
                        }
                        else{
                            $mail->AddAddress($v[1],$v[1]);
                        }
                    }else {
                        $mail->AddAddress($v,$v);
                    }
		}
        }else {
            $mail->AddAddress($to,$to);  // Add a recipient
        }
        // default name
        $mail->addCustomHeader('X-MC-MergeVars', '{"NAME": "User"}');
						
        //Here for CC and BCC
        if ($cc)
            $mail->addCC($cc, $ccname);
        if ($bcc)
            $mail->addBCC($bcc, $bccname);
			
        //add template
        if($template)
            $mail->addCustomHeader('X-MC-Template', trim($template));
			
        $mail->IsHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = strip_tags($body);

        //Here goes attachment
	if(is_array($attach)){
            foreach($attach as $p){
                if ($p && file_exists($p))
                    $mail->addAttachment($p);
            }
        }else{
            if ($attach && file_exists($attach))
                $mail->addAttachment($attach);
        }

       /* if ($attach && file_exists($attach))
            $mail->addAttachment($attach);*/



        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }

}
