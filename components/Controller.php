<?php

namespace app\components;

use Yii;
use yii\base\InlineAction;

class Controller extends \yii\web\Controller {
    public $adminEmail = 'manas.pakal@gympik.com';
    public $adminName = 'Traq Team';
    public function sendviaMandrill($to, $subject, $body, $from, $fromname = '',$tag = '', $cc = '', $ccname = '', $bcc = '', $bccname = '', $template='', $attach = '') {
        $fromname = ($fromname != '') ? $fromname : "Traq Team";
        require_once \Yii::getAlias('@app') .'/common/libraries/mandrill/class.phpmailer.php';
        $mail = new \PHPMailer;
        $mail->IsSMTP();                                      // Set mailer to use SMTP
         $mail->Host = 'smtp.mandrillapp.com';
        $mail->Port = '587';
        $mail->SMTPAuth = true;
        $mail->Username = 'afixi.manas09@gmail.com';
        $mail->Password = '1x0pLq1TNH4ph9iMlJuWSg';
        $mail->SMTPSecure = 'tls';
        $mail->From = $from;
        $mail->FromName = $fromname;
        //Here for multiple recipients
        if(is_array($to) && !empty($to)){
	        //Header not to show other recipients in the mail
	        $mail->addCustomHeader('X-MC-PreserveRecipients', "false");
	        //Here added tags to mail content
	        if($tag)
        	    $mail->addCustomHeader('X-MC-Tags', $tag);
				// below line is temporary
				//$mail->addCustomHeader('X-MC-SigningDomain', 'gmail.com');
		foreach ($to as $k => $v) {
                    if (is_array($v)) {
                        if (strlen(trim($v[0]))){
                            $mail->AddAddress($v[1], $v[0]);
                            $mail->addCustomHeader('X-MC-MergeVars', '{"_rcpt": "'.$v[1].'", "NAME": "'.$v[0].'"}');
                        }
                        else{
                            $mail->AddAddress($v[1],$v[1]);
                        }
                    }else {
                        $mail->AddAddress($v,$v);
                    }
		}
        }else {
            $mail->AddAddress($to,$to);  // Add a recipient
        }
        // default name
        $mail->addCustomHeader('X-MC-MergeVars', '{"NAME": "User"}');
						
        //Here for CC and BCC
        if ($cc)
            $mail->addCC($cc, $ccname);
        if ($bcc)
            $mail->addBCC($bcc, $bccname);
			
        //add template
        if($template)
            $mail->addCustomHeader('X-MC-Template', trim($template));
			
        $mail->IsHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = strip_tags($body);

        //Here goes attachment
	if(is_array($attach)){
            foreach($attach as $p){
                if ($p && file_exists($p))
                    $mail->addAttachment($p);
            }
        }else{
            if ($attach && file_exists($attach))
                $mail->addAttachment($attach);
        }

       /* if ($attach && file_exists($attach))
            $mail->addAttachment($attach);*/



        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }
}