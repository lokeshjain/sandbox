<?php
$ips = getIPs();
if((isset($_SERVER['SERVER_ADDR']) && in_array($_SERVER['SERVER_ADDR'],['127.0.0.1'])) || (isset($ips[0]) && $ips[0] == '127.0.0.1'))
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=sandbox',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => 'sb__'
];
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=sandbox',
    'username' => 'devuser',
    'password' => 'Gympik1!',
    'charset' => 'utf8',
    'tablePrefix' => 'sb__'
];


function getIPs($withV6 = true) {
    preg_match_all('/inet'.($withV6 ? '6?' : '').' addr: ?([^ ]+)/', `ifconfig`, $ips);
    return $ips[1];
}