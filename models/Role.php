<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%sb__center_user_role}}".
 *
 * @property integer $id
 * @property string $role
 * @property integer $status
 *
 * @property SbCenterUser[] $sbCenterUsers
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%center_user_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'status'], 'required'],
            [['status'], 'integer'],
            [['role'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSbCenterUsers()
    {
        return $this->hasMany(SbCenterUser::className(), ['role' => 'id']);
    }
}
