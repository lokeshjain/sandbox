<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Center;

/**
 * CenterSearch represents the model behind the search form about `app\models\Center`.
 */
class CenterSearch extends Center
{
//    public $cityName;
//    public $localityName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id', 'city', 'locality', 'state', 'added_by', 'updated_by', 'status'], 'integer'],
            [['center_name', 'description', 'added_on', 'updated_on','state','city', 'locality','status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $cityTbl = City::tableName();
        $localityTbl = Locality::tableName();
        $stateTbl = State::tableName();
        $query = Center::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith(['city0','locality0','state0']);
        $query->andFilterWhere(['like', 'center_name', $this->center_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', "$cityTbl.city", $this->city])
            ->andFilterWhere(['like', "$localityTbl.locality", $this->locality])
            ->andFilterWhere([$this->tableName().'.status'=>  $this->status])
            ->andFilterWhere(['like', "$stateTbl.state", $this->state]);
        
        $dataProvider->sort->attributes['city'] = [
            'asc' => [$cityTbl.'.city' => SORT_ASC],
            'desc' => [$cityTbl.'.city' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['locality'] = [
            'asc' => [$localityTbl.'.locality' => SORT_ASC],
            'desc' => [$localityTbl.'.locality' => SORT_DESC],
        ];
        return $dataProvider;
    }
}
