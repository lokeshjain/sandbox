<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%personaltrainer_workout}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $workout_plan
 * @property string $user_notes
 * @property string $workout_date
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $status
 */
class PersonaltrainerWorkout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%personaltrainer_workout}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'workout_date', 'added_by', 'updated_by', 'added_on', 'updated_on'], 'required'],
            [['user_id', 'added_by', 'updated_by', 'status'], 'integer'],
            [['workout_plan_cardio', 'workout_plan_noncardio' , 'user_notes'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
//            [['workout_type'], 'in','range' => [0,1], "message"=>"{attribute} must be 0 or 1"],
            [['status'], 'in','range' => [0,1,2], "message"=>"{attribute} must be 0 / 1 / 2"],
            [['workout_date'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'workout_plan' => 'Workout Plan',
            'user_notes' => 'User Notes',
            'workout_date' => 'Workout Date',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
}
