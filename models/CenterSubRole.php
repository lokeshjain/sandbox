<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%center_sub_role}}".
 *
 * @property integer $id
 * @property integer $role_id
 * @property integer $center_id
 * @property string $role_name
 * @property integer $added_by
 * @property integer $updated_by
 * @property string $added_on
 * @property string $updated_on
 */
class CenterSubRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%center_sub_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'center_id', 'role_name'], 'required'],
            [['role_id', 'center_id', 'added_by', 'updated_by'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['role_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'center_id' => 'Center ID',
            'role_name' => 'Role Name',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
        ];
    }
}
