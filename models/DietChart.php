<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sb__diet_chart".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property string $pre_breakfast
 * @property string $pre_breakfast_time
 * @property string $breakfast
 * @property string $breakfast_time
 * @property string $later_morning_meal
 * @property string $later_morning_meal_time
 * @property string $lunch
 * @property string $lunch_time
 * @property string $late_afternoon
 * @property string $late_afternoon_time
 * @property string $evening_tea
 * @property string $evening_tea_time
 * @property string $dinner
 * @property string $dinner_time
 * @property string $post_dinner
 * @property string $post_dinner_time
 * @property string $feedback
 * @property string $added_on
 * @property integer $added_by
 * @property string $updated_on
 * @property integer $updated_by
 * @property integer $status
 */
class DietChart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%diet_chart}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date','added_by'], 'required'],
            [['user_id', 'added_by', 'updated_by', 'status'], 'integer'],
            [['feedback'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
            [['date', 'pre_breakfast_time', 'breakfast_time', 'later_morning_meal_time', 'lunch_time', 'late_afternoon_time', 'evening_tea_time', 'dinner_time', 'post_dinner_time'], 'string', 'max' => 50],
            [['pre_breakfast', 'breakfast', 'later_morning_meal', 'lunch', 'late_afternoon', 'evening_tea', 'dinner', 'post_dinner'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'pre_breakfast' => 'Pre Breakfast',
            'pre_breakfast_time' => 'Pre Breakfast Time',
            'breakfast' => 'Breakfast',
            'breakfast_time' => 'Breakfast Time',
            'later_morning_meal' => 'Later Morning Meal',
            'later_morning_meal_time' => 'Later Morning Meal Time',
            'lunch' => 'Lunch',
            'lunch_time' => 'Lunch Time',
            'late_afternoon' => 'Late Afternoon',
            'late_afternoon_time' => 'Late Afternoon Time',
            'evening_tea' => 'Evening Tea',
            'evening_tea_time' => 'Evening Tea Time',
            'dinner' => 'Dinner',
            'dinner_time' => 'Dinner Time',
            'post_dinner' => 'Post Dinner',
            'post_dinner_time' => 'Post Dinner Time',
            'feedback' => 'Feedback',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
}
