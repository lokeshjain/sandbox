<?php

namespace app\models;

use Yii;
use common\models\Role;
/**
 * This is the model class for table "sb__center_user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile
 * @property string $password
 * @property integer $role
 * @property integer $center_id
 * @property integer $general_trainer_id
 * @property integer $personal_trainer_id
 * @property integer $nutritionist_id
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $status
 *
 * @property CenterAssociation[] $centerAssociations
 * @property CenterUserRole $role0
 * @property CenterUser $nutritionist
 * @property CenterUser[] $centerUsers
 * @property Center $center
 * @property CenterUser $addedBy
 * @property CenterUser $updatedBy
 */
class CenterUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%center_user}}';
    }
    public static function tempTableName()
    {
        return '{{%center_user_temp}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'email', 'mobile', 'role', 'center_id'], 'required'],
            [['email'],'email'],
            [['role', 'sub_role', 'center_id', 'general_trainer_id', 'personal_trainer_id', 'nutritionist_id', 'added_by', 'updated_by', 'status'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 150],
            [['email'], 'string', 'max' => 50],
            [['mobile'], 'string', 'max' => 20],
            [['password'], 'string', 'max' => 70],
            [['email', 'mobile'], 'unique']
//            [['email', 'mobile'], 'unique', 'targetAttribute' => ['email', 'mobile'], 'message' => 'The combination of Email and Mobile has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Center Admin First Name',
            'last_name' => 'Center Admin Last Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'password' => 'Password',
            'role' => 'Role',
            'center_id' => 'Center ID',
            'general_trainer_id' => 'General Trainer ID',
            'personal_trainer_id' => 'Personal Trainer ID',
            'nutritionist_id' => 'Nutritionist ID',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenterAssociations()
    {
        return $this->hasMany(CenterAssociation::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }
    public function getLocality0()
    {
        return $this->hasOne(\common\models\Locality::className(), ['id' => 'locality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionist()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'nutritionist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenterUsers()
    {
        return $this->hasMany(CenterUser::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenter()
    {
        return $this->hasOne(Center::className(), ['id' => 'center_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddedBy()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'added_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'updated_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'id']);
    }
    public function fields() {
        $fields = parent::fields();
        unset($fields['password']);
        $fields['role'] = function ($model){
            return isset($model->role0->role) ? $model->role0->role : 'N/A';  
        };
        $fields['role_id'] = function ($model){
            return $model->role;  
        };
        return $fields;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralTrainer()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'general_trainer_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalTrainer()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'personal_trainer_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutrition()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'nutritionist_id']);
    }
}
