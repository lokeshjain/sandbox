<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%center}}".
 *
 * @property integer $id
 * @property string $center_name
 * @property integer $city
 * @property integer $locality
 * @property integer $state
 * @property string $description
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $status
 *
 * @property City $city0
 * @property Locality $locality0
 * @property State $state0
 */
class Center extends \yii\db\ActiveRecord
{
    public $role,$subrole,$excel;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%center}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['center_name', 'city', 'locality', 'state', 'description', 'added_by', 'updated_by', 'status'], 'required'],
            [['city', 'locality', 'state', 'added_by', 'updated_by', 'status'], 'integer'],
            [['description'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
            [['center_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'center_name' => 'Center Name',
            'city' => 'City',
            'locality' => 'Locality',
            'state' => 'State',
            'description' => 'Description',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity0()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocality0()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState0()
    {
        return $this->hasOne(State::className(), ['id' => 'state']);
    }
}
