<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%emailer_compose}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $center_id
 * @property string $subject
 * @property string $from_name
 * @property string $from_email
 * @property string $body
 * @property integer $added_by
 * @property integer $updated_by
 * @property string $added_on
 * @property string $updated_on
 * @property string $sent_on
 * @property integer $completed
 */
class EmailerCompose extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%emailer_compose}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'center_id', 'subject', 'from_name', 'from_email', 'body', 'added_by', 'updated_by'], 'required'],
            [['group_id', 'center_id', 'added_by', 'updated_by', 'completed'], 'integer'],
            [['body'], 'string'],
            [['added_on', 'updated_on', 'sent_on'], 'safe'],
            [['subject', 'from_name', 'from_email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'center_id' => 'Center ID',
            'subject' => 'Subject',
            'from_name' => 'From Name',
            'from_email' => 'From Email',
            'body' => 'Body',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'sent_on' => 'Sent On',
            'completed' => 'Completed',
        ];
    }
}
