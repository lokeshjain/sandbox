<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

//$this->title =  "Update Center | {$model->center_name}";
?>
<div class="center-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php
        $roles = \app\models\Role::find()
                ->select(["id","role"])
                ->where("status AND id IN (2,3,4,5,6)")
                ->all();
        $roles = \yii\helpers\ArrayHelper::map($roles, 'id', 'role');
    ?>
    <?= $form->field($model, 'role')->dropDownList($roles,
    [
        "prompt"=>"Select Role",
        "class"=>'form-control select2-use',
        "onchange"=>"$('#center-subrole').html('<option val=\"\">Select Subrole</option>');"
            . "$.post('".Yii::$app->urlManager->createAbsoluteUrl(["center/get-subroles","center_id"=>$model->id])."',{role_id:$(this).val()},function(res){"
            . "$('#center-subrole').html(res);"
            . "})"
    ]) ?>
    <?= $form->field($model, 'subrole')->dropDownList([],
    [
      "prompt"=>"Select SubRole",  
      "class"=>'form-control select2-use',
    ]        
    ); ?>
    <?= $form->field($model, 'excel')->fileInput(); ?>
    <div class="form-group">
        <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>
        Download Sample excel : <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(["center/download-sample","file"=>'trainers']) ?>">Trainers</a> | 
        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(["center/download-sample","file"=>'members']) ?>">Members</a>
    </div>
    <?php ActiveForm::end(); ?>
</div>