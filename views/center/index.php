<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Centers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="center-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Center', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php yii\widgets\Pjax::begin(["timeout"=>10000,'clientOptions'=>['container'=>'pajax-container']]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'=>$searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'center_name',
            [
                'attribute'=>'city',
                'value'=>'city0.city'
            ],
            [
                'attribute'=>'locality',
                'value'=>'locality0.locality'
            ],
            [
                'attribute'=>'state',
                'value'=>'state0.state'
            ],
            [
                'attribute'=>'status',
                'value'=>  function ($data){
                    return isset($data->status) && $data->status ? "Active" : "InActive";
                },
                "filter"=> Html::activeDropDownList($searchModel, 'status',["1"=>"Active","0"=>"InActive"],['class'=>'form-control','prompt' => 'Status'])
            ],
            // 'description:ntext',
            // 'added_on',
            // 'updated_on',
            // 'added_by',
            // 'updated_by',
            // 'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{upload-users}&nbsp{update}&nbsp;{delete}',
                'buttons'=> [
                    'upload-users'=>function ($url, $model, $key) {
                        return Html::a('Upload Users', $url);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php yii\widgets\Pjax::end(); ?>

</div>
