<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Center */
/* @var $form yii\widgets\ActiveForm */
$this->title = ($model->isNewRecord) ? 'Add Centers' : "Update Center | {$model->center_name}";
$this->registerJsFile(Yii::$app->request->baseUrl."/js/select2.min.js",
        ['depends' => [
            yii\bootstrap\BootstrapAsset::className(),
             yii\web\JqueryAsset::className()
            ]
        ]
      );
$this->registerCssFile(Yii::$app->request->baseUrl."/css/select2.css",
        ['depends' => [yii\bootstrap\BootstrapAsset::className()],]
      );
?>

<div class="center-form">

    <?php $form = ActiveForm::begin([
        /*'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],*/
        'fieldConfig' => [
//            'template' => "<div class='col-md-6'>{label}{input}{error}</div>",
//            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'center_name')->textInput(['maxlength' => 255]) ?>
    <?php
        $states = \app\models\State::find()->all();
        $states = \yii\helpers\ArrayHelper::map($states, 'id', 'state');
    ?>
    <?= $form->field($model, 'state')->dropDownList($states,
    [
        "prompt"=>"Select State",
        "class"=>'form-control select2-use',
        "onchange"=>"$('#center-city').html('<option val=\"\">Select City</option>');"
        . "$('#center-locality').html('<option val=\"\">Select Locality</option>');"
        . " $.post('".Yii::$app->urlManager->createAbsoluteUrl(['service/get-city'])."',{state_id:$(this).val()},function(res){"
        . "$('#center-city').html(res);"
        . "}) "
    ]) ?>
    <?php
    $cityList = [];
    if(isset($model->state)){
         $cities = \app\models\City::find()->select(["id","city"])->where(["state"=>$model->state])->asArray()->all();
         $cityList = \yii\helpers\ArrayHelper::map($cities, 'id', 'city');
    }
    ?>
    <?= $form->field($model, 'city')->dropDownList($cityList,
            [
                "prompt" => "Select City",
                "onchange"=>"$('#center-locality').html('<option val=\"\">Select Locality</option>');"
                . " $.post('".Yii::$app->urlManager->createAbsoluteUrl(['service/get-locality'])."',{city_id:$(this).val()},function(res){"
                . "$('#center-locality').html(res);"
                . "}) "
            ]
            ) ?>
    <?php
    $localityList = [];
    if(isset($model->city)){
        $localities = \app\models\Locality::find()->select(["id","locality"])->where(["city"=>$model->city])->asArray()->all();
        $localityList = yii\helpers\ArrayHelper::map($localities, 'id', 'locality');
    }
    ?>
    <?= $form->field($model, 'locality')->dropDownList($localityList,["prompt"=>"Select Locality"]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model_admin, 'first_name')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model_admin, 'last_name')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model_admin, 'email')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model_admin, 'mobile')->textInput(['maxlength' => 10,'minlength'=>10]) ?>

    <?= $form->field($model, 'status')->dropDownList(["1"=>"Active","0"=>"In Active"]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
