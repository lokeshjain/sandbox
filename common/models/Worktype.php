<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__worktype".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $status
 * @property string $adddate
 */
class Worktype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__worktype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'status'], 'integer'],
            [['adddate'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'status' => 'Status',
            'adddate' => 'Adddate',
        ];
    }
}
