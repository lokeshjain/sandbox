<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%emailer_group}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $about
 * @property integer $center_id
 * @property integer $added_by
 * @property integer $updated_by
 * @property string $added_on
 * @property string $updated_on
 * @property string $excel_file
 */
class EmailerGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%emailer_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'about', 'center_id', 'added_by', 'updated_by','excel_file'], 'required'],
            [['excel_file'], "checkExtensions"],
            [['about'], 'string'],
            [['center_id', 'added_by', 'updated_by'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }
    public function checkExtensions($attribute, $params)
    {
        $ext = pathinfo($this->excel_file, PATHINFO_EXTENSION);
        if(!in_array($ext, ["xlsx","xls"]))
            $this->addError($attribute, ".$ext file is not allowed");
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'about' => 'About',
            'center_id' => 'Center ID',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
        ];
    }
}
