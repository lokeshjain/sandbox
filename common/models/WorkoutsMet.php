<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__workouts_met".
 *
 * @property integer $id
 * @property integer $workout_id
 * @property double $min_speed
 * @property double $max_speed
 * @property double $met
 */
class WorkoutsMet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__workouts_met';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['workout_id', 'min_speed', 'max_speed', 'met'], 'required'],
            [['workout_id'], 'integer'],
            [['min_speed', 'max_speed', 'met'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'workout_id' => 'Workout ID',
            'min_speed' => 'Min Speed',
            'max_speed' => 'Max Speed',
            'met' => 'Met',
        ];
    }
}
