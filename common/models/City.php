<?php

namespace common\models;

use Yii;
use common\models\State;
/**
 * This is the model class for table "sb__city".
 *
 * @property integer $id
 * @property string $city
 * @property integer $state
 * @property integer $status
 *
 * @property SbCenter[] $sbCenters
 * @property SbState $state0
 * @property SbLocality[] $sbLocalities
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'status'], 'required'],
            [['state', 'status'], 'integer'],
            [['city'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'state' => 'State',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSbCenters()
    {
        return $this->hasMany(SbCenter::className(), ['city' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState0()
    {
        return $this->hasOne(State::className(), ['id' => 'state']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSbLocalities()
    {
        return $this->hasMany(SbLocality::className(), ['city' => 'id']);
    }
}
