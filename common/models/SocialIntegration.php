<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%social_integration}}".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $fb_app_id
 * @property string $fb_app_secret
 * @property string $fb_page_id
 * @property string $fb_page_name
 * @property string $fb_page_access_token
 * @property string $tw_app_consumer_key
 * @property string $tw_app_consumer_secret
 * @property string $tw_access_token
 * @property string $tw_access_token_secret
 * @property string $created_on
 * @property string $updated_on
 */
class SocialIntegration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_integration}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'fb_app_id', 'fb_app_secret', 'fb_page_id', 'fb_page_name', 'fb_page_access_token'], 'required', 'on'=>"facebook"],
            [['company_id', 'tw_app_consumer_key', 'tw_app_consumer_secret', 'tw_access_token', 'tw_access_token_secret','tw_screen_name','tw_user_id'], 'required', 'on'=>"twitter"],
            [['company_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['fb_app_id', 'fb_app_secret', 'fb_page_id', 'fb_page_name', 'fb_page_access_token', 'tw_app_consumer_key', 'tw_app_consumer_secret', 'tw_access_token', 'tw_access_token_secret'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'fb_app_id' => 'Fb App ID',
            'fb_app_secret' => 'Fb App Secret',
            'fb_page_id' => 'Fb Page ID',
            'fb_page_name' => 'Fb Page Name',
            'fb_page_access_token' => 'Fb Page Access Token',
            'tw_app_consumer_key' => 'Tw App Consumer Key',
            'tw_app_consumer_secret' => 'Tw App Consumer Secret',
            'tw_access_token' => 'Tw Access Token',
            'tw_access_token_secret' => 'Tw Access Token Secret',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
