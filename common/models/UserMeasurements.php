<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__user_measurements".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $weight
 * @property double $height
 * @property double $neck
 * @property double $shoulder
 * @property double $chest_in
 * @property double $chest_exp
 * @property double $u_abd
 * @property double $l_abd
 * @property double $waist
 * @property double $hips
 * @property double $u_thighs
 * @property double $l_thighs
 * @property double $calves
 * @property double $right_u_arm
 * @property double $right_l_arm
 * @property double $left_u_arm
 * @property double $left_l_arm
 * @property double $wrist
 * @property double $rhr
 * @property string $due_date
 * @property string $added_on
 * @property integer $added_by
 * @property string $updated_on
 * @property integer $updated_by
 * @property string $notes
 * @property integer $status
 */
class UserMeasurements extends \yii\db\ActiveRecord
{
    public $date_diff;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__user_measurements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','due_date', 'status'], 'required'],
            [['due_date'], 'checkFuture'],
            [['user_id', 'added_by', 'updated_by', 'status'], 'integer'],
            [['weight', 'height', 'neck', 'shoulder', 'chest_in', 'chest_exp', 'u_abd', 'l_abd', 'waist', 'hips', 'u_thighs', 'l_thighs', 'calves', 'right_u_arm', 'right_l_arm', 'left_u_arm', 'left_l_arm', 'wrist', 'rhr'], 'number'],
            [['added_on', 'updated_on'], 'safe'],
            [['notes'], 'string'],
            [['due_date'], 'string', 'max' => 50]
        ];
    }
    public function checkFuture($attribute,$params)
    {
        $due = new \DateTime($this->{$attribute});
        $today = new \DateTime(date('Y-m-d'));
        if($due <= $today){
            $this->addError($attribute,'Due Date is not a future Date');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'weight' => 'Weight',
            'height' => 'Height',
            'neck' => 'Neck',
            'shoulder' => 'Shoulder',
            'chest_in' => 'Chest In',
            'chest_exp' => 'Chest Exp',
            'u_abd' => 'U Abd',
            'l_abd' => 'L Abd',
            'waist' => 'Waist',
            'hips' => 'Hips',
            'u_thighs' => 'U Thighs',
            'l_thighs' => 'L Thighs',
            'calves' => 'Calves',
            'right_u_arm' => 'Right U Arm',
            'right_l_arm' => 'Right L Arm',
            'left_u_arm' => 'Left U Arm',
            'left_l_arm' => 'Left L Arm',
            'wrist' => 'Wrist',
            'rhr' => 'Rhr',
            'due_date' => 'Due Date',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'notes' => 'Notes',
            'status' => 'Status',
        ];
    }
    public function getUserdata()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'user_id']);
    }
    public function getUserdetails()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'user_id']);
    }
}
