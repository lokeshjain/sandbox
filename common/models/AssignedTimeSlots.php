<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__assigned_time_slots".
 *
 * @property integer $id
 * @property integer $trainer_id
 * @property integer $member_id
 * @property integer $time_from
 * @property integer $time_to
 * @property integer $day
 * @property integer $added_by
 * @property integer $updated_by
 * @property string $added_on
 * @property string $updated_on
 */
class AssignedTimeSlots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%assigned_time_slots}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trainer_id', 'member_id', 'time_from', 'time_to', 'day', 'added_by', 'updated_by'], 'required'],
            [['trainer_id', 'member_id', 'time_from', 'time_to', 'day', 'added_by', 'updated_by'], 'integer'],
            [['added_on', 'updated_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trainer_id' => 'Trainer ID',
            'member_id' => 'Member ID',
            'time_from' => 'Time From',
            'time_to' => 'Time To',
            'day' => 'Day',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
        ];
    }
}
