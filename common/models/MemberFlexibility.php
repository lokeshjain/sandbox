<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__member_flexibility".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $company_id
 * @property double $sit_reach
 * @property double $shoulder_left
 * @property double $shoulder_right
 * @property integer $left_leg_standing
 * @property integer $right_leg_standing
 * @property string $added_on
 * @property integer $added_by
 */
class MemberFlexibility extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__member_flexibility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'company_id', 'left_leg_standing', 'right_leg_standing', 'added_by'], 'integer'],
            [['sit_reach', 'shoulder_left', 'shoulder_right'], 'number'],
            [['added_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'company_id' => 'Company ID',
            'sit_reach' => 'Sit Reach',
            'shoulder_left' => 'Shoulder Left',
            'shoulder_right' => 'Shoulder Right',
            'left_leg_standing' => 'Left Leg Standing',
            'right_leg_standing' => 'Right Leg Standing',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
        ];
    }
}
