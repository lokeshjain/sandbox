<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__notification".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $trainer_id
 * @property string $assessment
 * @property string $due_date
 * @property string $added_on
 * @property integer $added_by
 * @property string $updated_on
 * @property integer $updated_by
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'trainer_id', 'assessment', 'due_date', 'added_by', 'updated_by'], 'required'],
            [['member_id', 'trainer_id', 'added_by', 'updated_by'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['assessment', 'due_date'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'trainer_id' => 'Trainer ID',
            'assessment' => 'Assessment',
            'due_date' => 'Due Date',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
}
