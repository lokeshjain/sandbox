<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__member_strength".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $company_id
 * @property integer $bench_press1
 * @property integer $bench_press2
 * @property integer $bench_press3
 * @property integer $legpress1
 * @property integer $legpress2
 * @property integer $legpress3
 * @property string $added_on
 * @property integer $added_by
 */
class MemberStrength extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__member_strength';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'company_id', 'bench_press1', 'bench_press2', 'bench_press3', 'legpress1', 'legpress2', 'legpress3', 'added_by'], 'integer'],
            [['added_on'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'company_id' => 'Company ID',
            'bench_press1' => 'Bench Press1',
            'bench_press2' => 'Bench Press2',
            'bench_press3' => 'Bench Press3',
            'legpress1' => 'Legpress1',
            'legpress2' => 'Legpress2',
            'legpress3' => 'Legpress3',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
        ];
    }
}
