<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__trainer_workout".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $worktype_id
 * @property integer $workout_id
 * @property double $duration
 * @property double $distance
 * @property double $calories
 * @property double $weight
 * @property double $repetition
 * @property string $workout_date
 * @property string $notes
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $status
 * @property integer $is_complete
 */
class TrainerWorkout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__trainer_workout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'worktype_id', 'workout_id', 'workout_date', 'added_by', 'updated_by', 'status', 'is_complete'], 'required'],
            [['user_id', 'worktype_id', 'workout_id', 'added_by', 'updated_by', 'status', 'is_complete'], 'integer'],
            [['duration', 'distance', 'calories', 'weight', 'repetition'], 'number'],
            [['notes'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
            [['workout_date'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'worktype_id' => 'Worktype ID',
            'workout_id' => 'Workout ID',
            'duration' => 'Duration',
            'distance' => 'Distance',
            'calories' => 'Calories',
            'weight' => 'Weight',
            'repetition' => 'Repetition',
            'workout_date' => 'Workout Date',
            'notes' => 'Notes',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'is_complete' => 'Is Complete',
        ];
    }
    
    public function getWorkoutName()
    {
        return $this->hasOne(Workouts::className(), ['id' => 'workout_id']);
    }
    public function getUserWorkOut()
    {
        return $this->hasOne(UserWorkout::className(), ['trainer_workout_id' => 'id']);
    }
     public function fields() {
        $fields = parent::fields();
        $fields['workout_name'] = function ($model){
            return isset($model->workoutName->name) ? $model->workoutName->name : '';  
        };
        return $fields;
    }
}
