<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__user_cardio_workout".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $workout_id
 * @property double $duration
 * @property double $distance
 * @property double $calories
 * @property string $workout_date
 * @property string $notes
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $status
 * @property integer $is_complete
 */
class UserCardioWorkout extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__user_cardio_workout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'workout_id', 'duration', 'calories','workout_date', 'added_by', 'is_complete'], 'required'],
            [['user_id', 'workout_id', 'added_by', 'updated_by', 'status', 'is_complete'], 'integer'],
            [['duration', 'distance', 'calories'], 'number'],
            [['notes'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
            [['workout_date'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'workout_id' => 'Workout ID',
            'duration' => 'Duration',
            'distance' => 'Distance',
            'calories' => 'Calories',
            'workout_date' => 'Workout Date',
            'notes' => 'Notes',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
            'is_complete' => 'Is Complete',
        ];
    }
    
    public function getWorkoutName()
    {
        return $this->hasOne(Workouts::className(), ['id' => 'workout_id']);
    }
}
