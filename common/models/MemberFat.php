<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__member_fat".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $company_id
 * @property string $calculation_type
 * @property integer $chest
 * @property integer $abd
 * @property integer $thigh
 * @property integer $triceps
 * @property integer $subscapular
 * @property integer $suprailliac
 * @property integer $mid_axillary
 * @property integer $bicep
 * @property integer $lower_back
 * @property integer $calf
 * @property double $fat_percentage
 * @property string $added_on
 * @property integer $added_by
 */
class MemberFat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__member_fat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'company_id', 'chest', 'abd', 'thigh', 'triceps', 'subscapular', 'suprailliac', 'mid_axillary', 'bicep', 'lower_back', 'calf', 'added_by'], 'integer'],
            [['fat_percentage'], 'number'],
            [['added_on'], 'safe'],
            [['calculation_type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'company_id' => 'Company ID',
            'calculation_type' => 'Calculation Type',
            'chest' => 'Chest',
            'abd' => 'Abd',
            'thigh' => 'Thigh',
            'triceps' => 'Triceps',
            'subscapular' => 'Subscapular',
            'suprailliac' => 'Suprailliac',
            'mid_axillary' => 'Mid Axillary',
            'bicep' => 'Bicep',
            'lower_back' => 'Lower Back',
            'calf' => 'Calf',
            'fat_percentage' => 'Fat Percentage',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
        ];
    }
}
