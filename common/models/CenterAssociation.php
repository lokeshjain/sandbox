<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%center_association}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $association_start
 * @property string $association_end
 * @property integer $center_id
 *
 * @property CenterUser $user
 */
class CenterAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%center_association}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'center_id'], 'integer'],
            [['association_start', 'association_end'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'association_start' => 'Association Start',
            'association_end' => 'Association End',
            'center_id' => 'Center ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'user_id']);
    }
}
