<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%emailer_list}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property integer $group_id
 * @property string $sent_on
 */
class EmailerList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%emailer_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name', 'group_id'], 'required'],
            [['group_id'], 'integer'],
            [['sent_on'], 'safe'],
            [['email', 'first_name', 'last_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'group_id' => 'Group ID',
            'sent_on' => 'Sent On',
        ];
    }
}
