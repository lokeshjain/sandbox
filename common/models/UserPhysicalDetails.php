<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__user_physical_details".
 *
 * @property integer $id
 * @property double $current_weight
 * @property double $goal_weight
 * @property double $height
 * @property string $medical_history
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $user_id
 */
class UserPhysicalDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_physical_details}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['current_weight', 'goal_weight', 'height'], 'required', 'on'=>['notmhis']],
            [['medical_history'], 'required', 'except'=>['notmhis']],
            [['current_weight', 'goal_weight', 'height'], 'number'],
            [['medical_history'], 'string'],
            [['added_on', 'updated_on'], 'safe'],
            [['added_by', 'updated_by', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'current_weight' => 'Current Weight',
            'goal_weight' => 'Goal Weight',
            'height' => 'Height',
            'medical_history' => 'Medical History',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'user_id' => 'User ID',
        ];
    }
    public function fields() {
        $fields = parent::fields();
        $fields['medical_history'] = function ($model){
            $mh = json_decode($model->medical_history,TRUE);
            if(is_null($mh))
                $mh = array();
            return $mh;
        };
        return $fields;
    }
}
