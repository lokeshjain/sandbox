<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__workouts".
 *
 * @property integer $id
 * @property integer $worktype_id
 * @property string $name
 * @property string $intensity
 * @property double $met
 * @property integer $status
 */
class Workouts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__workouts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worktype_id', 'name'], 'required'],
            [['worktype_id', 'status'], 'integer'],
            [['met'], 'number'],
            [['name', 'intensity'], 'string', 'max' => 255]
        ];
    }
    public function getWorkoutType()
    {
        return $this->hasOne(Worktype::className(), ['id' => 'worktype_id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worktype_id' => 'Worktype ID',
            'name' => 'Name',
            'intensity' => 'Intensity',
            'met' => 'Met',
            'status' => 'Status',
        ];
    }
}
