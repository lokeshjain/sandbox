<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%attendance}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_id
 * @property string $month
 * @property integer $year
 * @property string $attendance
 */
class Attendance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attendance}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'month', 'year'], 'required'],
            [['attendance'], 'required','on'=>'default'],
            [['user_id', 'company_id', 'year'], 'integer'],
            [['month'], 'string', 'max' => 20],
            [['month'],  function ($attribute, $params) {
                if($this->$attribute !== ""){
                    $mon_str = jdmonthname(gregoriantojd($this->$attribute, 1, 1), CAL_MONTH_GREGORIAN_LONG);
                    if($mon_str == "")
                        $this->addError ($attribute, "Incorrect Month Giveen");
                }
            }],
            [['attendance'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'company_id' => 'Company ID',
            'month' => 'Month',
            'year' => 'Year',
            'attendance' => 'Attendance',
        ];
    }
}
