<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__member_endurance".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $company_id
 * @property integer $knee_sit_repetition
 * @property integer $knee_sit_time
 * @property integer $push_up_repetition
 * @property integer $push_up_time
 * @property integer $core
 * @property string $step_test1
 * @property string $step_test2
 * @property string $step_test3
 * @property string $step_test4
 * @property string $added_on
 * @property integer $added_by
 */
class MemberEndurance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__member_endurance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'company_id', 'knee_sit_repetition', 'knee_sit_time', 'push_up_repetition', 'push_up_time', 'core', 'added_by'], 'integer'],
            [['added_on'], 'safe'],
            [['step_test1', 'step_test2', 'step_test3', 'step_test4'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'company_id' => 'Company ID',
            'knee_sit_repetition' => 'Knee Sit Repetition',
            'knee_sit_time' => 'Knee Sit Time',
            'push_up_repetition' => 'Push Up Repetition',
            'push_up_time' => 'Push Up Time',
            'core' => 'Core',
            'step_test1' => 'Step Test1',
            'step_test2' => 'Step Test2',
            'step_test3' => 'Step Test3',
            'step_test4' => 'Step Test4',
            'added_on' => 'Added On',
            'added_by' => 'Added By',
        ];
    }
}
