<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%social_post_data}}".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $added_by
 * @property integer $updated_by
 * @property integer $added_on
 * @property integer $updated_on
 * @property integer $social_type
 * @property string $post_data
 * @property string $post_id
 */
class SocialPostData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_post_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'added_by', 'updated_by', 'added_on', 'updated_on', 'social_type', 'post_data', 'post_id'], 'required'],
            [['company_id', 'added_by', 'updated_by', 'social_type'], 'integer'],
            [['post_data'], 'string'],
            [['post_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'social_type' => 'Social Type',
            'post_data' => 'Post Data',
            'post_id' => 'Post ID',
        ];
    }
}
