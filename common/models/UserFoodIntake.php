<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__user_foodintake".
 *
 * @property integer $id
 * @property integer $diet_id
 * @property integer $user_id
 * @property string $date
 * @property string $time
 * @property integer $meal_type
 * @property string $food_intake
 * @property string $addded_on
 */
class UserFoodIntake extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_foodintake}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date', 'meal_type'], 'required'],
            [['diet_id', 'user_id'], 'integer'],
            [['addded_on'], 'safe'],
            [['date', 'time', 'food_intake'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'diet_id' => 'Diet ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'time' => 'Time',
            'meal_type' => 'Meal Type',
            'food_intake' => 'Food Intake',
            'addded_on' => 'Addded On',
        ];
    }
}
