<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__state".
 *
 * @property integer $id
 * @property string $state
 *
 * @property SbCity[] $sbCities
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%state}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state'], 'required'],
            [['state'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state' => 'State',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSbCities()
    {
        return $this->hasMany(SbCity::className(), ['state' => 'id']);
    }
}
