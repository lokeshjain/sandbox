<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sb__locality".
 *
 * @property integer $id
 * @property integer $city
 * @property string $locality
 *
 * @property SbCenter[] $sbCenters
 * @property SbCity $city0
 */
class Locality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%locality}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'locality'], 'required'],
            [['city'], 'integer'],
            [['locality'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'locality' => 'Locality',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSbCenters()
    {
        return $this->hasMany(SbCenter::className(), ['locality' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity0()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }
}
