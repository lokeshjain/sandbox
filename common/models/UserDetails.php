<?php

namespace common\models;
use common\models\Locality;
use common\models\City;
use Yii;

/**
 * This is the model class for table "sb__user_details".
 *
 * @property integer $id
 * @property string $avtar
 * @property string $address
 * @property string $addredd_line2
 * @property integer $locality
 * @property integer $city
 * @property integer $state
 * @property integer $pincode
 * @property string $dob
 * @property integer $gender
 * @property integer $user_id
 * @property string $added_on
 * @property string $updated_on
 * @property integer $added_by
 * @property integer $updated_by
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sb__user_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['locality','city', /*'address', 'city', 'state', 'pincode',*/ 'dob', 'gender', 'user_id', 'added_by'], 'required'],
            [['locality', 'city', 'state', 'pincode', 'gender', 'user_id', 'added_by', 'updated_by'], 'integer'],
            [['added_on', 'updated_on'], 'safe'],
            [['avtar', 'address'], 'string', 'max' => 255],
            [['dob'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avtar' => 'Avtar',
            'address' => 'Address Line1',
            'locality' => 'Locality',
            'city' => 'City',
            'state' => 'State',
            'pincode' => 'Pincode',
            'dob' => 'Dob',
            'gender' => 'Gender',
            'user_id' => 'User ID',
            'added_on' => 'Added On',
            'updated_on' => 'Updated On',
            'added_by' => 'Added By',
            'updated_by' => 'Updated By',
        ];
    }
    public function getLocality0()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality']);
    }
    public function getCity0()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }
    public function getMedicalDetails()
    {
        return $this->hasOne( UserPhysicalDetails::className(), ['user_id' => 'user_id']);
    }
}
