<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%center_user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        return static::findOne(['email' => $token]);
//        \Yii::$app->response->format = isset($_REQUEST['callback']) && trim($_REQUEST['callback']) != '' ? \yii\web\Response::FORMAT_JSONP : \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return static::findOne(['md5(email)' => $token]);
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    public function getIsManager(){
        return isset($this->role) && $this->role == 1;
    }
    public function getIsAdmin(){
        return isset($this->role) && $this->role == 9;
    }
    public function getIsGeneralTrainer(){
        return isset($this->role) && $this->role == 3;
    }
    public function getIsPersonalTrainer(){
        return isset($this->role) && $this->role == 4;
    }
    public function getIsNutritionist(){
        return isset($this->role) && $this->role == 5;
    }
    public function getIsUser(){
        return isset($this->role) && $this->role == 6;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralTrainer()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'general_trainer_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalTrainer()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'personal_trainer_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutrition()
    {
        return $this->hasOne(CenterUser::className(), ['id' => 'nutritionist_id']);
    }
}
