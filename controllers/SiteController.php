<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index'],
                'rules' => [
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        if (!\Yii::$app->user->isGuest) {
            $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl('center'));
        }
        return $this->render('index');
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
//            $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl('center'));
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
            $this->redirect(\Yii::$app->urlManager->createAbsoluteUrl('center'));
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionEmail() {
       /* $mandrill = new \Mandrill('1x0pLq1TNH4ph9iMlJuWSg');
        $message = array(
            'subject' => 'Test message',
            'from_email' => 'you@yourdomain.com',
            'html' => '<p>this *|FNAME|* is a test *|NAME|* message with *|FIRSTNAME|* Mandrill\'s PHP wrapper!. *|LASTNAME|*</p>',
            'to' => array(
                array('email' => 'manas.pakal@gympik.com', 'name' => 'Manas Pakal'),
                array('email' => 'msg4manas@gmail.com')
                ),
            'merge_vars' => array(array(
                    'rcpt' => 'manas.pakal@gympik.com',
                    'vars' =>
                    array(
                        array(
                            'name' => 'FIRSTNAME',
                            'content' => 'Manas'),
                        array(
                            'name' => 'LASTNAME',
                            'content' => 'Pakal')
        ))));

        $template_name = '';//'Stationary';

        $template_content = array(
            array(
                'name' => 'main',
                'content' => 'Hi *|FIRSTNAME|* *|LASTNAME|*, thanks for signing up.'),
            array(
                'name' => 'footer',
                'content' => 'Copyright 2012.')
        );

        print_r($mandrill->messages->send($message));
//        print_r($mandrill->messages->sendTemplate($template_name, $template_content, $message));
        exit;

        $uri = 'https://mandrillapp.com/api/1.0/messages/send.json';

        $postString = '{
"key": "1x0pLq1TNH4ph9iMlJuWSg",
"message": {
    "html": "this is the emails html content",
    "text": "this is the emails text content",
    "subject": "this is the subject",
    "from_email": "someone@example.com",
    "from_name": "John",
    "to": [
        {
            "email": "manas.pakal@gympik.com",
            "name": "Manas Pakal"
        }
    ],
    "headers": {

    },
    "track_opens": true,
    "track_clicks": true,
    "auto_text": true,
    "url_strip_qs": true,
    "preserve_recipients": true,

    "merge": true,
    "global_merge_vars": [

    ],
    "merge_vars": [

    ],
    "tags": [

    ],
    "google_analytics_domains": [

    ],
    "google_analytics_campaign": "...",
    "metadata": [

    ],
    "recipient_metadata": [

    ],
    "attachments": [

    ]
},
"async": false
}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);

        $result = curl_exec($ch);

        echo $result;
        exit;*/


//        $this->import('common\mandrill\class.phpmailer');
        require_once __DIR__ .'/../common/libraries/mandrill/class.phpmailer.php';
        $mail = new \PHPMailer();
        $mail->IsSMTP();
        $mail->Host = 'smtp.mandrillapp.com';
        $mail->Port = '587';
        $mail->SMTPAuth = true;
        $mail->Username = 'afixi.manas09@gmail.com';
        $mail->Password = '1x0pLq1TNH4ph9iMlJuWSg';
        $mail->SetFrom('afixi.manas09@gmail.com', 'Manas Pakal');
        $mail->Subject = "test in yii2";
        //   $mail->Body = $body;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $body = "<b>Manas here</b>";
        $mail->MsgHTML($body);
        $email = "manas.pakal@gympik.com";
        $mail->AddAddress($email);
        var_dump($mail->Send());
        echo "<pre>";
        print_r($mail->ErrorInfo);
        exit;
        echo "<pre>";
        print_r($mail);
        exit;
    }

    function import($namespace) {
        static $registered = false;
        static $paths = [];
        static $classMap = [];

        if (!$registered) {
            spl_autoload_register(function($class) use(&$paths, &$classMap) {
                if (empty($paths) && empty($classMap)) {
                    return;
                }
                if (strpos($class, '\\') === false) {
                    if (isset($classMap[$class])) {
                        return class_alias($classMap[$class], $class);
                    } else {
                        $baseFile = '/' . str_replace('_', '/', $class) . '.php';
                        foreach ($paths as $namespace => $path) {
                            if (is_file($path . $baseFile)) {
                                return class_alias($namespace . '\\' . $class, $class);
                            }
                        }
                    }
                }
            });
            $registered = true;
        }
        if (($pos = strrpos($namespace, '\\')) !== false) {
            $ns = trim(substr($namespace, 0, $pos), '\\');
            $alias = substr($namespace, $pos + 1);
            if ($alias === '*') {
                if (!isset($paths[$ns]) || $paths[$ns] === false) {
                    $paths[$ns] = Yii::getAlias('@' . str_replace('\\', '/', $ns), false);
                }
            } elseif (!empty($alias)) {
                $classMap[$alias] = trim($namespace, '\\');
            }
        } else {
            throw new yiibaseInvalidParamException("Invalid import alias: $namespace");
        }
    }

}
