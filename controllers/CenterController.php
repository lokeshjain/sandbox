<?php

namespace app\controllers;

use Yii;
use app\models\Center;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CenterController implements the CRUD actions for Center model.
 */
class CenterController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'index', 'upload-users', 'get-subroles', 'download-sample'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete', 'index', 'upload-users', 'get-subroles', 'download-sample'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Center models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new \app\models\CenterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Center model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Center model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Center();
        $model_admin = new \app\models\CenterUser();
        if ($model->load(Yii::$app->request->post()) && $model_admin->load(Yii::$app->request->post())) {
            $model->added_by = Yii::$app->user->identity->id;
            $model->updated_by = Yii::$app->user->identity->id;
            $model->updated_on = date('Y-m-d H:i:s');
            $model->added_on = date('Y-m-d H:i:s');
            $model_admin->added_by = Yii::$app->user->identity->id;
            $model_admin->updated_by = Yii::$app->user->identity->id;
            $model_admin->updated_on = date('Y-m-d H:i:s');
            $model_admin->added_on = date('Y-m-d H:i:s');
            $model->validate();
            $model_admin->validate(["first_name", "last_name", "email", "mobile"]);
            if (!$model->hasErrors() && !$model_admin->hasErrors()) {
                if ($model->save(FALSE)) {
                    $model_admin->center_id = $model->id;
                    $model_admin->role = 7;
                    $pwd = '123456';
                    $model_admin->password = sha1($pwd);
                    $model_admin->status = $model->status;
                    if ($model_admin->save(FALSE)) {
                        $this->sendviaMandrill($model_admin->email, "TraQ Password", "Password : $pwd", $this->adminEmail, $this->adminName, "Admin User Created");
                        return $this->redirect(['index']);
                    }
                }
            }
        } //else {
        return $this->render('_form', [
                    'model' => $model,
                    'model_admin' => $model_admin
        ]);
        //}
    }

    /**
     * Updates an existing Center model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model_admin = \app\models\CenterUser::find()->where("center_id = :center_id AND role = :role", [":center_id" => $model->id, ":role" => 7])->one();
        if (!isset($model_admin->id)) {
            $model_admin = new \app\models\CenterUser();
        }
        if ($model->load(Yii::$app->request->post()) && $model_admin->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->identity->id;
            $model->updated_on = date('Y-m-d H:i:s');
            $model_admin->updated_by = Yii::$app->user->identity->id;
            $model_admin->updated_on = date('Y-m-d H:i:s');
            $model->validate();
            $model_admin->validate(["first_name", "last_name", "email", "mobile"]);
            if (!$model->hasErrors() && !$model_admin->hasErrors()) {
                if ($model->save(FALSE)) {
                    $isnew = $model_admin->isNewRecord;
                    $pwd = sha1('123456');
                    if ($isnew) {
                        $model_admin->center_id = $model->id;
                        $model_admin->role = 7;
                        $model_admin->password = $pwd;
                    }
                    $model_admin->status = $model->status;
                    if ($model_admin->save(FALSE)) {
                        if ($isnew)
                            $this->sendviaMandrill($model_admin->email, "TraQ Password", "Password : $pwd", $this->adminEmail, $this->adminName, "Admin User Created");
                        return $this->redirect(['index']);
                    }
                }
            }
        } //else {
        return $this->render('_form', [
                    'model' => $model,
                    'model_admin' => $model_admin
        ]);
        //}
    }

    /**
     * Deletes an existing Center model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Center model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Center the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Center::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUploadUsers($id) {

        $center_model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            /* $val_model = \yii\base\DynamicModel::validateData(compact('role', 'subrole', 'excel'));
              $val_model->addRule(["role"], 'required')
              ->validate(); */
            $data = Yii::$app->request->post();
            if (isset($data['Center'])) {
                $data = $data['Center'];
            }
            if (!isset($data["role"]) || $data["role"] == '') {
                $center_model->addError('role', "Role Is Required");
            } else {
                $center_model->role = $data["role"];
            }
            $excel = \yii\web\UploadedFile::getInstance($center_model, 'excel');

            if (is_null($excel)) {
                $center_model->addError('excel', "Excel File Is Required");
            } else {
                if ($excel->error != 0) {
                    $center_model->addError('excel', "Error in upload");
                }
                if (!isset($excel->extension) || !in_array(strtolower($excel->extension), ["xlx", "xlsx"])) {
                    $center_model->addError('excel', "Invalid files , xlx,xlsx");
                }
            }
            if (!$center_model->hasErrors()) {
                $name = $center_model->id . "_" . time() . "_" . $excel->baseName . "." . $excel->extension;
                $path = "excel_upload/$name";
                $excel->saveAs($path);
                require_once \Yii::getAlias('@app') . '/common/libraries/PHPExcel/Classes/PHPExcel.php';
                $objPHPExcel = \PHPExcel_IOFactory::load($path);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $columns = [];
                $allColumns = [];
                if (isset($sheetData[1])) {
                    $columns = $sheetData[1];
                    unset($sheetData[1]);
                }
                if (count($sheetData)) {
                    $tobeinsert = count($sheetData);
//                    $sql = \Yii::$app->db->createCommand()->batchInsert(\app\models\CenterUser::tableName(), $columns, $sheetData)->getRawSql();
//                    $inserted = \Yii::$app->db->createCommand()->batchInsert(\app\models\CenterUser::tableName(), $columns, $sheetData)->execute();
                    $errors = '';
                    $filtered_data = [];
                    $batch_tag = $id . "_" . time();
                    $excelStr = "";
                    $emails = $mobiles = [];
                    foreach ($sheetData as $row) {
                        $rdata_copy = $row;
                        $rdata = array_combine($columns, $row);

                        $rdata['center_id'] = $id;
                        $rdata['batch_tag'] = $batch_tag;
                        $rdata['role'] = isset($data['role']) ? $data['role'] : 0;
                        $rdata['sub_role'] = isset($data['subrole']) ? $data['subrole'] : 0;
                        $rdata['status'] = 1;
                        $rdata['added_on'] = $rdata['updated_on'] = date('Y-m-d H:i:s');
                        $rdata['added_by'] = $rdata['updated_by'] = isset(\Yii::$app->user->identity->id) ? \Yii::$app->user->identity->id : 0;
                        $pwd = '123456';
                        $rdata['password'] = sha1($pwd);
                        /*
                         * $user_model = new \app\models\CenterUser();
                         * $user_model->attributes = $rdata;
                          $user_model->mobile = (string) $user_model->mobile;
                          $pwd = '123456';
                          $user_model->password = sha1($pwd);
                          if($user_model->save()){
                          $filtered_data[] = $user_model->attributes;
                          }else{
                          $errors.= implode("\t", $rdata);
                          $errors.="\t".$this->arrayToString($user_model->getErrors());
                          $errors.="\n";
                          } */
                        if (count($allColumns) == 0)
                            $allColumns = array_keys($rdata);
                        $noerr = TRUE;
                        $cust_err = "";
                        if (trim($rdata['first_name']) == '') {
                            $cust_err.= " Blank first name";
                            $noerr = FALSE;
                        }
                        if (trim($rdata['email']) == '') {
                            $cust_err.= " Blank Email";
                            $noerr = FALSE;
                        }
                        if (strlen(trim($rdata['mobile'])) != 10) {
                            $cust_err.= " Mobile 10digit required";
                            $noerr = FALSE;
                        }
                        if (in_array($rdata['email'], $emails)) {
                            $cust_err.= " Dulicate Email in sheet";
                            $noerr = FALSE;
                        }
                        if (in_array($rdata['mobile'], $mobiles)) {
                            $cust_err.= " Dulicate mobile in sheet";
                            $noerr = FALSE;
                        }
                        if ($noerr) {
                            $filtered_data[] = $rdata;
                            $emails[] = $rdata['email'];
                            $mobiles[] = $rdata['mobile'];
                        } else {
//                            $excelStr.= implode("\t", $rdata)."\t$cust_err\n";
                            $excelStr.= implode("\t", $rdata_copy) . "\t$cust_err\n";
                        }
                    }
                    if (count($filtered_data)) {
                        $temp_tbl = \app\models\CenterUser::tempTableName();
                        $user_tbl = \app\models\CenterUser::tableName();
                        $transaction = \Yii::$app->db->beginTransaction();
                        try {

                            $inserted = \Yii::$app->db->createCommand()->batchInsert($temp_tbl, $allColumns, $filtered_data)->execute();
                            /*
                              SELECT temp.* FROM
                              (
                              SELECT tmp.first_name,tmp.last_name,tmp.email,tmp.mobile,tmp.password,tmp.role,tmp.sub_role,tmp.center_id,u.id general_trainer_id, p.id personal_trainer_id, n.id nutritionist_id
                              FROM `sb__center_user_temp` tmp
                              LEFT JOIN sb__center_user u ON u.email = tmp.general_trainer AND u.center_id = 1
                              LEFT JOIN sb__center_user p ON p.email = tmp.personal_trainer AND p.center_id = 1
                              LEFT JOIN sb__center_user n ON n.email = tmp.nutritionist AND n.center_id = 1
                              WHERE tmp.batch_tag = '1_1433766854'
                              ) temp
                              LEFT JOIN sb__center_user user ON user.email = temp.email OR user.mobile = temp.mobile
                              WHERE user.id IS NULL
                             */
                            if ($inserted) {
                                /*
                                  tmp.first_name,tmp.last_name,tmp.email,tmp.mobile,tmp.password,tmp.role,tmp.sub_role,tmp.center_id,u.id general_trainer_id, p.id personal_trainer_id, n.id nutritionist_id,tmp.added_on,tmp.updated_on,tmp.added_by,tmp.updated_by,tmp.in_que,tmp.diet_in_queue,tmp.status
                                 */
                                $slt = "tmp." . implode(",tmp.", $columns);
                                $failed_data = "
                                SELECT temp.* FROM
                                (
                                SELECT $slt
                                FROM $temp_tbl tmp
                                LEFT JOIN $user_tbl u ON u.email = tmp.general_trainer AND u.center_id = $id
                                LEFT JOIN $user_tbl p ON p.email = tmp.personal_trainer AND p.center_id = $id
                                LEFT JOIN $user_tbl n ON n.email = tmp.nutritionist AND n.center_id = $id
                                WHERE tmp.batch_tag = '$batch_tag'
                                ) temp
                                LEFT JOIN sb__center_user user ON user.email = temp.email OR user.mobile = temp.mobile
                                WHERE user.id IS NOT NULL";
//                            echo "<pre>";
//                                print_r($failed_data);
//                                exit;    
                                $failed_data = \Yii::$app->db->createCommand($failed_data)->queryAll();

                                $insert_sql = "
                            INSERT INTO $user_tbl (first_name,last_name,email,mobile,password,role,sub_role,center_id,general_trainer_id,personal_trainer_id,nutritionist_id,added_on,updated_on,added_by,updated_by,in_que,diet_in_queue,status)
                            SELECT temp.* FROM
                            (
                            SELECT tmp.first_name,tmp.last_name,tmp.email,tmp.mobile,tmp.password,tmp.role,tmp.sub_role,tmp.center_id,u.id general_trainer_id, p.id personal_trainer_id, n.id nutritionist_id,tmp.added_on,tmp.updated_on,tmp.added_by,tmp.updated_by,tmp.in_que,tmp.diet_in_queue,tmp.status
                            FROM $temp_tbl tmp
                            LEFT JOIN $user_tbl u ON u.email = tmp.general_trainer AND u.center_id = $id
                            LEFT JOIN $user_tbl p ON p.email = tmp.personal_trainer AND p.center_id = $id
                            LEFT JOIN $user_tbl n ON n.email = tmp.nutritionist AND n.center_id = $id
                            WHERE tmp.batch_tag = '$batch_tag'
                            ) temp
                            LEFT JOIN sb__center_user user ON user.email = temp.email OR user.mobile = temp.mobile
                            WHERE user.id IS NULL";
                                $filtered_inserted = \Yii::$app->db->createCommand($insert_sql)->execute();
                                /* $delete_batch = "DELETE FROM $temp_tbl WHERE batch_tag = '$batch_tag'";
                                  \Yii::$app->db->createCommand($delete_batch)->execute(); */
                                if (count($failed_data)) {
                                    $header = [];
                                    foreach ($failed_data as $frow) {
                                        if (!count($header))
                                            $header = array_keys($frow);
                                        $excelStr .= implode("\t", $frow) . "\tEmail or mobile Already exist\n";
                                    }
                                }
                            }
                            $transaction->commit();
                        } catch (Exception $exc) {
                            $transaction->rollBack();
                            echo $exc->getTraceAsString();
                        }
                    }
                    if ($excelStr != '') {
                        $filename = $center_model->center_name . "_errors";
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-disposition: csv" . date("Y-m-d") . ".xlx");
                        header("Content-disposition: filename=" . $filename . ".xlx");
                        echo implode("\t", $columns);
//                        echo "\n";
                        echo "\tErrors\n";
                        print $excelStr;
                        exit;
                    }
                }
            }
        }
        return $this->render("users-upload", [
                    "model" => $center_model,
//                "val_model" => $val_model,
//                "model" => $model
        ]);
    }

    public function actionGetSubroles($center_id) {
        $role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : 0;
        $subroles = \app\models\CenterSubRole::find()
                ->select(["id", "role_name"])
                ->where("center_id = :center_id AND role_id = :role_id", ["center_id" => $center_id, "role_id" => $role_id])
                ->all();
        $subroles = \yii\helpers\ArrayHelper::map($subroles, 'id', 'role_name');
        echo "<option value=''>Select Subrole</option>";
        foreach ($subroles as $id => $txt) {
            echo "<option value='$id'>$txt</option>";
        }
    }

    public function arrayToString($arr) {
        $err = "";
        foreach ($arr as $erra) {
            foreach ($erra as $fld => $e)
                $err.="$e ";
        }
        return trim($err, ',');
    }

    public function actionDownloadSample($file = "members") {
        $ctype = "Content-type:application/force-download";
        $dfile = "excel_upload/samples/$file.xlsx";
        header($ctype);
        header("Content-Disposition:attachment;filename=$file.xlsx");
        readfile($dfile);
    }

}
