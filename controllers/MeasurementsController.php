<?php

namespace api\modules\v1\controllers;

use api\components\RestController;
use yii\web\Response;
use yii\filters\auth\HttpBasicAuth;
use Yii;
use common\models\UserMeasurements;
use yii\db\Query;


/*sdasd*/
//error_reporting(0);
class MeasurementsController extends RestController
{/*
     public function behaviors() {
        $no_auth_actions = array("getRoles1");
        $behaviors = parent::behaviors();
        return $behaviors;
        if(in_array($this->action->id, $no_auth_actions))
            return $behaviors;
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className()
        ];
        return $behaviors;
    }*/

    public function actions() {
        $actions_c = [
            'addEditMeasurement' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionAddEditMeasurement']
            ],
            'fitnessComponents' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'actionFitnessComponents']
            ],
        ];
        $actions = parent::actions();
        return array_merge($actions, $actions_c);
        return $actions_c;
    }
    
    public function actionGetUserMeasurements()
    {
       $details_result    = array();
       $status;
       $message;
       $user_id           =  !empty($_REQUEST['user_id'])?$_REQUEST['user_id']:''; 
       
       if(!empty($user_id))
       {
           $measurement_model  = UserMeasurements::find('id,due_date,added_on,added_by')
                               ->where('user_id = :user_id', ['user_id'=>$user_id])
                               ->orderBy('added_on desc')
                               ->all();
           
           if(isset($measurement_model))
           {
                foreach($measurement_model as $val)
                {
                    $deatil['measurement_id']    =    $val['id'];
                    $deatil['due_date']          =    $val['due_date'];
                    $deatil['added_on']          =    $val['added_on'];

                    $user_model                  =   CenterUser::find('first_name,last_name')
                                                     ->where('id = :id', ['id'=>$user_id])
                                                     ->one();

                    $deatil['added_by']          =   $user_model['first_name'].' '.$user_model['last_name']; 

                    array_push($details_result, $deatil);

                }
                
                $status   = 1;
                $message  = "Measurement fetched sucessfully!";
           }
           else{
                 $status  = 0;
                 $message = "There are no measurements stored ";
               
           }
       }
       else{
           
             $status   = 0;
             $message  = "Please provide user id";
           
       }
       
       $result   =  array("status"=>$status,"message"=>$message,"result"=>$details_result);
       return $result;
       
    }
    
    public function actionAddEditMeasurement()
    {
        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());
        
        // get today year and today week number
        $today_date_array   = date_parse($current_timestamp);
        $today_year         = $today_date_array['year'];
               
        $today_week_number = date("W", strtotime($current_timestamp));
       
        $status;
        $message;
        
        $measurement['weight']             =  isset($_REQUEST['weight'])?$_REQUEST['weight']:0;
        $measurement['height']             =  isset($_REQUEST['height'])?$_REQUEST['height']:0;
        $measurement['neck']               =  isset($_REQUEST['neck'])?$_REQUEST['neck']:0;
        $measurement['shoulder']           =  isset($_REQUEST['shoulder'])?$_REQUEST['shoulder']:0;
        $measurement['chest_in']           =  isset($_REQUEST['chest_in'])?$_REQUEST['chest_in']:0;
        $measurement['chest_exp']          =  isset($_REQUEST['chest_exp'])?$_REQUEST['chest_exp']:0;
        $measurement['u_abd']              =  isset($_REQUEST['u_abd'])?$_REQUEST['u_abd']:0;
        $measurement['l_abd']              =  isset($_REQUEST['l_abd'])?$_REQUEST['l_abd']:0;
        $measurement['waist']              =  isset($_REQUEST['waist'])?$_REQUEST['waist']:0;
        $measurement['hips']               =  isset($_REQUEST['hips'])?$_REQUEST['hips']:0;
        $measurement['u_thighs']           =  isset($_REQUEST['u_thighs'])?$_REQUEST['u_thighs']:0;
        $measurement['l_thighs']           =  isset($_REQUEST['l_thighs'])?$_REQUEST['l_thighs']:0;
        $measurement['calves']             =  isset($_REQUEST['calves'])?$_REQUEST['calves']:0;
        $measurement['right_u_arm']        =  isset($_REQUEST['right_u_arm'])?$_REQUEST['right_u_arm']:0;
        $measurement['right_l_arm']        =  isset($_REQUEST['right_l_arm'])?$_REQUEST['right_l_arm']:0;
        $measurement['left_u_arm']         =  isset($_REQUEST['left_u_arm'])?$_REQUEST['left_u_arm']:0;
        $measurement['left_l_arm']         =  isset($_REQUEST['left_l_arm'])?$_REQUEST['left_l_arm']:0;
        $measurement['wrist']              =  isset($_REQUEST['wrist'])?$_REQUEST['wrist']:0;
        $measurement['rhr']                =  isset($_REQUEST['rhr'])?$_REQUEST['rhr']:0;
        $measurement['notes']              =  isset($_REQUEST['notes'])?$_REQUEST['notes']:'';
        $measurement['user_id']            =  isset($_REQUEST['user_id'])?$_REQUEST['user_id']:'';
        $measurement['logged_id']          =  isset($_REQUEST['logged_id'])?$_REQUEST['logged_id']:'';
        $measurement['due_date']           =  isset($_REQUEST['due_date'])?$_REQUEST['due_date']:'';
        $measurement['status']             =  1;
        $measurement['action']             =  isset($_REQUEST['action'])?$_REQUEST['action']:'';
        $measurement['measurement_id']     =  isset($_REQUEST['measurement_id'])?$_REQUEST['measurement_id']:'';
              
        if(!empty($measurement['action']) && ($measurement['action']=='add' or $measurement['action']=='edit'))        
        {
        if(!empty($measurement['user_id']) && !empty($measurement['logged_id']))
        {
            if($measurement['action']=="add")
            {
               $last_mesurements  = UserMeasurements::find()->where('user_id = :user_id', ['user_id'=>$measurement['user_id']])->orderBy('added_on desc')->one();
            }
            else if($measurement['action']=="edit"){
                
                $last_mesurements  = UserMeasurements::findOne($measurement['measurement_id']);
            }
            if(!is_null($last_mesurements))
            {
                 // get last added year and last added week number
                $last_added_date_array   = date_parse($last_mesurements->added_on);
                $last_added_year         = $last_added_date_array['year'];
               
                $last_added_week_number = date("W", strtotime($last_mesurements->added_on));
                
                if($today_year==$last_added_year && $today_week_number==$last_added_week_number && $measurement['action']=="add")
                {
                    $status   = 0;
                    $message  = "Measurements already have entered in this week for this user.";
                }
                else if(($today_year!=$last_added_year or $today_week_number!=$last_added_week_number) && $measurement['action']=="edit")
                {
                    $status   = 0;
                    $message  = "You can update this week measurements only!";
                }
                else{
                    
                    $add_measurements = $this->addEditMeasurements($measurement);
                    $status           = $add_measurements['status'];
                    $message          = $add_measurements['message'];
                    
                }
            }
            
            else{
                   $add_measurements = $this->addEditMeasurements($measurement);
                   $status           = $add_measurements['status'];
                   $message          = $add_measurements['message'];
            }
        }
        else{
                 $status     = 4;
                 $message    = "Please provide all required fields"; 
        }
        }
        else{
             $status     = 5;
             $message    = "Please provide valid action";
            
        }
        
        $result  = array("status"=>$status,"message"=>$message);
        return $result;
     
    }
    
    public function addEditMeasurements($measurement=NULL)
    {
        if(!is_null($measurement))
        {
                date_default_timezone_set('Asia/Kolkata');
                $current_timestamp = date('Y-m-d h:i:s', time());
                if($measurement['action']=="add")
                {
                   $model   = new UserMeasurements();
                }
                else if($measurement['action']=="edit")
                {
                    
                    $model  = UserMeasurements::findOne($measurement['measurement_id']);
                }

                               
            if(isset($model))
            {    
                // assign all parameter 
                $model->weight              = $measurement['weight'];
                $model->height              = $measurement['height'];
                $model->neck                = $measurement['neck'];
                $model->shoulder            = $measurement['shoulder'];
                $model->chest_in            = $measurement['chest_in'];
                $model->chest_exp           = $measurement['chest_exp'];
                $model->u_abd               = $measurement['u_abd'];
                $model->l_abd               = $measurement['l_abd'];
                $model->waist               = $measurement['waist'];
                $model->hips                = $measurement['hips'];
                $model->u_thighs            = $measurement['u_thighs'];
                $model->l_thighs            = $measurement['l_thighs'];
                $model->calves              = $measurement['calves'];
                $model->right_u_arm         = $measurement['right_u_arm'];
                $model->right_l_arm         = $measurement['right_l_arm'];
                $model->left_u_arm          = $measurement['left_u_arm'];
                $model->left_l_arm          = $measurement['left_l_arm'];
                $model->wrist               = $measurement['wrist'];
                $model->rhr                 = $measurement['rhr'];
                $model->user_id             = $measurement['user_id'];
                if($measurement['action']=="add")
                {
                    $model->added_on            = $current_timestamp;
                    $model->added_by            = $measurement['logged_id'];
                    $model->updated_on          = $current_timestamp;
                    $model->updated_by          = $measurement['logged_id'];
                }
                else if($measurement['action']=="edit")
                {
                    $model->updated_on          = $current_timestamp;
                    $model->updated_by          = $measurement['logged_id'];
                }
                $model->notes               = $measurement['notes'];
                $model->status              = $measurement['status'];

                if($model->save())
                {
                    $status   = 1;
                    $message  = ($measurement['action']=="add")?"user measurements save sucessfully!":"user measurements updated sucessfully!";
                }
                else{

                    $status  = 2;
                    $message = "Problem in saving model!";

                }
            }
            else{
                
                $status  = 3;
                $message = "Please provide valid measurement id!";
            }
        }
        else{
            $status     = 5;
            $message    = "Please provide all required fields";
        }
        
        $result = array("status"=>$status,"message"=>$message);
        return($result);
    }
    
    
    public function actionFitnessComponents()
    {
        echo "hello";exit;
        $status;
        $message;
        
        $component   =  $_REQUEST['components'];
        
        if($component =='strength')
        {
            $bench_press1   = isset($_REQUEST['bench_press1'])?$_REQUEST['bench_press1']:'';
            $bench_press2   = isset($_REQUEST['bench_press2'])?$_REQUEST['bench_press2']:'';
            $bench_press3   = isset($_REQUEST['bench_press3'])?$_REQUEST['bench_press3']:'';
            $egpress1       = isset($_REQUEST['egpress1'])?$_REQUEST['egpress1']:'';
            $egpress2       = isset($_REQUEST['egpress2'])?$_REQUEST['egpress2']:'';
            $egpress3       = isset($_REQUEST['egpress3'])?$_REQUEST['egpress3']:'';
            
            $model          =  new MemberStrength();
            $model->bench_press1  = $bench_press1;
            $model->bench_press2  = $bench_press2;
            $model->bench_press3  = $bench_press3;
            $model->egpress1      = $egpress1;
            $model->egpress2      = $egpress2;
            $model->egpress3      = $egpress3;
     
        }
        else if($component =='endurance')
        {
            $knee_sit_repetition  = isset($_REQUEST['knee_sit_repetition'])?$_REQUEST['knee_sit_repetition']:'';
            $knee_sit_time        = isset($_REQUEST['knee_sit_time'])?$_REQUEST['knee_sit_time']:'';
            $push_up_repetition   = isset($_REQUEST['push_up_repetition'])?$_REQUEST['push_up_repetition']:'';
            $push_up_time         = isset($_REQUEST['push_up_time'])?$_REQUEST['push_up_time']:'';
            $core                 = isset($_REQUEST['core'])?$_REQUEST['core']:'';
            $step_test1           = isset($_REQUEST['step_test1'])?$_REQUEST['step_test1']:'';
            $step_test2           = isset($_REQUEST['step_test2'])?$_REQUEST['step_test2']:'';
            $step_test3           = isset($_REQUEST['step_test3'])?$_REQUEST['step_test3']:'';
            $step_test4           = isset($_REQUEST['step_test4'])?$_REQUEST['step_test4']:'';
            
            $model    = new MemberEndurance();
            $model->knee_sit_repetition  = $knee_sit_repetition;
            $model->knee_sit_time        = $knee_sit_time;
            $model->push_up_repetition   = $push_up_repetition;
            $model->push_up_time         = $push_up_time;
            $model->core                 = $core;
            $model->step_test1           = $step_test1;
            $model->step_test2           = $step_test2;
            $model->step_test3           = $step_test3;
            $model->step_test4           = $step_test4;
            
        }
        else if($component =='flexibility')
        {
            $sit_reach         =  isset($_REQUEST['sit_reach'])?$_REQUEST['sit_reach']:'';
            $shoulder_left     =  isset($_REQUEST['shoulder_left'])?$_REQUEST['shoulder_left']:'';
            $shoulder_right    =  isset($_REQUEST['shoulder_right'])?$_REQUEST['shoulder_right']:'';
            $left_leg_standing =  isset($_REQUEST['left_leg_standing'])?$_REQUEST['left_leg_standing']:'';
            $right_leg_standing=  isset($_REQUEST['right_leg_standing'])?$_REQUEST['right_leg_standing']:'';
            
            $model   = new MemberFlexibility();
            $model->sit_reach     = $sit_reach;
            $model->shoulder_left = $shoulder_left;
            $model->shoulder_right = $shoulder_right;
            $model->left_leg_standing = $left_leg_standing;
            $model->right_leg_standing = $right_leg_standing;
            
        }
        else if($component =='fat')
        {
            
            $calculation_type =  isset($_REQUEST['calculation_type'])?$_REQUEST['calculation_type']:'';
            $chest            =  isset($_REQUEST['chest'])?$_REQUEST['chest']:'';
            $abs              =  isset($_REQUEST['abs'])?$_REQUEST['abs']:'';
            $thigh            =  isset($_REQUEST['thigh'])?$_REQUEST['thigh']:'';
            $triceps          =  isset($_REQUEST['triceps'])?$_REQUEST['triceps']:'';
            $subscapular      =  isset($_REQUEST['subscapular'])?$_REQUEST['subscapular']:'';
            $suprailliac      =  isset($_REQUEST['suprailliac'])?$_REQUEST['suprailliac']:'';
            $mid_axillary     =  isset($_REQUEST['mid_axillary'])?$_REQUEST['mid_axillary']:'';
            $fat_percentage   =  isset($_REQUEST['fat_percentage'])?$_REQUEST['fat_percentage']:'';
            
            $model   = new MemberFat();
            $model->calculation_type = $calculation_type;
            $model->chest = $chest;
            $model->abs   = $abs;
            $model->thigh = $thigh;
            $model->triceps = $triceps;
            $model->subscapular = $subscapular;
            $model->suprailliac = $suprailliac;
            $model->mid_axillary = $mid_axillary;
            $model->fat_percentage = $fat_percentage;
        }
        
        if($model->save())
        {
            $status = 1;
            switch($component)
            {
                case 'strength' :  $message = 'Strength saved sucessfully!';
                                   break;
                               
                case 'endurance':  $message = 'Endurance saved sucessfully!';
                                   break;
                
                case 'flexibility' : $message = 'Flexibility Saved sucessfully!';
                                     break;
                
                case 'fat'  : $message = 'Fat attribute saved sucessfully!';
                               break;
                
            }
        }
        else{
            $status = -1;
            $message = 'Problem in saving model.';
        }
        
        $result  = array('status'=>$staus,'message'=>$message);
        return($result);
    }
    

}