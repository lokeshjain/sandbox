<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
//use yii\web\Controller;
use app\components\Controller;
use yii\filters\VerbFilter;
use app\models\CenterUser;
use app\models\Role;
use yii\db\Query;
/*sdasd*/
//error_reporting(0);
class UserController extends Controller
{
    public function actionIndex()
    {
       
    }
    
    public function actionAddEditUser(){
        $status;
        $message;
        $result              =  array();
       
        date_default_timezone_set('Asia/Kolkata');
        $current_timestamp = date('Y-m-d h:i:s', time());
        
        // get all the parameter
        $firstname           =  isset($_GET['firstname'])?$_GET['firstname']:'';
        $lastname            =  isset($_GET['lastname'])?$_GET['lastname']:'';      
        $email               =  isset($_GET['email'])?$_GET['email']:'';
        $mobile              =  isset($_GET['mobile'])?$_GET['mobile']:'';
        $password            =  isset($_GET['password'])?sha1($_GET['password']):'';
        $role                =  isset($_GET['role'])?$_GET['role']:'';
        $center_id           =  isset($_GET['center_id'])?$_GET['center_id']:'';
        $general_trainer_id  =  isset($_GET['general_trainer_id'])?$_GET['general_trainer_id']:NULL;
        $personal_trainer_id =  isset($_GET['personal_trainer_id'])?$_GET['personal_trainer_id']:NULL;
        $nutritionist_id     =  isset($_GET['nutritionist_id'])?$_GET['nutritionist_id']:NULL; 
        $logged_in_id        =  isset($_GET['session_id'])?$_GET['session_id']:NULL;
        $action              =  isset($_GET['action'])?$_GET['action']:NULL;
        $user_id             =  isset($_GET['user_id'])?$_GET['user_id']:NULL;
        $user_status         =  1;
        
        if($action=='add')
        {
            // check if email or mobile no is already registered
            $userExist   = CenterUser::find()->where('email = :email or mobile = :mobile', ['email'=>$email, 'mobile'=>$mobile])->one();
           // echo $firstname.'--'.$email.'--'.$mobile.'--'.$role.'--'.$password.'--'.$center_id;exit;
            if(is_null($userExist))
            {
            // check if any of required fields empty
            if(!empty($firstname) && !empty($email) && !empty($mobile) && !empty($role)) 
            {
            $model   = new CenterUser();   // create model of center user table

            // assign all parameter 
            $model->firstname          = $firstname;
            $model->lastname           = $lastname;
            $model->email              = $email;
            $model->mobile             = $mobile;
            $model->role               = $role;
            $model->password           = $password;
            $model->center_id          = $center_id;
            $model->general_trainer_id = $general_trainer_id;
            $model->personal_trainer_id= $personal_trainer_id;
            $model->nutritionist_id    = $nutritionist_id;
            $model->added_on           = $current_timestamp;
            $model->updated_on         = $current_timestamp;
            $model->added_by           = $logged_in_id;
            $model->updated_by         = $logged_in_id;
            $model->status             = $user_status;

            $transaction = Yii::$app->db->beginTransaction();   // begin the transaction

            try {
                //check model is saved or not
                if($model->save()){

                 $transaction->commit();   // commit transaction and data sucessfully saved
                 $status     = 1;
                 $message    = "user sucessfully saved";

                }else{

                 $status     = 2;
                 $message    = "problem in saving model";    
              }

            }catch (Exception $e) {

                $transaction->rollBack();
                $status     = 3;
                $message    = "there is some exception"; 

           }
            }else{
                 $status     = 4;
                 $message    = "Please fill all required fields";  
            }
            }else{
                $status     = 5;
                $message    = "email or mobile no already exits";  
            }  
        }
        else if($action=='edit')
        {
            $emailExist  = count(CenterUser::find()->where('email = :email', ['email'=>$email])->one()); 
            $mobileExist = count(CenterUser::find()->where('mobile = :mobile', ['mobile'=>$mobile])->one());

            $user_model = CenterUser::findOne($user_id);
            
            if(!is_null($user_model))
            {
            if(!empty($firstname) && !empty($email) && !empty($mobile) && !empty($role)) 
            {
                if(($mobileExist==0 or $user_model->mobile==$mobile) && ($emailExist==0 or $user_model->email==$email))
                {
                    $user_model->firstname          = $firstname;
                    $user_model->lastname           = $lastname;
                    $user_model->email              = $email;
                    $user_model->mobile             = $mobile;
                    $user_model->role               = $role;
                    $user_model->general_trainer_id = $general_trainer_id;
                    $user_model->personal_trainer_id= $personal_trainer_id;
                    $user_model->nutritionist_id    = $nutritionist_id;
                    $user_model->updated_on         = $current_timestamp;
                    $user_model->updated_by         = $logged_in_id;
                    if($user_model->save())
                    {
                        $status  = 1;
                        $message = "User information updated sucessfully!";                  
                    }else{
                        $status  = 2;
                        $message = "problem in saving model";
                    }
                }
                else if($mobileExist>0 && $user_model->mobile!=$mobile && $emailExist>0 && $user_model->email!=$email)
                {
                    $status  = 3;
                    $message = "Both mobile and email are registered with someone else";
                }
                else if($mobileExist>0 && $user_model->mobile!=$mobile)
                {
                    $status  = 3;
                    $message = "This mobile number is registered with someone else";
                }
                else if($emailExist>0 && $user_model->email!=$email)
                {
                    $status  = 3;
                    $message = "This email is registered with someone else";
                }
            }
            else{
                 $status     = 4;
                 $message    = "Please fill all required fields";
            }
        }
        else{
            $status     = 4;
            $message    = "This user doesn't exist. please add this user.";
        }
        }
        else{
              $status     = 0;
              $message    = "Please provide valid action";
        }
      $result = array("status"=>$status,"message"=>$message);  
      echo json_encode($result);
      exit;
    }
    
    // edit user 
    public function actionUserUpdate()
    {
        $status;
        $message;
        $result              =  array();
       
        date_default_timezone_set('Asia/Kolkata');
        $updated_on = date('Y-m-d h:i:s', time());
        
        // get all the parameter
        $firstname           =  isset($_GET['firstname'])?$_GET['firstname']:'';
        $lastname            =  isset($_GET['lastname'])?$_GET['lastname']:'';      
        $email               =  isset($_GET['email'])?$_GET['email']:'';
        $mobile              =  isset($_GET['mobile'])?$_GET['mobile']:'';
        $role                =  isset($_GET['role'])?$_GET['role']:'';
        $general_trainer_id  =  isset($_GET['general_trainer_id'])?$_GET['general_trainer_id']:NULL;
        $personal_trainer_id =  isset($_GET['personal_trainer_id'])?$_GET['personal_trainer_id']:NULL;
        $nutritionist_id     =  isset($_GET['nutritionist_id'])?$_GET['nutritionist_id']:NULL; 
        $updated_by          =  isset($_GET['session_id'])?$_GET['session_id']:NULL;
        $user_id             =  isset($_GET['user_id'])?$_GET['user_id']:NULL;
        $user_status         =  1; 
          
        $emailExist  = count(CenterUser::find()->where('email = :email', ['email'=>$email])->one()); 
        $mobileExist = count(CenterUser::find()->where('mobile = :mobile', ['mobile'=>$mobile])->one());
        
        $user_model = CenterUser::findOne($user_id);

        if(!empty($firstname) && !empty($email) && !empty($mobile) && !empty($role)) 
        {
            if(($mobileExist==0 or $user_model->mobile==$mobile) && ($emailExist==0 or $user_model->email==$email))
            {
                $user_model->firstname          = $firstname;
                $user_model->lastname           = $lastname;
                $user_model->email              = $email;
                $user_model->mobile             = $mobile;
                $user_model->role               = $role;
                $user_model->general_trainer_id = $general_trainer_id;
                $user_model->personal_trainer_id= $personal_trainer_id;
                $user_model->nutritionist_id    = $nutritionist_id;
                $user_model->updated_on         = $added_on;
                $user_model->updated_by         = $updated_by;
                if($user_model->save())
                {
                    $status  = 1;
                    $message = "User information updated sucessfully!";                  
                }else{
                    $status  = 2;
                    $message = "problem in saving model";
                }
            }
            else if($mobileExist>0 && $user_model->mobile!=$mobile && $emailExist>0 && $user_model->email!=$email)
            {
                $status  = 3;
                $message = "Both mobile and email are registered with someone else";
            }
            else if($mobileExist>0 && $user_model->mobile!=$mobile)
            {
                $status  = 3;
                $message = "This mobile number is registered with someone else";
            }
            else if($emailExist>0 && $user_model->email!=$email)
            {
                $status  = 3;
                $message = "This email is registered with someone else";
            }
        }
        else{
             $status     = 4;
             $message    = "Please fill all required fields";
        }
        $result = array("status"=>$status,"message"=>$message);  
        echo json_encode($result);
        exit;
    }
    
    // get user roles
    public function actionGetRoles()
    {
        $roles_result = array();
        $role         = array();
        $status;
        $message;
        
        try{
                $model = Role::find()->all();
                if(isset($model))
                {
                foreach($model as $val)
                {
                    $role   = array();
                    if($val->status==1)
                    {
                      $role['id']  = $val->id;
                      $role['name']= $val->role;
                    }

                    array_push($roles_result,$role);
                }

                $status  = 1;
                $message = "User Roles fetched sucessfully!";

                }
                else{
                        $status  = 0;
                        $message = "No roles defined!";
                }
        } catch (Exception $ex) {
            
               $status  = 0;
               $message = "Exception Occurs!";
        }
        
        $result  = array("status"=>$status,"message"=>$message,"result"=>$roles_result);
        echo json_encode($result);
    }
    
    public function actionGetTrainer()
    {
        $status;
        $message;
        $details_result = array();
        $result = array();
        $role       =   (isset($_GET['trainer_type']))?$_GET['trainer_type']:'';
        $center_id  =   (isset($_GET['center_id']))?$_GET['center_id']:'';
        if(!empty($role) && !empty($center_id))
        {
            $role_model = Role::find()->where('role = :role', ['role'=>$role])->one();
            if(!is_null($role_model->id))
            {
            $role_id = $role_model->id;
            $query = new Query();
            $query->select('id,firstname,lastname')
                  ->from('sb__center_user')
                  ->where(['role' =>$role_id]);
            
            $command = $query->createCommand();
            $data    = $command->queryAll();
            
            if(!empty($data))
            {
                    foreach($data as $val)
                    {
                        $user['id']        = $val['id'];
                        $user['firstname'] = $val['firstname'];
                        $user['lastname']  = $val['lastname'];

                        array_push($details_result,$user);
                    }
                    $status  = 1;
                    $message = "Trainer List fetched succefully"; 
                    }
            else{
                $status  = 0;
                $message = "No trainer found in this center"; 
            }
        }
        else{
             $status  = 0;
             $message = "This type of trainers not available!";
        }
        }
        else{
            $status  = 0;
            $message = "Please provide trainer type and center!";        
        }
        
        $result  = array('status'=>$status,'message'=>$message,'result'=>$details_result);
        echo json_encode($result);
    }

    // user login
    public function actionUserLogin()
    {
        $status;
        $message;
        $result        = array();
        $username      = isset($_GET['username'])?$_GET['username']:'';
        $password      = isset($_GET['password'])?sha1($_GET['password']):'';
        
        if(!empty($username) && !empty($password))
        {            
                   $model = CenterUser::find()
                            ->where(['email'=>$username])
                            ->orWhere(['mobile'=>$username])
                            ->andWhere(['password'=>$password])
                            ->one();

            if(isset($model->email) && !empty($model->email))
            {
              
            }else{
                
                   $status     = 5;
                   $api_status = 400;
                   $message    = "wrong username or password";
            }
                       
        }else{
            $status     = 5;
            $api_status = 400;
            $message    = "please enter all required fields";
        }
       
        $result = array("status"=>$status,"message"=>$message);  
        $this->_sendResponse($api_status, $result);
        exit;
        
    }

        // reset password using center admin
    public function actionResetPassword()
    {
      //  echo "error";exit;
   //   $oldpassword  =  isset($_GET['oldpassword'])?$_GET['oldpassword']:'';
        $newpassword  =  isset($_GET['newpassword'])?$_GET['newpassword']:'';
        $confirmpassword  =  isset($_GET['confirmpassword'])?$_GET['confirmpassword']:'';
        $user_id      =  isset($_GET['user_id'])?$_GET['user_id']:'';
        
        if(!empty($user_id) && !empty($newpassword))
        {
           if($confirmpassword==$newpassword) 
           {
              $transaction = Yii::$app->db->beginTransaction();   // begin the transaction

               try {
               $model = CenterUser::findOne($user_id); 
               if(!is_null($model))
               {
               $model->password  = sha1($newpassword);
               if($model->save())
               {
                   $transaction->commit();
                   $status     = 1;
                   $api_status = 200;
                   $message    = "password changed successfully";
                  
                   
               }else{
                   $status     = 2;
                   $api_status = 400;
                   $message    = "problem in saving model";
               }
               }else{
                   $status     = 2;
                   $api_status = 400;
                   $message    = "this user does not exist";
               }
               }catch (Exception $e) {

            $transaction->rollBack();
            $status     = 3;
            $api_status = 500;
            $message    = "there is some exception"; 

           }               
         }else{
               $status   = 4; 
               $api_status = 400;
               $message  = "password does not match";
           }
        }
        else{
            $status   = 5; 
            $api_status = 400;
            $message  = "Please fill all required feilds";
        }
        
        $result = array("status"=>$status,"message"=>$message);  
        $this->_sendResponse($api_status, $result);
        exit;
    }
    
    public function actionDeleteUser()
    {
        $user_id = isset($_GET['user_id'])?$_GET['user_id']:'';
        if(!empty($user_id))
        {
         $transaction = Yii::$app->db->beginTransaction();   // begin the transaction

         try {
                    $model = CenterUser::findOne($user_id);
                    if(!is_null($model))
                    {
                        if($model->delete())
                        {
                            $transaction->commit();
                            $status     = 1;
                            $api_status = 200;
                            $message    = "user has been deleted sucessfully";

                        }else{
                              $status     = 2;
                              $api_status = 400;
                              $message    = "problem in saving model";
                        }
                    }else{
                              $status     = 4;
                              $api_status = 400;
                              $message    = "this user does not exist";
                    }
        }catch (Exception $e) {

            $transaction->rollBack();
            $status     = 3;
            $api_status = 500;
            $message    = "there is some exception"; 

           } 
           
        }else{
            
            $status   = 5; 
            $api_status = 400;
            $message  = "Please select a user to delete";
            
        }
        
        $result = array("status"=>$status,"message"=>$message);  
        $this->_sendResponse($api_status, $result);
        exit;
    }

}