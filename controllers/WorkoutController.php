<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
//use yii\web\Controller;
use app\components\Controller;
use yii\filters\VerbFilter;
use app\models\Worktype;
use \app\models\Workouts;


/*sdasd*/
//error_reporting(0);
class WorkoutController extends Controller
{
    public function actionIndex()
    {
       
    }
    
    public function actionAddWorkOut()
    {
        $workout_id   = isset($_GET['workout_id'])?$_GET['workout_id']:'';
        $worktype_id  = isset($_GET['worktype_id'])?$_GET['worktype_id']:'';
        $user_id      = isset($_GET['user_id'])?$_GET['user_id']:'';
        $gender       = isset($_GET['gender'])?$_GET['gender']:'';
        $age          = isset($_GET['age'])?$_GET['age']:'';
        $duration     = isset($_GET['duration'])?$_GET['duration']:'';
        $session_id   = isset($_GET['session_id'])?$_GET['session_id']:'';
        $notes        = isset($_GET['notes'])?$_GET['notes']:'';
        
        if($worktype_id==2)
        {
            if(!empty($workout_id) && !empty($user_id) && !empty($gender) && !empty($duration) && !empty($session_id) && !empty($age))
            {
                
                if($gender==1)
                {
                    //$bmr =  13.75 * $user->curweight+(5*$user->height)-(6.76 * $age)+66;}
                }
            }else{
                
                    $status  = 0;
                    $message = "Please provide all required feilds";
            }
        }
        
      
    }
    
    public function actionWorkoutCategoryList()
    {
        $model =  Worktype::find()->all();
        $result = array();
        foreach ($model as $val)
        {
            $worktype['id']       = $val->id;
            $worktype['name']     = $val->name;
            
            array_push($result, $worktype);
        }
        
        echo json_encode($result);
    }
    
    public function actionWorkoutList()
    {
        $category     =  isset($_GET['category_id'])?$_GET['category_id']:'';
        $result       =  array();
        if(!empty($category))
        {
            $model = Workouts::find()->where('worktype_id = :worktype_id', ['worktype_id'=>$category])->all();
            foreach($model as $val)
            {
                $workout['id']            = $val->id;
                $workout['worktype']      = $val->worktype_id;
                $workout['name']          = $val->name;
                
                array_push($result, $workout);
            }
        }
        else{
            
            $model = Workouts::find()->all();
            foreach($model as $val)
            {
                $workout['id']            = $val->id;
                $workout['worktype']      = $val->worktype_id;
                $workout['name']          = $val->name;
                
                array_push($result, $workout);
            }
            
        }
        
        echo json_encode($result);
    }
    
}