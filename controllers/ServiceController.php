<?php
namespace app\controllers;

class ServiceController extends \app\components\Controller{
    public function actionGetCity(){
        $state_id = isset($_REQUEST['state_id']) ? $_REQUEST['state_id'] : "";
        $dataSet = [];
        if($state_id != ""){
            $cities = \app\models\City::find()->select(["id","city"])->where(["state"=>$state_id])->asArray()->all();
            $dataSet = \yii\helpers\ArrayHelper::map($cities, 'id', 'city');
        }
        echo "<option value=''>Select City</option>";
        foreach ($dataSet as $id=>$city){
            echo "<option value='$id'>$city</option>";
        }
    }
    public function actionGetLocality(){
        $city_id = isset($_REQUEST['city_id']) ? $_REQUEST['city_id'] : "";
        $dataSet = [];
        if($city_id != ""){
            $localities = \app\models\Locality::find()->select(["id","locality"])->where(["city"=>$city_id])->asArray()->all();
            $dataSet = \yii\helpers\ArrayHelper::map($localities, 'id', 'locality');
        }
        echo "<option value=''>Select Locality</option>";
        foreach ($dataSet as $id=>$city){
            echo "<option value='$id'>$city</option>";
        }
    }
}