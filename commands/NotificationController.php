<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\CenterUser;
use app\models\PersonaltrainerWorkout;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Manas Pakal <msg4manas@gmail.com>
 * @since 2.0
 */
class NotificationController extends Controller {

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'Cron Running') {
        echo $message . "\n";
//        \Yii::warning("Cron Called : ".  date('Y-m-d H:i:s'));
    }

    /**
     * send email using mandril SMTP and phpMailer
     * @author Manas Pakal <msg4manas@gmail.com>
     * 
     */
    public function sendviaMandrill($to, $subject, $body, $from, $fromname = '', $tag = '', $cc = '', $ccname = '', $bcc = '', $bccname = '', $template = '', $attach = '') {
        $fromname = ($fromname != '') ? $fromname : "Traq Team";
        require_once \Yii::getAlias('@app') . '/common/libraries/mandrill/class.phpmailer.php';
        $mail = new \PHPMailer;
        $mail->IsSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.mandrillapp.com';
        $mail->Port = '587';
        $mail->SMTPAuth = true;
        $mail->Username = Yii::$app->params['mandrilUsername'];
        $mail->Password = Yii::$app->params['mandrilPassword'];
        $mail->SMTPSecure = 'tls';
        $mail->From = $from;
        $mail->FromName = $fromname;
        //Here for multiple recipients
        if (is_array($to) && !empty($to)) {
            //Header not to show other recipients in the mail
            $mail->addCustomHeader('X-MC-PreserveRecipients', "false");
            //Here added tags to mail content
            if ($tag)
                $mail->addCustomHeader('X-MC-Tags', $tag);
            // below line is temporary
            //$mail->addCustomHeader('X-MC-SigningDomain', 'gmail.com');
            foreach ($to as $k => $v) {
                if (is_array($v)) {
                    if (strlen(trim($v[0]))) {
                        $mail->AddAddress($v[1], $v[0]);
                        $mail->addCustomHeader('X-MC-MergeVars', '{"_rcpt": "' . $v[1] . '", "NAME": "' . $v[0] . '"}');
                        if (isset($v[2]) && is_array($v[2]) && count($v[2])) {
                            foreach ($v[2] as $mtag => $mval) {
                                $mail->addCustomHeader('X-MC-MergeVars', '{"_rcpt": "' . $v[1] . '", "' . $mtag . '": "' . $mval . '"}');
                            }
                        }
                    } else {
                        $mail->AddAddress($v[1], $v[1]);
                    }
                } else {
                    $mail->AddAddress($v, $v);
                }
            }
        } else {
            $mail->AddAddress($to, $to);  // Add a recipient
        }
        // default name
        $mail->addCustomHeader('X-MC-MergeVars', '{"NAME": "User"}');

        //Here for CC and BCC
        if ($cc)
            $mail->addCC($cc, $ccname);
        if ($bcc)
            $mail->addBCC($bcc, $bccname);

        //add template
        if ($template)
            $mail->addCustomHeader('X-MC-Template', trim($template));

        $mail->IsHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AltBody = strip_tags($body);

        //Here goes attachment
        if (is_array($attach)) {
            foreach ($attach as $p) {
                if ($p && file_exists($p))
                    $mail->addAttachment($p);
            }
        }else {
            if ($attach && file_exists($attach))
                $mail->addAttachment($attach);
        }

        /* if ($attach && file_exists($attach))
          $mail->addAttachment($attach); */



        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Send Notification EMail to trainer and member if - on respective week workout chart preparation is pending by trainer
     * @author Manas Pakal <msg4manas@gmail.com>
     */
    public function actionWorkoutWeekly() {
        $user_tbl = CenterUser::tableName();
        $workout_tbl = PersonaltrainerWorkout::tableName();
        $from_date = date("d-m-Y", strtotime('monday this week'));
        $to_date = date("d-m-Y", strtotime('sunday this week'));
        $sql = "SELECT t.id,CONCAT(t.first_name,' ',t.last_name) trainer_name,t.email trainer_email,GROUP_CONCAT(CONCAT(u.first_name,' ',u.last_name,'==',u.email)) member 
FROM $user_tbl t 
LEFT JOIN $user_tbl u ON t.id = u.personal_trainer_id
LEFT JOIN $workout_tbl w ON u.id = w.user_id AND STR_TO_DATE(w.workout_date,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(w.workout_date,'%d-%m-%Y') <= '$to_date'
where t.role = 4 AND u.in_que AND u.status = 1 AND t.status = 1 AND w.workout_date IS NULL AND u.id IS NOT NULL
GROUP BY t.id";
//        Yii::warning($sql);
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($sql);
        $data = $query_bld->queryAll();
        $email_list = array();
        $member_emails = [];
        foreach ($data as $row) {
            /* if($row['id'] == 5)
              $row['trainer_email'] = 'manas.pakal@gympik.com';
              else
              $row['trainer_email'] = 'msg4manas@gmail.com'; */
            $user_lists = "";
            if (isset($row['member']) && !is_null($row['member'])) {
                $members = $row['member'];
                $members = explode(',', $members);
                $list = [];
                if (is_array($members) && count($members)) {
                    foreach ($members as $mem) {
                        $list_str = explode("==", $mem);
                        if (is_array($list_str) && count($list_str) == 2) {
                            $list[$list_str[0]] = $list_str[1];
                        }
                    }
                }
                if (count($list)) {
                    foreach ($list as $email => $name) {
                        $name = ucwords($name);
                        $user_lists .= "$name &nbsp; $email <br/>";
                        $member_emails[] = [$email, $name, ['TRN_NAME' => ucwords($row['trainer_name'])]];
                    }
                }
            }
            $email_list[] = [
                ucwords($row['trainer_name']),
                $row['trainer_email'],
                [
                    'USR_LST' => $user_lists
                ]
            ];
        }
        $mon = date("d-m-Y", strtotime('monday this week'));
        $sun = date("d-m-Y", strtotime('sunday this week'));
        $subject = "Workout chart Pending $mon - $sun";
        $email_body = <<<EOP
                Dear *|NAME|* <br />
                <p>
                You Have following List of Members,<br /><br />
                *|USR_LST|*
                </p>
EOP;
        
        if (count($email_list)) {
            $ret = $this->sendviaMandrill($email_list, $subject, $email_body, Yii::$app->params['adminEmail'], Yii::$app->params['adminName']);
            var_dump($ret);
        }
        $user_cnt = <<<EOP
                Dear *|NAME|* <br />
                <p>
                Please Ask you trainer (*|TRN_NAME|*) to make workout
                </p>
EOP;

        if (count($member_emails)) {
            $ret = $this->sendviaMandrill($member_emails, $subject, $user_cnt, Yii::$app->params['adminEmail'], Yii::$app->params['adminName']);
            var_dump($ret);
        }
    }

    public function actionDietWeekly(){
        $user_tbl = CenterUser::tableName();
        $diet_tbl = \app\models\DietChart::tableName();
                
        $from_date = date("Y-m-d", strtotime('monday this week'));
        $to_date = date("Y-m-d", strtotime('sunday this week'));
        $sql = "SELECT t.id,CONCAT(t.first_name,' ',t.last_name) trainer_name,t.email trainer_email,GROUP_CONCAT(CONCAT(u.first_name,' ',u.last_name,'==',u.email)) member 
FROM $user_tbl t 
LEFT JOIN $user_tbl u ON t.id = u.nutritionist_id
LEFT JOIN $diet_tbl d ON u.id = d.user_id AND STR_TO_DATE(d.date,'%d-%m-%Y') >= '$from_date' AND STR_TO_DATE(d.date,'%d-%m-%Y') <= '$to_date'
where t.role = 5 AND u.diet_in_queue AND u.status = 1 AND t.status = 1 AND d.date IS NULL AND u.id IS NOT NULL
GROUP BY t.id";
        $conn = Yii::$app->db;
        $query_bld = $conn->createCommand($sql);
        $data = $query_bld->queryAll();
        $email_list = array();
        $member_emails = [];
        foreach ($data as $row) {
            /* if($row['id'] == 5)
              $row['trainer_email'] = 'manas.pakal@gympik.com';
              else
              $row['trainer_email'] = 'msg4manas@gmail.com'; */
            $user_lists = "";
            if (isset($row['member']) && !is_null($row['member'])) {
                $members = $row['member'];
                $members = explode(',', $members);
                $list = [];
                if (is_array($members) && count($members)) {
                    foreach ($members as $mem) {
                        $list_str = explode("==", $mem);
                        if (is_array($list_str) && count($list_str) == 2) {
                            $list[$list_str[0]] = $list_str[1];
                        }
                    }
                }
                if (count($list)) {
                    foreach ($list as $email => $name) {
                        $name = ucwords($name);
                        $user_lists .= "$name &nbsp; $email <br/>";
                        $member_emails[] = [$email, $name, ['TRN_NAME' => ucwords($row['trainer_name'])]];
                    }
                }
            }
            $email_list[] = [
                ucwords($row['trainer_name']),
                $row['trainer_email'],
                [
                    'USR_LST' => $user_lists
                ]
            ];
        }
        $mon = date("d-m-Y", strtotime('monday this week'));
        $sun = date("d-m-Y", strtotime('sunday this week'));
        $subject = "Diet chart Pending $mon - $sun";
        $email_body = <<<EOP
                Dear *|NAME|* <br />
                <p>
                You Have following List of Members,<br /><br />
                *|USR_LST|*
                </p>
EOP;
        
        if (count($email_list)) {
            $ret = $this->sendviaMandrill($email_list, $subject, $email_body, Yii::$app->params['adminEmail'], Yii::$app->params['adminName']);
            var_dump($ret);
        }
        $user_cnt = <<<EOP
                Dear *|NAME|* <br />
                <p>
                Please Ask you trainer (*|TRN_NAME|*) to make Diet Chart
                </p>
EOP;

        if (count($member_emails)) {
            $ret = $this->sendviaMandrill($member_emails, $subject, $user_cnt, Yii::$app->params['adminEmail'], Yii::$app->params['adminName']);
            var_dump($ret);
        }
    }
    
    // to send email to list
    public function actionSendemailtolist(){
        
        $now = date("Y-m-d H:i:s");
        $compose_list = \app\models\EmailerCompose::find()
                ->where("sent_on <= '$now' AND completed = 0")
                ->all();
        echo "<pre>";
        print_r($compose_list);
        exit;
    }
}
